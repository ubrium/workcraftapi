/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : storecraft_final

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-04-01 16:00:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `access_token_types`
-- ----------------------------
DROP TABLE IF EXISTS `access_token_types`;
CREATE TABLE `access_token_types` (
  `AccessTokenTypeId` int(10) NOT NULL,
  `Caption` varchar(50) NOT NULL,
  PRIMARY KEY (`AccessTokenTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of access_token_types
-- ----------------------------
INSERT INTO `access_token_types` VALUES ('1', 'Admin');
INSERT INTO `access_token_types` VALUES ('2', 'Portal');

-- ----------------------------
-- Table structure for `app_languages`
-- ----------------------------
DROP TABLE IF EXISTS `app_languages`;
CREATE TABLE `app_languages` (
  `AppLanguageId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `OriginalName` varchar(255) NOT NULL,
  `ISO` varchar(255) NOT NULL,
  PRIMARY KEY (`AppLanguageId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_languages
-- ----------------------------
INSERT INTO `app_languages` VALUES ('1', 'English', 'English', 'en');
INSERT INTO `app_languages` VALUES ('2', 'Arabic', '', 'ar');
INSERT INTO `app_languages` VALUES ('3', 'Bulgarian', '', 'bg');
INSERT INTO `app_languages` VALUES ('4', 'Catalan', '', 'ca');
INSERT INTO `app_languages` VALUES ('5', 'German', 'Deutsch', 'de');
INSERT INTO `app_languages` VALUES ('6', 'Greek', '', 'el');
INSERT INTO `app_languages` VALUES ('7', 'Spanish', '', 'es');
INSERT INTO `app_languages` VALUES ('8', 'French', 'Francais', 'fr');
INSERT INTO `app_languages` VALUES ('9', 'Indonesian', '', 'in');
INSERT INTO `app_languages` VALUES ('10', 'Italian', '', 'it');
INSERT INTO `app_languages` VALUES ('11', 'Japanese', '', 'ja');
INSERT INTO `app_languages` VALUES ('12', 'Macedonian', '', 'mk');
INSERT INTO `app_languages` VALUES ('13', 'Dutch', '', 'nl');
INSERT INTO `app_languages` VALUES ('14', 'Portuguese', '', 'pt');
INSERT INTO `app_languages` VALUES ('15', 'Russian', '', 'ru');
INSERT INTO `app_languages` VALUES ('16', 'Albanian', '', 'sq');
INSERT INTO `app_languages` VALUES ('17', 'Serbian', '', 'rs');
INSERT INTO `app_languages` VALUES ('18', 'Swedish', '', 'sv');
INSERT INTO `app_languages` VALUES ('19', 'Turkish', '', 'tr');

-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `ArticleId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Subtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `Picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateCreated` datetime NOT NULL,
  `Tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ArticleId`),
  KEY `FK_blog_users` (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('1', '26', 'qwe', 'qwe', 'qwe', 'qwe', '2018-03-31 20:53:11', 'qwe');
INSERT INTO `articles` VALUES ('2', '26', 'drugi post', 'fudbal', 'ovo je drugi post', 'asd', '2018-03-31 22:24:56', 'fudbal');

-- ----------------------------
-- Table structure for `comments`
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `CommentId` int(11) NOT NULL AUTO_INCREMENT,
  `ArticleId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Comment` text COLLATE utf8_unicode_ci NOT NULL,
  `DateCreated` datetime NOT NULL,
  PRIMARY KEY (`CommentId`),
  KEY `FK_comments_users` (`UserId`),
  KEY `FK_comments_articles` (`ArticleId`),
  CONSTRAINT `FK_comments_articles` FOREIGN KEY (`ArticleId`) REFERENCES `articles` (`ArticleId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_comments_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('1', '1', '26', 'Ovo je komentar', '2018-04-01 11:54:22');
INSERT INTO `comments` VALUES ('2', '1', '26', 'Ovo je drugi komentar', '2018-04-01 12:14:48');

-- ----------------------------
-- Table structure for `confirmation_links`
-- ----------------------------
DROP TABLE IF EXISTS `confirmation_links`;
CREATE TABLE `confirmation_links` (
  `ConfirmationLinkId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ConfirmationLink` varchar(250) NOT NULL,
  PRIMARY KEY (`ConfirmationLinkId`),
  KEY `confirmation_links_ibfk_1` (`UserId`) USING BTREE,
  CONSTRAINT `confirmation_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of confirmation_links
-- ----------------------------

-- ----------------------------
-- Table structure for `content_sections`
-- ----------------------------
DROP TABLE IF EXISTS `content_sections`;
CREATE TABLE `content_sections` (
  `ContentSectionId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `TypeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ContentSectionId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_sections
-- ----------------------------
INSERT INTO `content_sections` VALUES ('1', 'Education', '1');
INSERT INTO `content_sections` VALUES ('2', 'Experience', '1');
INSERT INTO `content_sections` VALUES ('3', 'Skills', '1');
INSERT INTO `content_sections` VALUES ('4', 'Languages', '1');
INSERT INTO `content_sections` VALUES ('5', 'Certificates', '1');
INSERT INTO `content_sections` VALUES ('6', 'Volunteering', '1');
INSERT INTO `content_sections` VALUES ('7', 'Interests', '1');
INSERT INTO `content_sections` VALUES ('8', 'Portfolio', '2');
INSERT INTO `content_sections` VALUES ('9', 'Quotes', '2');
INSERT INTO `content_sections` VALUES ('10', 'Blog', '2');
INSERT INTO `content_sections` VALUES ('11', 'Intro', '3');

-- ----------------------------
-- Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `JobId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ExperienceLevel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Technologies` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`JobId`),
  KEY `FK_jobs_users` (`UserId`),
  CONSTRAINT `FK_jobs_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES ('1', '26', 'FullTime', 'Mid-Level', 'Frontend Developer', 'JavaScript', 'We are looking for frontend developer with at least 3 years of experience');
INSERT INTO `jobs` VALUES ('2', '26', 'Intership', 'Beginer', 'Backend Developer', 'php', 'We offer an intership for talented beginer');

-- ----------------------------
-- Table structure for `password_reset_links`
-- ----------------------------
DROP TABLE IF EXISTS `password_reset_links`;
CREATE TABLE `password_reset_links` (
  `PasswordResetLinkId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ResetLink` varchar(250) NOT NULL,
  PRIMARY KEY (`PasswordResetLinkId`),
  KEY `confirmation_links_ibfk_1` (`UserId`) USING BTREE,
  CONSTRAINT `password_reset_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of password_reset_links
-- ----------------------------

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `PermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `Caption` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`PermissionId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'Dashboard', null);
INSERT INTO `permissions` VALUES ('2', 'ViewUsers', null);
INSERT INTO `permissions` VALUES ('3', 'AddUsers', null);
INSERT INTO `permissions` VALUES ('4', 'EditUsers', null);
INSERT INTO `permissions` VALUES ('5', 'DeleteUsers', null);
INSERT INTO `permissions` VALUES ('20', 'Profile', null);

-- ----------------------------
-- Table structure for `portfolio_comments`
-- ----------------------------
DROP TABLE IF EXISTS `portfolio_comments`;
CREATE TABLE `portfolio_comments` (
  `ComentId` int(11) NOT NULL AUTO_INCREMENT,
  `PortfolioId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Comment` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ComentId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of portfolio_comments
-- ----------------------------

-- ----------------------------
-- Table structure for `portfolio_images`
-- ----------------------------
DROP TABLE IF EXISTS `portfolio_images`;
CREATE TABLE `portfolio_images` (
  `PortfolioImageId` int(11) NOT NULL AUTO_INCREMENT,
  `Picture` varchar(255) NOT NULL,
  `ResumePortfolioId` int(11) NOT NULL,
  PRIMARY KEY (`PortfolioImageId`),
  KEY `fk_portfolio_images_resume_portfolios` (`ResumePortfolioId`),
  CONSTRAINT `fk_portfolio_images_resume_portfolios` FOREIGN KEY (`ResumePortfolioId`) REFERENCES `resume_portfolios` (`ResumePortfolioId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of portfolio_images
-- ----------------------------

-- ----------------------------
-- Table structure for `publish_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `publish_statuses`;
CREATE TABLE `publish_statuses` (
  `PublishStatusId` int(11) NOT NULL DEFAULT '0',
  `Caption` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PublishStatusId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of publish_statuses
-- ----------------------------
INSERT INTO `publish_statuses` VALUES ('0', 'Opened');
INSERT INTO `publish_statuses` VALUES ('1', 'Draft');
INSERT INTO `publish_statuses` VALUES ('2', 'Published');

-- ----------------------------
-- Table structure for `resumes`
-- ----------------------------
DROP TABLE IF EXISTS `resumes`;
CREATE TABLE `resumes` (
  `ResumeId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `TemplateId` int(11) DEFAULT NULL,
  `ThemeId` int(11) DEFAULT NULL,
  `StatusId` int(11) DEFAULT NULL,
  `Views` int(11) DEFAULT '0',
  `LanguageCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ResumeId`),
  KEY `fk_resumes_users` (`UserId`),
  CONSTRAINT `fk_resumes_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resumes
-- ----------------------------
INSERT INTO `resumes` VALUES ('23', '26', '1', '10', '1', '4', 'en');

-- ----------------------------
-- Table structure for `resume_basics`
-- ----------------------------
DROP TABLE IF EXISTS `resume_basics`;
CREATE TABLE `resume_basics` (
  `ResumeBasicId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `About` text,
  `ContactEmail` varchar(255) DEFAULT NULL,
  `Website` varchar(255) DEFAULT NULL,
  `CountryCode` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `ResumeId` int(11) DEFAULT NULL,
  `Picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ResumeBasicId`),
  KEY `fk_resume_basics_resumes` (`ResumeId`),
  CONSTRAINT `fk_resume_basics_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_basics
-- ----------------------------
INSERT INTO `resume_basics` VALUES ('23', 'Milos Stojkovic', 'web developer', 'This is about', 'milos.stojkovic90@yahoo.com', '', 'RS', '062 48 73 74', '23', 'project-blogitalqzpknr.jpg');

-- ----------------------------
-- Table structure for `resume_certificates`
-- ----------------------------
DROP TABLE IF EXISTS `resume_certificates`;
CREATE TABLE `resume_certificates` (
  `ResumeCertificateId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Organisation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`ResumeCertificateId`),
  KEY `fk_resume_certifikates_resume` (`ResumeId`),
  CONSTRAINT `resume_certificates_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_certificates
-- ----------------------------
INSERT INTO `resume_certificates` VALUES ('10', '23', 'PHP &amp; Javascript course', 'IT Academy', '2016-10-01');

-- ----------------------------
-- Table structure for `resume_educations`
-- ----------------------------
DROP TABLE IF EXISTS `resume_educations`;
CREATE TABLE `resume_educations` (
  `ResumeEducationId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `University` varchar(255) NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date DEFAULT NULL,
  `AverageAchieved` float(3,1) DEFAULT NULL,
  `AverageTotal` int(11) DEFAULT NULL,
  PRIMARY KEY (`ResumeEducationId`),
  KEY `fk_resume_educations_resume` (`ResumeId`),
  CONSTRAINT `resume_educations_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_educations
-- ----------------------------
INSERT INTO `resume_educations` VALUES ('32', '23', '', 'College of Applied Technical Sciences in Niš', '2010-10-01', '2014-05-01', '7.0', '10');
INSERT INTO `resume_educations` VALUES ('33', '23', 'Information Technology', '<span style=\"color: rgb(34, 34, 34); font-family: &quot;Gotham SSm&quot;, Helvetica, Arial, sans-serif; font-size: 15px; background-color: rgb(255, 255, 255);\">Engineer\'s degree | Alfa Univerzitet</span>', '2012-10-01', '2016-04-01', '8.0', '10');
INSERT INTO `resume_educations` VALUES ('36', '23', 'qwe', 'qwe', '2018-03-06', '2018-03-10', '12.4', '12');

-- ----------------------------
-- Table structure for `resume_experiences`
-- ----------------------------
DROP TABLE IF EXISTS `resume_experiences`;
CREATE TABLE `resume_experiences` (
  `ResumeExperienceId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date DEFAULT NULL,
  PRIMARY KEY (`ResumeExperienceId`),
  KEY `fk_resume_experience_resume` (`ResumeId`),
  CONSTRAINT `resume_experiences_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_experiences
-- ----------------------------
INSERT INTO `resume_experiences` VALUES ('35', '23', 'Web developer', 'Gentlemandevelopment', 'Building web applications, systems and websites.', '2014-10-01', '2016-04-01');

-- ----------------------------
-- Table structure for `resume_interests`
-- ----------------------------
DROP TABLE IF EXISTS `resume_interests`;
CREATE TABLE `resume_interests` (
  `ResumeInterestId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci,
  `Icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ResumeInterestId`),
  KEY `fk_resume_interests_resume` (`ResumeId`),
  CONSTRAINT `resume_interests_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_interests
-- ----------------------------
INSERT INTO `resume_interests` VALUES ('34', '23', 'Sport', 'I like sport', 'fa fa-cubes');
INSERT INTO `resume_interests` VALUES ('35', '23', 'Gym', 'Building snake body', 'fa fa-cubes');

-- ----------------------------
-- Table structure for `resume_intros`
-- ----------------------------
DROP TABLE IF EXISTS `resume_intros`;
CREATE TABLE `resume_intros` (
  `ResumeIntroId` int(11) NOT NULL AUTO_INCREMENT,
  `Picture` varchar(255) DEFAULT NULL,
  `BackgroundColor` varchar(255) DEFAULT NULL,
  `Text` text,
  `ResumeId` int(11) NOT NULL,
  PRIMARY KEY (`ResumeIntroId`),
  KEY `fk_resume_intro_resumes` (`ResumeId`) USING BTREE,
  CONSTRAINT `fk_resume_intro_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_intros
-- ----------------------------
INSERT INTO `resume_intros` VALUES ('7', 'abg4.jpg', null, null, '23');
INSERT INTO `resume_intros` VALUES ('8', 'abg4.jpg', null, null, '23');

-- ----------------------------
-- Table structure for `resume_languages`
-- ----------------------------
DROP TABLE IF EXISTS `resume_languages`;
CREATE TABLE `resume_languages` (
  `ResumeLanguageId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`ResumeLanguageId`),
  KEY `fk_resume_languages_resume` (`ResumeId`),
  CONSTRAINT `resume_languages_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_languages
-- ----------------------------
INSERT INTO `resume_languages` VALUES ('22', '23', 'English', '4');
INSERT INTO `resume_languages` VALUES ('23', '23', 'German', '3');
INSERT INTO `resume_languages` VALUES ('24', '23', 'Svahili', '5');

-- ----------------------------
-- Table structure for `resume_portfolios`
-- ----------------------------
DROP TABLE IF EXISTS `resume_portfolios`;
CREATE TABLE `resume_portfolios` (
  `ResumePortfolioId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Title` text,
  `CaseStudy` text,
  `Category` text,
  `Picture` varchar(255) DEFAULT NULL,
  `ShortDescription` varchar(255) DEFAULT NULL,
  `PublishStatusId` int(11) DEFAULT NULL,
  `ParentId` int(11) DEFAULT NULL,
  `DatePublished` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `Comments` text NOT NULL,
  PRIMARY KEY (`ResumePortfolioId`),
  KEY `fk_resume_portfolio_resumes` (`ResumeId`),
  KEY `fk_resume_portfolio_publish_statuses` (`PublishStatusId`),
  KEY `fk_resume_portfolio_resume_portfolios` (`ParentId`),
  CONSTRAINT `fk_resume_portfolio_publish_statuses` FOREIGN KEY (`PublishStatusId`) REFERENCES `publish_statuses` (`PublishStatusId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_resume_portfolio_resume_portfolios` FOREIGN KEY (`ParentId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_resume_portfolio_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_portfolios
-- ----------------------------
INSERT INTO `resume_portfolios` VALUES ('18', '23', 'Gynny', '                                <p>Proveravam kejs of stady</p>', '', 'project-blog0ohas8mqju.jpg', 'Gynny is german company', '2', null, '2018-03-13 13:58:15', null, '');

-- ----------------------------
-- Table structure for `resume_quotes`
-- ----------------------------
DROP TABLE IF EXISTS `resume_quotes`;
CREATE TABLE `resume_quotes` (
  `ResumeQuoteId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Description` text NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Picture` varchar(255) DEFAULT NULL,
  `Position` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ResumeQuoteId`),
  KEY `fk_resume_quotes_resumes` (`ResumeId`),
  CONSTRAINT `fk_resume_quotes_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_quotes
-- ----------------------------
INSERT INTO `resume_quotes` VALUES ('8', '23', 'Ljudi ljudi vratia se Sime', null, 'Miki Mikic', '', 'Ovo je najbolja app na svetu');
INSERT INTO `resume_quotes` VALUES ('9', '23', 'I vratia frisku ribu :)', null, 'Sulje', '', 'Sulje je dzukela');

-- ----------------------------
-- Table structure for `resume_sections`
-- ----------------------------
DROP TABLE IF EXISTS `resume_sections`;
CREATE TABLE `resume_sections` (
  `ResumeSectionId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `ContentSectionId` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ResumeSectionId`),
  KEY `fk_resume_sections_resumes` (`ResumeId`),
  CONSTRAINT `fk_resume_sections_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_sections
-- ----------------------------
INSERT INTO `resume_sections` VALUES ('57', '23', '3', 'Skills');
INSERT INTO `resume_sections` VALUES ('58', '23', '1', 'Education');
INSERT INTO `resume_sections` VALUES ('59', '23', '2', 'Experience');
INSERT INTO `resume_sections` VALUES ('60', '23', '5', 'Certificates');
INSERT INTO `resume_sections` VALUES ('61', '23', '4', 'Languages');
INSERT INTO `resume_sections` VALUES ('62', '23', '6', 'Volunteering');
INSERT INTO `resume_sections` VALUES ('64', '23', '7', 'Interests');
INSERT INTO `resume_sections` VALUES ('65', '23', '8', 'Portfolio');
INSERT INTO `resume_sections` VALUES ('66', '23', '9', 'Quotes');
INSERT INTO `resume_sections` VALUES ('67', '23', '11', 'Intro');
INSERT INTO `resume_sections` VALUES ('68', '23', '11', 'Intro');

-- ----------------------------
-- Table structure for `resume_skills`
-- ----------------------------
DROP TABLE IF EXISTS `resume_skills`;
CREATE TABLE `resume_skills` (
  `ResumeSkillId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`ResumeSkillId`),
  KEY `fk_resume_skills_resume` (`ResumeId`),
  CONSTRAINT `resume_skills_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_skills
-- ----------------------------
INSERT INTO `resume_skills` VALUES ('54', '23', 'HTml', '90');
INSERT INTO `resume_skills` VALUES ('55', '23', 'css', '90');
INSERT INTO `resume_skills` VALUES ('56', '23', 'php', '70');
INSERT INTO `resume_skills` VALUES ('57', '23', 'javascript', '70');
INSERT INTO `resume_skills` VALUES ('58', '23', 'jquery', '70');
INSERT INTO `resume_skills` VALUES ('59', '23', 'mysql', '70');
INSERT INTO `resume_skills` VALUES ('60', '23', 'MVC Framework', '70');
INSERT INTO `resume_skills` VALUES ('61', '23', 'git', '80');
INSERT INTO `resume_skills` VALUES ('62', '23', 'wamp', '80');

-- ----------------------------
-- Table structure for `resume_volunteer_works`
-- ----------------------------
DROP TABLE IF EXISTS `resume_volunteer_works`;
CREATE TABLE `resume_volunteer_works` (
  `ResumeVolunteerWorkId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Organisation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date DEFAULT NULL,
  PRIMARY KEY (`ResumeVolunteerWorkId`),
  KEY `fk_resume_volunteers_resume` (`ResumeId`),
  CONSTRAINT `resume_volunteer_works_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_volunteer_works
-- ----------------------------
INSERT INTO `resume_volunteer_works` VALUES ('11', '23', 'Host', 'Nisville', 'I was host for foreign musician', '2014-07-01', '2014-08-01');
INSERT INTO `resume_volunteer_works` VALUES ('12', '23', 'Guard', 'Exit', 'Guarding Exit from drunk junkies', '2010-06-01', '2012-11-01');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `Protected` tinyint(4) NOT NULL,
  `Active` tinyint(4) NOT NULL,
  `Caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '1', '1', 'Administrator');
INSERT INTO `roles` VALUES ('2', '1', '1', 'User');
INSERT INTO `roles` VALUES ('3', '1', '1', 'Visitor');

-- ----------------------------
-- Table structure for `role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions` (
  `RolePermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) NOT NULL,
  `PermissionId` int(11) NOT NULL,
  `Protected` smallint(6) NOT NULL,
  PRIMARY KEY (`RolePermissionId`),
  KEY `FK_RolePermissions_Permissions` (`PermissionId`) USING BTREE,
  KEY `FK_RolePermissions_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `role_permissions_ibfk_1` FOREIGN KEY (`PermissionId`) REFERENCES `permissions` (`PermissionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permissions_ibfk_2` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES ('1', '1', '1', '0');
INSERT INTO `role_permissions` VALUES ('2', '1', '20', '0');
INSERT INTO `role_permissions` VALUES ('3', '2', '20', '0');
INSERT INTO `role_permissions` VALUES ('4', '1', '2', '0');
INSERT INTO `role_permissions` VALUES ('5', '1', '4', '0');
INSERT INTO `role_permissions` VALUES ('6', '1', '5', '0');
INSERT INTO `role_permissions` VALUES ('7', '1', '3', '0');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `UserId` int(10) NOT NULL AUTO_INCREMENT,
  `Email` varchar(250) NOT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Image` varchar(250) DEFAULT NULL,
  `RegistrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ConfirmRegistration` tinyint(1) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `UniqueEmail` (`Email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('26', 'milos.stojkovic90@yahoo.com', '$2a$10$CFQgMKJsmnYiXv3YBhKW3einDe87CW.tRhG0bnLBQDsyFYMb/ZccG', null, '2018-03-13 12:54:39', '1', '1');

-- ----------------------------
-- Table structure for `user_access_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `user_access_tokens`;
CREATE TABLE `user_access_tokens` (
  `UserAccessTokenId` int(11) NOT NULL AUTO_INCREMENT,
  `Token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UserId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EndDate` datetime DEFAULT NULL,
  `AccessTokenTypeId` int(10) NOT NULL,
  PRIMARY KEY (`UserAccessTokenId`),
  UNIQUE KEY `Unique_Token` (`Token`) USING BTREE,
  KEY `FK_UserAccessTokens_Users` (`UserId`) USING BTREE,
  KEY `TypeId` (`AccessTokenTypeId`) USING BTREE,
  CONSTRAINT `user_access_tokens_ibfk_1` FOREIGN KEY (`AccessTokenTypeId`) REFERENCES `access_token_types` (`AccessTokenTypeId`) ON UPDATE CASCADE,
  CONSTRAINT `user_access_tokens_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_access_tokens
-- ----------------------------
INSERT INTO `user_access_tokens` VALUES ('14', '157a41be915e05eec9dfecf8653743b6', '26', '2018-03-13 13:37:40', '2018-03-13 13:37:53', '1');
INSERT INTO `user_access_tokens` VALUES ('15', 'f31656d2764573f6852617a74fbb18fb', '26', '2018-03-13 13:59:28', '2018-03-20 12:23:54', '2');
INSERT INTO `user_access_tokens` VALUES ('16', '5c62d6bb2b7fb4719388247485a6782a', '26', '2018-03-20 12:23:59', '2018-03-27 09:49:57', '2');
INSERT INTO `user_access_tokens` VALUES ('17', 'd3a575700af1af971f3c3c34fda8141e', '26', '2018-03-27 09:49:57', '2018-03-27 09:59:42', '2');
INSERT INTO `user_access_tokens` VALUES ('18', 'dd089997c1d8bff989e93192cdd55cd6', '26', '2018-04-01 09:31:39', null, '2');

-- ----------------------------
-- Table structure for `user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `UserRoleId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  PRIMARY KEY (`UserRoleId`),
  KEY `FK_UserRoles_Users` (`UserId`) USING BTREE,
  KEY `FK_UserRoles_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('1', '1', '1');
INSERT INTO `user_roles` VALUES ('2', '2', '2');
INSERT INTO `user_roles` VALUES ('3', '3', '2');
INSERT INTO `user_roles` VALUES ('4', '4', '2');
INSERT INTO `user_roles` VALUES ('5', '5', '2');
INSERT INTO `user_roles` VALUES ('6', '6', '1');
INSERT INTO `user_roles` VALUES ('7', '7', '2');
INSERT INTO `user_roles` VALUES ('8', '8', '2');
INSERT INTO `user_roles` VALUES ('9', '9', '2');
INSERT INTO `user_roles` VALUES ('10', '10', '2');
INSERT INTO `user_roles` VALUES ('11', '11', '2');
INSERT INTO `user_roles` VALUES ('12', '12', '2');
INSERT INTO `user_roles` VALUES ('13', '13', '2');
INSERT INTO `user_roles` VALUES ('14', '14', '2');
INSERT INTO `user_roles` VALUES ('15', '15', '2');
INSERT INTO `user_roles` VALUES ('16', '16', '2');
INSERT INTO `user_roles` VALUES ('17', '17', '2');
INSERT INTO `user_roles` VALUES ('18', '18', '2');
INSERT INTO `user_roles` VALUES ('19', '19', '2');
INSERT INTO `user_roles` VALUES ('20', '20', '2');
INSERT INTO `user_roles` VALUES ('21', '21', '2');
INSERT INTO `user_roles` VALUES ('22', '22', '2');
INSERT INTO `user_roles` VALUES ('23', '23', '2');
INSERT INTO `user_roles` VALUES ('24', '24', '2');
INSERT INTO `user_roles` VALUES ('25', '25', '2');
INSERT INTO `user_roles` VALUES ('26', '26', '1');
