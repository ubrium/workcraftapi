/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : storecraft_final

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2017-10-13 04:02:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `access_token_types`
-- ----------------------------
DROP TABLE IF EXISTS `access_token_types`;
CREATE TABLE `access_token_types` (
  `AccessTokenTypeId` int(10) NOT NULL,
  `Caption` varchar(50) NOT NULL,
  PRIMARY KEY (`AccessTokenTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of access_token_types
-- ----------------------------
INSERT INTO `access_token_types` VALUES ('1', 'Admin');
INSERT INTO `access_token_types` VALUES ('2', 'Portal');

-- ----------------------------
-- Table structure for `app_languages`
-- ----------------------------
DROP TABLE IF EXISTS `app_languages`;
CREATE TABLE `app_languages` (
  `AppLanguageId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `OriginalName` varchar(255) NOT NULL,
  `ISO` varchar(255) NOT NULL,
  PRIMARY KEY (`AppLanguageId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_languages
-- ----------------------------
INSERT INTO `app_languages` VALUES ('1', 'English', 'English', 'en');
INSERT INTO `app_languages` VALUES ('2', 'Arabic', '', 'ar');
INSERT INTO `app_languages` VALUES ('3', 'Bulgarian', '', 'bg');
INSERT INTO `app_languages` VALUES ('4', 'Catalan', '', 'ca');
INSERT INTO `app_languages` VALUES ('5', 'German', 'Deutsch', 'de');
INSERT INTO `app_languages` VALUES ('6', 'Greek', '', 'el');
INSERT INTO `app_languages` VALUES ('7', 'Spanish', '', 'es');
INSERT INTO `app_languages` VALUES ('8', 'French', 'Francais', 'fr');
INSERT INTO `app_languages` VALUES ('9', 'Indonesian', '', 'in');
INSERT INTO `app_languages` VALUES ('10', 'Italian', '', 'it');
INSERT INTO `app_languages` VALUES ('11', 'Japanese', '', 'ja');
INSERT INTO `app_languages` VALUES ('12', 'Macedonian', '', 'mk');
INSERT INTO `app_languages` VALUES ('13', 'Dutch', '', 'nl');
INSERT INTO `app_languages` VALUES ('14', 'Portuguese', '', 'pt');
INSERT INTO `app_languages` VALUES ('15', 'Russian', '', 'ru');
INSERT INTO `app_languages` VALUES ('16', 'Albanian', '', 'sq');
INSERT INTO `app_languages` VALUES ('17', 'Serbian', '', 'rs');
INSERT INTO `app_languages` VALUES ('18', 'Swedish', '', 'sv');
INSERT INTO `app_languages` VALUES ('19', 'Turkish', '', 'tr');

-- ----------------------------
-- Table structure for `confirmation_links`
-- ----------------------------
DROP TABLE IF EXISTS `confirmation_links`;
CREATE TABLE `confirmation_links` (
  `ConfirmationLinkId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ConfirmationLink` varchar(250) NOT NULL,
  PRIMARY KEY (`ConfirmationLinkId`),
  KEY `confirmation_links_ibfk_1` (`UserId`) USING BTREE,
  CONSTRAINT `confirmation_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of confirmation_links
-- ----------------------------

-- ----------------------------
-- Table structure for `content_sections`
-- ----------------------------
DROP TABLE IF EXISTS `content_sections`;
CREATE TABLE `content_sections` (
  `ContentSectionId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `TypeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ContentSectionId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content_sections
-- ----------------------------
INSERT INTO `content_sections` VALUES ('1', 'Education', '1');
INSERT INTO `content_sections` VALUES ('2', 'Experience', '1');
INSERT INTO `content_sections` VALUES ('3', 'Skills', '1');
INSERT INTO `content_sections` VALUES ('4', 'Languages', '1');
INSERT INTO `content_sections` VALUES ('5', 'Certificates', '1');
INSERT INTO `content_sections` VALUES ('6', 'Volunteering', '1');
INSERT INTO `content_sections` VALUES ('7', 'Interests', '1');
INSERT INTO `content_sections` VALUES ('8', 'Portfolio', '2');
INSERT INTO `content_sections` VALUES ('9', 'Quotes', '2');
INSERT INTO `content_sections` VALUES ('10', 'Blog', '2');

-- ----------------------------
-- Table structure for `password_reset_links`
-- ----------------------------
DROP TABLE IF EXISTS `password_reset_links`;
CREATE TABLE `password_reset_links` (
  `PasswordResetLinkId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ResetLink` varchar(250) NOT NULL,
  PRIMARY KEY (`PasswordResetLinkId`),
  KEY `confirmation_links_ibfk_1` (`UserId`) USING BTREE,
  CONSTRAINT `password_reset_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of password_reset_links
-- ----------------------------

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `PermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `Caption` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`PermissionId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'Dashboard', null);
INSERT INTO `permissions` VALUES ('2', 'ViewUsers', null);
INSERT INTO `permissions` VALUES ('3', 'AddUsers', null);
INSERT INTO `permissions` VALUES ('4', 'EditUsers', null);
INSERT INTO `permissions` VALUES ('5', 'DeleteUsers', null);
INSERT INTO `permissions` VALUES ('20', 'Profile', null);

-- ----------------------------
-- Table structure for `portfolio_images`
-- ----------------------------
DROP TABLE IF EXISTS `portfolio_images`;
CREATE TABLE `portfolio_images` (
  `PortfolioImageId` int(11) NOT NULL AUTO_INCREMENT,
  `Picture` varchar(255) NOT NULL,
  `ResumePortfolioId` int(11) NOT NULL,
  PRIMARY KEY (`PortfolioImageId`),
  KEY `fk_portfolio_images_resume_portfolios` (`ResumePortfolioId`),
  CONSTRAINT `fk_portfolio_images_resume_portfolios` FOREIGN KEY (`ResumePortfolioId`) REFERENCES `resume_portfolios` (`ResumePortfolioId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of portfolio_images
-- ----------------------------
INSERT INTO `portfolio_images` VALUES ('1', 'alwaysdata.gif', '1');
INSERT INTO `portfolio_images` VALUES ('2', 'artboard_1.png', '1');
INSERT INTO `portfolio_images` VALUES ('3', 'dribbble-animation.gif', '1');
INSERT INTO `portfolio_images` VALUES ('4', 'dribbble-article.jpg', '1');
INSERT INTO `portfolio_images` VALUES ('5', 'qontohome.gif', '1');
INSERT INTO `portfolio_images` VALUES ('6', 'web1.png', '1');
INSERT INTO `portfolio_images` VALUES ('7', 'project-blogktk1dfgqwa.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('8', 'project-blogk2tmeiozgi.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('9', 'project-blogcfdh9xlt4e.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('10', 'project-blogdomiejhlat.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('11', 'project-blogmqfztvw0ll.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('12', 'project-bloganeklbovgz.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('13', 'project-blogs9bukm3xrn.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('14', 'project-blogcb0ioapyrf.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('15', 'project-blogvew8lj5lxh.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('16', 'project-blog0a92xdduk5.jpg', '7');
INSERT INTO `portfolio_images` VALUES ('17', 'project-blogi0fq9mauzd.png', '7');
INSERT INTO `portfolio_images` VALUES ('18', 'project-blogatnfje19tr.jpg', '9');
INSERT INTO `portfolio_images` VALUES ('19', 'project-bloghvhom698it.jpg', '9');
INSERT INTO `portfolio_images` VALUES ('20', 'project-blogec59svqlsn.jpg', '9');
INSERT INTO `portfolio_images` VALUES ('21', 'project-bloglvkb1qxruj.jpg', '9');
INSERT INTO `portfolio_images` VALUES ('22', 'project-blogk6npmdwaar.jpg', '11');
INSERT INTO `portfolio_images` VALUES ('23', 'project-blogm7jt95n6wp.jpg', '11');
INSERT INTO `portfolio_images` VALUES ('24', 'project-blogg1g3pzqe0z.gif', '11');
INSERT INTO `portfolio_images` VALUES ('25', 'project-blogfgarquxjxb.jpg', '11');
INSERT INTO `portfolio_images` VALUES ('26', 'project-blogzqyw7qferb.gif', '11');
INSERT INTO `portfolio_images` VALUES ('27', 'project-blogebncfytkm5.jpg', '11');

-- ----------------------------
-- Table structure for `publish_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `publish_statuses`;
CREATE TABLE `publish_statuses` (
  `PublishStatusId` int(11) NOT NULL DEFAULT '0',
  `Caption` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PublishStatusId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of publish_statuses
-- ----------------------------
INSERT INTO `publish_statuses` VALUES ('0', 'Opened');
INSERT INTO `publish_statuses` VALUES ('1', 'Draft');
INSERT INTO `publish_statuses` VALUES ('2', 'Published');

-- ----------------------------
-- Table structure for `resumes`
-- ----------------------------
DROP TABLE IF EXISTS `resumes`;
CREATE TABLE `resumes` (
  `ResumeId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `TemplateId` int(11) DEFAULT NULL,
  `ThemeId` int(11) DEFAULT NULL,
  `StatusId` int(11) DEFAULT NULL,
  `Views` int(11) DEFAULT '0',
  `LanguageCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ResumeId`),
  KEY `fk_resumes_users` (`UserId`),
  CONSTRAINT `fk_resumes_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resumes
-- ----------------------------
INSERT INTO `resumes` VALUES ('1', '6', '1', '5', '1', '5', 'en');
INSERT INTO `resumes` VALUES ('4', '9', '1', '1', '1', '0', 'de');
INSERT INTO `resumes` VALUES ('5', '10', '1', '1', '1', '0', 'rs');
INSERT INTO `resumes` VALUES ('6', '11', '1', '1', '1', '10', 'en');
INSERT INTO `resumes` VALUES ('7', '12', '1', '5', '1', '0', 'en');
INSERT INTO `resumes` VALUES ('8', '13', '1', '1', '1', '0', null);
INSERT INTO `resumes` VALUES ('9', '14', '1', '8', '1', '0', 'en');
INSERT INTO `resumes` VALUES ('10', '6', '1', '6', '2', '1', 'rs');
INSERT INTO `resumes` VALUES ('11', '6', '1', '2', '2', '0', 'de');
INSERT INTO `resumes` VALUES ('12', '15', '1', '1', '1', '0', null);
INSERT INTO `resumes` VALUES ('13', '16', '1', '2', '1', '0', 'en');
INSERT INTO `resumes` VALUES ('14', '17', '1', '1', '1', '0', 'en');
INSERT INTO `resumes` VALUES ('15', '18', '1', '1', '1', '0', 'en');

-- ----------------------------
-- Table structure for `resume_basics`
-- ----------------------------
DROP TABLE IF EXISTS `resume_basics`;
CREATE TABLE `resume_basics` (
  `ResumeBasicId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `About` text,
  `ContactEmail` varchar(255) DEFAULT NULL,
  `Website` varchar(255) DEFAULT NULL,
  `CountryCode` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `ResumeId` int(11) DEFAULT NULL,
  `Picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ResumeBasicId`),
  KEY `fk_resume_basics_resumes` (`ResumeId`),
  CONSTRAINT `fk_resume_basics_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_basics
-- ----------------------------
INSERT INTO `resume_basics` VALUES ('1', 'Mihajlo Ljubenovic', 'Web developer', '<p class=\"lead text-lg\" style=\"margin-top: 0px; margin-bottom: 2rem; line-height: 1.6;\"><span style=\"font-size: 25px;\"><span style=\"color: rgb(33, 33, 33); font-size: 25px;\">Hello!</span><span style=\"font-size: 25px;\"> I am a </span></span><span style=\"font-weight: bold;\"><span style=\"color: rgb(33, 33, 33); font-size: 25px;\">web </span><span style=\"font-size: 25px;\"><span style=\"color: rgb(33, 33, 33);\">developer &amp; </span><span style=\"color: rgb(120, 33, 79);\">designer</span></span></span><span style=\"font-size: 25px;\"> from Serbia</span></p><p class=\"lead mb-5\" style=\"text-align: left; margin-top: 0px; margin-bottom: 2rem; line-height: 1.6;\"><span style=\"font-size: 18px;\"><span style=\"color: rgb(47, 47, 47);\">I am self taught and during&nbsp;</span><span style=\"font-weight: bold; color: rgb(120, 33, 79);\">5 years of my career</span><span style=\"color: rgb(47, 47, 47);\"> I have been working for </span><span style=\"color: rgb(47, 47, 47); font-weight: bold;\">clients from all over the World</span><span style=\"color: rgb(47, 47, 47);\">. Thanks to my experience I am able to provide high quality services. Good example of my work is this web app for generating an online&nbsp;</span></span><span style=\"font-size: 18px;\">résumé</span><span style=\"color: rgb(47, 47, 47); font-size: 18px;\">&nbsp;and portfolio, you are currently browsing.</span></p>', 'mihajlo.ljubenovic@ubrium.com', 'www.ubrium.com', 'RS', '+381692300899', '1', 'project-blog5tsxuuxafn.jpg');
INSERT INTO `resume_basics` VALUES ('5', '', '', '', '', '', '', '', '4', '');
INSERT INTO `resume_basics` VALUES ('6', 'John Doe', 'Music producer, traveler, hipster', 'Use a modal for dialog boxes, confirmation messages, or other content that can be called up. In order for the modal to work you have to add the Modal ID to the link of the trigger. To add a close button, just add the class .modal-close to your button.', 'myemail@example.com', 'www.sample.com', 'US', '+381444555666', '5', 'project-blogbl7ys34djq.jpg');
INSERT INTO `resume_basics` VALUES ('7', 'James Doe', 'Architect &amp; 3d artist', '<p style=\"\"><span style=\"color: rgb(33, 33, 33);\"><span style=\"font-size: 37px; font-weight: bold;\"><span style=\"background-color: rgb(250, 250, 250);\">Hello!</span> I am a 27 years old </span><span style=\"font-size: 37px; font-weight: bold;\">architect</span><span style=\"font-weight: bold; font-size: 37px;\"> </span><span style=\"font-size: 37px; font-weight: bold;\">and&nbsp;</span><span style=\"font-size: 37px; font-weight: bold;\">3d Designer</span><span style=\"font-size: 37px; font-weight: bold;\"> from </span><span style=\"font-size: 37px; font-weight: bold;\">Los Angeles, California.</span></span></p><p style=\"text-align: left;\"><span style=\"font-size: 18px;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</span></p>', 'm5@m.com', 'www.example.com', 'US', '+38111555333', '6', 'project-blog7fc3spa8t2.jpg');
INSERT INTO `resume_basics` VALUES ('8', 'Aleksandar Nikolic', 'Profesor, doktor', '<p>Write something about yourself here...</p>', 'ale@don.com', '', 'CO', '+38118333555', '7', 'project-blogc93kz8no7x.jpg');
INSERT INTO `resume_basics` VALUES ('9', 'Djorđije Hadži​‌ć', 'Environmental engineer', '                                <p><span style=\"vertical-align: super; font-weight: bold;\"><span style=\"font-size: 37px;\">The spectacle</span><span style=\"font-size: 37px;\"> before us was indeed </span><span style=\"font-size: 37px;\">sublime.</span></span></p><p>Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon of the sea to the level of the spectator on a hillside, the sable cloud beneath was dished out<br></p>', 'hadza@test.com', '', 'RS', '+381644464644', '9', 'project-blogvisjzyyklc.jpg');
INSERT INTO `resume_basics` VALUES ('10', null, null, null, null, null, null, null, '1', null);
INSERT INTO `resume_basics` VALUES ('11', 'Mihajlo Ljubenovic', 'Web developer', '                                <p><span style=\"font-size: 37px; font-weight: bold;\">I am...</span><br></p><ul><li><span style=\"font-size: 18px; font-weight: bold; color: rgb(233, 30, 99); background-color: rgb(250, 250, 250);\">Creative</span></li><li><span style=\"font-size: 18px; font-weight: bold;\">Professional</span></li><li><span style=\"font-size: 18px; font-weight: bold;\">Reliable</span></li></ul><p style=\"text-align: right; \"><span style=\"font-size: 37px; font-weight: bold; background-color: rgb(250, 250, 250); color: rgb(33, 33, 33);\">I offer</span></p><ul><li style=\"text-align: right;\"><span style=\"font-size: 18px; font-weight: bold;\">Creative</span></li><li style=\"text-align: right; \"><span style=\"font-size: 18px; font-weight: bold; color: rgb(233, 30, 99); background-color: rgb(250, 250, 250);\">Professional</span></li><li style=\"text-align: right; \"><span style=\"font-size: 18px; font-weight: bold;\">Reliable</span></li></ul><p><br></p>', '\n                                        \n                                        \n                                        \n                                        test@test.com\n                                    \n                                    \n   ', '\n                                        \n                                        \n                                        \n                                        www.example.com\n                                    \n                                    \n ', 'GB', '+381665533898', '10', 'project-blogi9stfly6es.jpg');
INSERT INTO `resume_basics` VALUES ('12', 'Mihajlo ljubenovic', '', '                                <p>Write something about yourself here...</p>', '', '', '', '', '11', 'project-blog4ntlvok38o.jpg');
INSERT INTO `resume_basics` VALUES ('13', 'New Test', 'Neki lik', '                                <p>Write something about yourself here...</p>', '', '', '', '', '13', 'project-blogpghfhbwm8v.jpg');
INSERT INTO `resume_basics` VALUES ('14', null, null, null, null, null, null, null, '14', null);
INSERT INTO `resume_basics` VALUES ('15', null, null, null, null, null, null, null, '15', null);

-- ----------------------------
-- Table structure for `resume_certificates`
-- ----------------------------
DROP TABLE IF EXISTS `resume_certificates`;
CREATE TABLE `resume_certificates` (
  `ResumeCertificateId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Organisation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`ResumeCertificateId`),
  KEY `fk_resume_certifikates_resume` (`ResumeId`),
  CONSTRAINT `resume_certificates_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_certificates
-- ----------------------------
INSERT INTO `resume_certificates` VALUES ('5', '1', 'Web development', 'IT Center Nis2', '2015-04-01');
INSERT INTO `resume_certificates` VALUES ('8', '5', 'Certificate name', 'sajdasjd', '2010-04-01');

-- ----------------------------
-- Table structure for `resume_educations`
-- ----------------------------
DROP TABLE IF EXISTS `resume_educations`;
CREATE TABLE `resume_educations` (
  `ResumeEducationId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `University` varchar(255) NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date DEFAULT NULL,
  `AverageAchieved` float(3,1) DEFAULT NULL,
  `AverageTotal` int(11) DEFAULT NULL,
  PRIMARY KEY (`ResumeEducationId`),
  KEY `fk_resume_educations_resume` (`ResumeId`),
  CONSTRAINT `resume_educations_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_educations
-- ----------------------------
INSERT INTO `resume_educations` VALUES ('6', '1', 'Law school - dropout', 'University of Nis', '2009-09-01', '2012-09-01', '7.5', '10');
INSERT INTO `resume_educations` VALUES ('12', '5', 'Economy bachelor degree', 'Harward University', '2009-09-01', '2014-10-01', '4.9', '5');
INSERT INTO `resume_educations` VALUES ('13', '5', 'Economy masters degree', 'Harward University', '2014-10-01', '2016-08-01', '4.8', '5');
INSERT INTO `resume_educations` VALUES ('18', '6', 'Architecture &amp; design', 'Harward University', '2009-09-01', '2013-10-01', '4.8', '5');
INSERT INTO `resume_educations` VALUES ('19', '6', 'Architecture &amp; design - master', 'Harward University<br>', '2013-11-01', '2014-06-01', '4.5', '5');
INSERT INTO `resume_educations` VALUES ('20', '9', 'Environmental engineer - bachelor', 'University in Nis', '2012-09-01', '2017-10-01', '7.4', '10');
INSERT INTO `resume_educations` VALUES ('24', '10', 'Computer sciences and graphic design', 'Harward University', '2011-05-01', '2015-04-01', '4.5', '5');
INSERT INTO `resume_educations` VALUES ('25', '10', 'Nesto drugo', 'Tupim', '2015-04-01', '0000-00-00', '3.0', '5');

-- ----------------------------
-- Table structure for `resume_experiences`
-- ----------------------------
DROP TABLE IF EXISTS `resume_experiences`;
CREATE TABLE `resume_experiences` (
  `ResumeExperienceId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date DEFAULT NULL,
  PRIMARY KEY (`ResumeExperienceId`),
  KEY `fk_resume_experience_resume` (`ResumeId`),
  CONSTRAINT `resume_experiences_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_experiences
-- ----------------------------
INSERT INTO `resume_experiences` VALUES ('19', '5', 'Sample job', 'Sample Inc', 'We are skilled in technologies like PHP, NodeJS, C#, MySQL, MongoDB, HTML5, CSS3, Angular', '2016-09-01', '0000-00-00');
INSERT INTO `resume_experiences` VALUES ('21', '6', 'Sample job', 'Test Inc.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', '2015-03-01', '2016-04-01');
INSERT INTO `resume_experiences` VALUES ('22', '6', 'Another job', 'Another Company LLC', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', '2016-05-01', '0000-00-00');
INSERT INTO `resume_experiences` VALUES ('23', '9', 'Sample job', 'Sample Inc.', 'Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon of the sea to&nbsp;', '2013-03-01', '2015-04-01');
INSERT INTO `resume_experiences` VALUES ('24', '9', 'Sales and marketing', 'Ubrium LLC', 'Apparently we had reached a great height in the atmosphere, for the sky was a dead black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon', '2015-08-01', '0000-00-00');
INSERT INTO `resume_experiences` VALUES ('27', '10', 'Junior web designer', 'Sample Inc.', 'Workcraft lets you setup your resume and portfolio in a flash!<div>Workcraft lets you setup your resume and portfolio in a flash!<br></div>', '2014-05-01', '2017-04-01');

-- ----------------------------
-- Table structure for `resume_interests`
-- ----------------------------
DROP TABLE IF EXISTS `resume_interests`;
CREATE TABLE `resume_interests` (
  `ResumeInterestId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci,
  `Icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ResumeInterestId`),
  KEY `fk_resume_interests_resume` (`ResumeId`),
  CONSTRAINT `resume_interests_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_interests
-- ----------------------------
INSERT INTO `resume_interests` VALUES ('12', '1', 'Football2', 'I\'ve played football for a local team', 'fa fa-futbol-o');
INSERT INTO `resume_interests` VALUES ('13', '1', 'Coding', 'Yeah, obviously...', 'fa fa-bitcoin');
INSERT INTO `resume_interests` VALUES ('14', '1', 'Music', 'Learning how to produce electronic music', 'fa fa-music');
INSERT INTO `resume_interests` VALUES ('15', '1', 'History', 'I\'ve won a history competition in 8th grade', ' fa fa-bank');
INSERT INTO `resume_interests` VALUES ('16', '1', 'Star Wars', 'My favorite fairy tale', ' fa fa-ra');
INSERT INTO `resume_interests` VALUES ('17', '1', 'Gaming', 'I still play Warcraft and CS 1.6', ' fa fa-gamepad');
INSERT INTO `resume_interests` VALUES ('24', '5', 'Autobus', 'Globus vozi autobus?', 'fa fa-bus');
INSERT INTO `resume_interests` VALUES ('25', '5', 'Nolo fotografi', 'Nolo foto', 'fa fa-camera-retro');
INSERT INTO `resume_interests` VALUES ('26', '5', 'Social media', 'Bad prpr', 'fa fa-hashtag');
INSERT INTO `resume_interests` VALUES ('28', '6', 'Cars', 'I love cars', 'fa fa-automobile');
INSERT INTO `resume_interests` VALUES ('29', '6', 'Social Media', 'I love social media', 'fa fa-hashtag');
INSERT INTO `resume_interests` VALUES ('30', '6', 'Moto GP', 'I love motorcycles too', 'fa fa-motorcycle');

-- ----------------------------
-- Table structure for `resume_languages`
-- ----------------------------
DROP TABLE IF EXISTS `resume_languages`;
CREATE TABLE `resume_languages` (
  `ResumeLanguageId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`ResumeLanguageId`),
  KEY `fk_resume_languages_resume` (`ResumeId`),
  CONSTRAINT `resume_languages_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_languages
-- ----------------------------
INSERT INTO `resume_languages` VALUES ('8', '1', 'Serbian2', '5');
INSERT INTO `resume_languages` VALUES ('9', '1', 'English2', '4');
INSERT INTO `resume_languages` VALUES ('10', '1', 'French', '1');
INSERT INTO `resume_languages` VALUES ('11', '5', 'English', '5');
INSERT INTO `resume_languages` VALUES ('12', '5', 'French', '4');
INSERT INTO `resume_languages` VALUES ('13', '5', 'German', '3');
INSERT INTO `resume_languages` VALUES ('16', '6', 'English', '5');
INSERT INTO `resume_languages` VALUES ('17', '6', 'French', '3');
INSERT INTO `resume_languages` VALUES ('18', '6', 'Spanish', '2');

-- ----------------------------
-- Table structure for `resume_portfolios`
-- ----------------------------
DROP TABLE IF EXISTS `resume_portfolios`;
CREATE TABLE `resume_portfolios` (
  `ResumePortfolioId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Title` text,
  `CaseStudy` text,
  `Category` text,
  `Picture` varchar(255) DEFAULT NULL,
  `ShortDescription` varchar(255) DEFAULT NULL,
  `PublishStatusId` int(11) DEFAULT NULL,
  `ParentId` int(11) DEFAULT NULL,
  `DatePublished` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`ResumePortfolioId`),
  KEY `fk_resume_portfolio_resumes` (`ResumeId`),
  KEY `fk_resume_portfolio_publish_statuses` (`PublishStatusId`),
  KEY `fk_resume_portfolio_resume_portfolios` (`ParentId`),
  CONSTRAINT `fk_resume_portfolio_publish_statuses` FOREIGN KEY (`PublishStatusId`) REFERENCES `publish_statuses` (`PublishStatusId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_resume_portfolio_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_resume_portfolio_resume_portfolios` FOREIGN KEY (`ParentId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_portfolios
-- ----------------------------
INSERT INTO `resume_portfolios` VALUES ('1', '6', 'Sample title', '<p style=\"\"><span style=\"color: rgb(33, 33, 33);\"><span style=\"font-size: 37px; font-weight: bold;\"><span style=\"background-color: rgb(250, 250, 250);\">Hello!</span> I am a 27 years old </span><span style=\"font-size: 37px; font-weight: bold;\">architect</span><span style=\"font-weight: bold; font-size: 37px;\"> </span><span style=\"font-size: 37px; font-weight: bold;\">and&nbsp;</span><span style=\"font-size: 37px; font-weight: bold;\">3d Designer</span><span style=\"font-size: 37px; font-weight: bold;\"> from Los Angeles, California.</span></span></p><p style=\"text-align: left;\"><span style=\"font-size: 18px;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</span></p>', 'architecture, design', 't1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', '2', null, null, null);
INSERT INTO `resume_portfolios` VALUES ('2', '6', 'Another example item', '<p style=\"\"><span style=\"font-size: 37px; font-weight: bold;\"><span style=\"background-color: rgb(250, 250, 250); color: rgb(49, 27, 146);\">Hello!</span> <span style=\"color: rgb(120, 33, 79);\">I am a 27 years old </span></span><span style=\"color: rgb(120, 33, 79);\"><span style=\"font-size: 37px; font-weight: bold;\">architect</span><span style=\"font-weight: bold; font-size: 37px;\"> </span><span style=\"font-size: 37px; font-weight: bold;\">and&nbsp;</span><span style=\"font-size: 37px; font-weight: bold;\">3d Designer</span><span style=\"font-size: 37px; font-weight: bold;\"> from Los Angeles, California.</span></span></p><p style=\"text-align: left;\"><span style=\"font-size: 18px;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</span></p>', 'architecture, design', 't2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', '2', null, null, null);
INSERT INTO `resume_portfolios` VALUES ('3', '6', 'Another example item', '<p style=\"\"><span style=\"font-size: 37px; font-weight: bold;\"><span style=\"background-color: rgb(250, 250, 250); color: rgb(49, 27, 146);\">Hello!</span> <span style=\"color: rgb(120, 33, 79);\">I am a 27 years old </span></span><span style=\"color: rgb(120, 33, 79);\"><span style=\"font-size: 37px; font-weight: bold;\">architect</span><span style=\"font-weight: bold; font-size: 37px;\"> </span><span style=\"font-size: 37px; font-weight: bold;\">and&nbsp;</span><span style=\"font-size: 37px; font-weight: bold;\">3d Designer</span><span style=\"font-size: 37px; font-weight: bold;\"> from Los Angeles, California.</span></span></p><p style=\"text-align: left;\"><span style=\"font-size: 18px;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</span></p>', 'architecture, design', 't3.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', '2', null, null, null);
INSERT INTO `resume_portfolios` VALUES ('4', '6', 'Another example item', '<p style=\"\"><span style=\"font-size: 37px; font-weight: bold;\"><span style=\"background-color: rgb(250, 250, 250); color: rgb(49, 27, 146);\">Hello!</span> <span style=\"color: rgb(120, 33, 79);\">I am a 27 years old </span></span><span style=\"color: rgb(120, 33, 79);\"><span style=\"font-size: 37px; font-weight: bold;\">architect</span><span style=\"font-weight: bold; font-size: 37px;\"> </span><span style=\"font-size: 37px; font-weight: bold;\">and&nbsp;</span><span style=\"font-size: 37px; font-weight: bold;\">3d Designer</span><span style=\"font-size: 37px; font-weight: bold;\"> from Los Angeles, California.</span></span></p><p style=\"text-align: left;\"><span style=\"font-size: 18px;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</span></p>', 'architecture, design', '5.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', '2', null, null, null);
INSERT INTO `resume_portfolios` VALUES ('5', '6', 'Another example item', '<p style=\"\"><span style=\"font-size: 37px; font-weight: bold;\"><span style=\"background-color: rgb(250, 250, 250); color: rgb(49, 27, 146);\">Hello!</span> <span style=\"color: rgb(120, 33, 79);\">I am a 27 years old </span></span><span style=\"color: rgb(120, 33, 79);\"><span style=\"font-size: 37px; font-weight: bold;\">architect</span><span style=\"font-weight: bold; font-size: 37px;\"> </span><span style=\"font-size: 37px; font-weight: bold;\">and&nbsp;</span><span style=\"font-size: 37px; font-weight: bold;\">3d Designer</span><span style=\"font-size: 37px; font-weight: bold;\"> from Los Angeles, California.</span></span></p><p style=\"text-align: left;\"><span style=\"font-size: 18px;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</span></p>', 'architecture, design', '6.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', '2', null, null, null);
INSERT INTO `resume_portfolios` VALUES ('6', '6', 'Another example item', '<p style=\"\"><span style=\"font-size: 37px; font-weight: bold;\"><span style=\"background-color: rgb(250, 250, 250); color: rgb(49, 27, 146);\">Hello!</span> <span style=\"color: rgb(120, 33, 79);\">I am a 27 years old </span></span><span style=\"color: rgb(120, 33, 79);\"><span style=\"font-size: 37px; font-weight: bold;\">architect</span><span style=\"font-weight: bold; font-size: 37px;\"> </span><span style=\"font-size: 37px; font-weight: bold;\">and&nbsp;</span><span style=\"font-size: 37px; font-weight: bold;\">3d Designer</span><span style=\"font-size: 37px; font-weight: bold;\"> from Los Angeles, California.</span></span></p><p style=\"text-align: left;\"><span style=\"font-size: 18px;\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</span></p>', 'architecture, design', '7.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', '2', null, null, null);
INSERT INTO `resume_portfolios` VALUES ('7', '1', 'My sample project', '                                <p><span style=\"font-weight: bold; font-size: 37px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p><p> Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p><p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'architecture, design', 'project-blogok0gya3nga.jpg', 'Lorem ipsum dolor sit amet, nesto kao jos kucam, ali nemam pojma sta. Zato sto je ovo test', '2', null, '2017-10-07 20:52:34', null);
INSERT INTO `resume_portfolios` VALUES ('8', '1', 'New super title!', '                                <p><span style=\"font-weight: bold; font-size: 37px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p><p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p><p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'test, drugi, treci, test, drugi, treci, ', 'project-bloglxn03vyi6f.jpg', 'asflak', '2', null, '2017-10-07 21:11:16', null);
INSERT INTO `resume_portfolios` VALUES ('9', '1', 'Another portfolio project', '                                <p><span style=\"font-size: 37px; font-weight: bold;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></p><p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p><p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'new category, sample, html, new category, sample, html, ', 'project-blogvpatg54tuc.jpg', 'Lorem ipsum dolor sit amet. Consetitur i forgot how it goes', '2', null, '2017-10-08 20:43:44', null);
INSERT INTO `resume_portfolios` VALUES ('10', '10', null, null, null, null, null, '1', null, null, null);
INSERT INTO `resume_portfolios` VALUES ('11', '1', 'cbxcbxc', '                                <p>Write detailed case study of your project here...</p>', 'ssj, ss, gg, ', 'project-blogdusvetjyfw.jpg', 'xcbxcbcx', '2', null, '2017-10-12 15:42:22', null);
INSERT INTO `resume_portfolios` VALUES ('14', '1', null, null, null, null, null, '1', null, null, null);

-- ----------------------------
-- Table structure for `resume_quotes`
-- ----------------------------
DROP TABLE IF EXISTS `resume_quotes`;
CREATE TABLE `resume_quotes` (
  `ResumeQuoteId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Description` text NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Picture` varchar(255) DEFAULT NULL,
  `Position` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ResumeQuoteId`),
  KEY `fk_resume_quotes_resumes` (`ResumeId`),
  CONSTRAINT `fk_resume_quotes_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_quotes
-- ----------------------------
INSERT INTO `resume_quotes` VALUES ('1', '6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', null, 'Samwell Tarly', 'h1.jpg', 'Night\'s Watch');
INSERT INTO `resume_quotes` VALUES ('2', '6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt', null, 'Joanna Doe', 'ex2.jpg', 'Night\'s Watch');
INSERT INTO `resume_quotes` VALUES ('5', '1', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', null, 'John Doe', 'project-blogbthby7az9y.jpg', 'CEO at Sample Inc.');
INSERT INTO `resume_quotes` VALUES ('6', '1', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', null, 'Sam Doe', 'project-blogmm7ns8ydoa.jpg', 'Project manager at Test LLC');

-- ----------------------------
-- Table structure for `resume_sections`
-- ----------------------------
DROP TABLE IF EXISTS `resume_sections`;
CREATE TABLE `resume_sections` (
  `ResumeSectionId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `ContentSectionId` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ResumeSectionId`),
  KEY `fk_resume_sections_resumes` (`ResumeId`),
  CONSTRAINT `fk_resume_sections_resumes` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume_sections
-- ----------------------------
INSERT INTO `resume_sections` VALUES ('1', '1', '1', 'Education');
INSERT INTO `resume_sections` VALUES ('2', '1', '2', 'Experience');
INSERT INTO `resume_sections` VALUES ('4', '1', '4', 'Languages');
INSERT INTO `resume_sections` VALUES ('5', '1', '3', 'Skills');
INSERT INTO `resume_sections` VALUES ('8', '1', '7', 'Interests');
INSERT INTO `resume_sections` VALUES ('9', '5', '1', 'Education');
INSERT INTO `resume_sections` VALUES ('10', '5', '2', 'Experience');
INSERT INTO `resume_sections` VALUES ('11', '5', '3', 'Skills');
INSERT INTO `resume_sections` VALUES ('12', '5', '4', 'Languages');
INSERT INTO `resume_sections` VALUES ('13', '5', '5', 'Certificates');
INSERT INTO `resume_sections` VALUES ('14', '5', '6', 'Volunteering');
INSERT INTO `resume_sections` VALUES ('15', '5', '7', 'Interests');
INSERT INTO `resume_sections` VALUES ('16', '6', '1', 'Education');
INSERT INTO `resume_sections` VALUES ('17', '6', '2', 'Experience');
INSERT INTO `resume_sections` VALUES ('18', '6', '3', 'Skills');
INSERT INTO `resume_sections` VALUES ('19', '6', '4', 'Languages');
INSERT INTO `resume_sections` VALUES ('23', '6', '8', 'Portfolio');
INSERT INTO `resume_sections` VALUES ('24', '6', '9', 'Quotes');
INSERT INTO `resume_sections` VALUES ('25', '6', '7', 'Interests');
INSERT INTO `resume_sections` VALUES ('26', '9', '1', 'Education');
INSERT INTO `resume_sections` VALUES ('27', '9', '2', 'Experience');
INSERT INTO `resume_sections` VALUES ('28', '1', '8', 'Portfolio');
INSERT INTO `resume_sections` VALUES ('29', '1', '9', 'Quotes');
INSERT INTO `resume_sections` VALUES ('31', '10', '1', 'Education');
INSERT INTO `resume_sections` VALUES ('32', '10', '2', 'Experience');
INSERT INTO `resume_sections` VALUES ('33', '10', '9', 'Quotes');
INSERT INTO `resume_sections` VALUES ('34', '10', '8', 'Portfolio');
INSERT INTO `resume_sections` VALUES ('35', '14', '1', 'Education');

-- ----------------------------
-- Table structure for `resume_skills`
-- ----------------------------
DROP TABLE IF EXISTS `resume_skills`;
CREATE TABLE `resume_skills` (
  `ResumeSkillId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`ResumeSkillId`),
  KEY `fk_resume_skills_resume` (`ResumeId`),
  CONSTRAINT `resume_skills_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_skills
-- ----------------------------
INSERT INTO `resume_skills` VALUES ('16', '1', 'HTML5\n                    \n                    \n                    \n                    \n                    \n                    \n                    \n                    \n                    \n                    \n                    \n                  ', '92');
INSERT INTO `resume_skills` VALUES ('17', '1', '\n                    \n                    CSS2', '76');
INSERT INTO `resume_skills` VALUES ('19', '1', '\n                    \n                    Javascript\n                    \n                    \n                    \n                    \n                    \n                    \n                    \n                    \n                    \n             ', '67');
INSERT INTO `resume_skills` VALUES ('20', '1', '\n                    \n                    PHP5', '77');
INSERT INTO `resume_skills` VALUES ('22', '1', '\n                    UX/UI Design\n                ', '85');
INSERT INTO `resume_skills` VALUES ('23', '1', '\n                    \n                    \n                    \n                    \n                    \n                    \n                    MySQL\n                \n                \n                \n                \n                \n                \n', '80');
INSERT INTO `resume_skills` VALUES ('30', '5', 'Sample', '84');
INSERT INTO `resume_skills` VALUES ('31', '5', 'asdfa', '91');
INSERT INTO `resume_skills` VALUES ('32', '5', 'aasgasb', '63');
INSERT INTO `resume_skills` VALUES ('33', '5', 'afsas', '85');
INSERT INTO `resume_skills` VALUES ('44', '6', '3d Modeling', '88');
INSERT INTO `resume_skills` VALUES ('45', '6', 'Architectural drawing', '92');
INSERT INTO `resume_skills` VALUES ('46', '6', 'Another skill', '66');
INSERT INTO `resume_skills` VALUES ('47', '6', 'Umem', '82');
INSERT INTO `resume_skills` VALUES ('48', '6', 'Nesto', '75');
INSERT INTO `resume_skills` VALUES ('49', '6', 'Opet', '69');

-- ----------------------------
-- Table structure for `resume_volunteer_works`
-- ----------------------------
DROP TABLE IF EXISTS `resume_volunteer_works`;
CREATE TABLE `resume_volunteer_works` (
  `ResumeVolunteerWorkId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumeId` int(11) NOT NULL,
  `Position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Organisation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date DEFAULT NULL,
  PRIMARY KEY (`ResumeVolunteerWorkId`),
  KEY `fk_resume_volunteers_resume` (`ResumeId`),
  CONSTRAINT `resume_volunteer_works_ibfk_1` FOREIGN KEY (`ResumeId`) REFERENCES `resumes` (`ResumeId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of resume_volunteer_works
-- ----------------------------
INSERT INTO `resume_volunteer_works` VALUES ('6', '1', 'Sample position', 'SSPF', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.222', '1996-01-01', '2016-01-01');
INSERT INTO `resume_volunteer_works` VALUES ('9', '5', 'sdfas afsfqr&nbsp;', 'fafas', 'asfasf asf asfqwetgweg wgw hwerrh erwhwerh rtherhqerherqhr esdg erhbeqrhbrs sdgsadhrah hr', '2015-06-01', null);

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `Protected` tinyint(4) NOT NULL,
  `Active` tinyint(4) NOT NULL,
  `Caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '1', '1', 'Administrator');
INSERT INTO `roles` VALUES ('2', '1', '1', 'User');
INSERT INTO `roles` VALUES ('3', '1', '1', 'Visitor');

-- ----------------------------
-- Table structure for `role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions` (
  `RolePermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) NOT NULL,
  `PermissionId` int(11) NOT NULL,
  `Protected` smallint(6) NOT NULL,
  PRIMARY KEY (`RolePermissionId`),
  KEY `FK_RolePermissions_Permissions` (`PermissionId`) USING BTREE,
  KEY `FK_RolePermissions_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `role_permissions_ibfk_1` FOREIGN KEY (`PermissionId`) REFERENCES `permissions` (`PermissionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permissions_ibfk_2` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES ('1', '1', '1', '0');
INSERT INTO `role_permissions` VALUES ('2', '1', '20', '0');
INSERT INTO `role_permissions` VALUES ('3', '2', '20', '0');
INSERT INTO `role_permissions` VALUES ('4', '1', '2', '0');
INSERT INTO `role_permissions` VALUES ('5', '1', '4', '0');
INSERT INTO `role_permissions` VALUES ('6', '1', '5', '0');
INSERT INTO `role_permissions` VALUES ('7', '1', '3', '0');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `UserId` int(10) NOT NULL AUTO_INCREMENT,
  `Email` varchar(250) NOT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Image` varchar(250) DEFAULT NULL,
  `RegistrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ConfirmRegistration` tinyint(1) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `UniqueEmail` (`Email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'john@doe.com', '$2a$10$9dRxNcvXVsK.Etoyx9yOoO2/GdYWJWzQ4eysCy3LHMbyPVRNeDRoy', '', '2017-02-08 16:42:33', '1', '1');
INSERT INTO `users` VALUES ('2', 'm132@m.com', '$2a$10$56pIhjj5eQkI11YKm/qfNOCnqJ19hcnALgg/j2Pj9EpsSJUin.GRm', null, '2017-09-21 15:58:34', '0', '0');
INSERT INTO `users` VALUES ('3', 'm1@m.com', '$2a$10$jLJgZbgni2WYTN9mX6vkN.AxLA9kfd1VQpV5FagdzruTVIHZ/6yVC', null, '2017-09-21 16:04:29', '0', '0');
INSERT INTO `users` VALUES ('4', 'test1@test.com', '$2a$10$d1hLOXuAZfYmAfg0M1tTueUluAiyB7STvR8s3j/kFlL4ca6Ydop9G', null, '2017-09-21 16:05:32', '1', '1');
INSERT INTO `users` VALUES ('5', 'test2@test.com', '$2a$10$H9KkR/BNBSywFRVvF.fO1.QjIMe/uvNhrnyv3Jh3mtjJFl50pvFse', null, '2017-09-21 16:17:16', '1', '1');
INSERT INTO `users` VALUES ('6', 'mihajlo.ljubenovic@ubrium.com', '$2a$10$z6N8I9yoGdXlzgy0PBkegeBtEmfuAmetznrdY/reXI5fH73S.Roj6', null, '2017-09-21 18:42:51', '1', '1');
INSERT INTO `users` VALUES ('7', 'wc1@wc.com', '$2a$10$zIlm8rrKCLV/qcWoezmzXuQRWL.O1D1awTM9LF/kTb4fYlhjn6TB2', null, '2017-09-27 22:15:04', '1', '1');
INSERT INTO `users` VALUES ('8', 'm2@m.com', '$2a$10$kFKskc/wBr0SamY30cBzX.YtpD4eCIbFPt.dIAPbjs/BXut/0TGYe', null, '2017-09-27 22:44:16', '1', '1');
INSERT INTO `users` VALUES ('9', 'm3@m.com', '$2a$10$ubLqVQBJigjod.tADnrag.VkkxvNNkhg8TXIEwBQOduoYmOnwsESe', null, '2017-09-27 22:53:39', '1', '1');
INSERT INTO `users` VALUES ('10', 'm4@m.com', '$2a$10$oY2dy8au9WsWFRjySOqShOmJPteZ//nbeZ0FPakIE3PrvtZ.Xixym', null, '2017-09-27 22:58:01', '1', '1');
INSERT INTO `users` VALUES ('11', 'm5@m.com', '$2a$10$7zrsz6XsnhkvA5FLy6.Da./SYq7t4z23Jnt87OKXbMn1x2IcTrXe.', null, '2017-10-02 15:57:08', '1', '1');
INSERT INTO `users` VALUES ('12', 'm6@m.com', '$2a$10$u2IzM2KNeRd4XH/x3BF4/OYRtt/BMtyDYnP5ZCla6cwbEWJd/IKFO', null, '2017-10-03 01:22:15', '1', '1');
INSERT INTO `users` VALUES ('13', 'm7@m.com', '$2a$10$QvjaE9nPbQ5DJCGULXhwZOFLPfEAfk5B.4Md6c5suFuIAJ9SPBhsi', null, '2017-10-04 14:29:53', '1', '1');
INSERT INTO `users` VALUES ('14', 'hadza@test.com', '$2a$10$uoBnGQYiOveaxU0HkXKVoOQb1aCA1JflcP6zsxLliXfedQyAbLfn.', null, '2017-10-04 23:13:16', '1', '1');
INSERT INTO `users` VALUES ('15', 'm8@m.com', '$2a$10$HJk.dr7u9ygeVPTboNN61umYZO8lqnCEnvH..zF6vVibtwH5bqCiS', null, '2017-10-13 02:36:19', '1', '1');
INSERT INTO `users` VALUES ('16', 'm9@m.com', '$2a$10$EhPd25N0rCkDecGTH2Tx4Oww4Uw0JS.gKHE2Gu330jIXtOxWtKWHS', null, '2017-10-13 02:37:55', '1', '1');
INSERT INTO `users` VALUES ('17', 'm10@m.com', '$2a$10$208Rc11QQGAAQP4vYDNFI.1CPruKM2MJuKHkuX3WeJuAzt4Adnc6m', null, '2017-10-13 02:43:10', '1', '1');
INSERT INTO `users` VALUES ('18', 'm11@m.com', '$2a$10$VLNhnZjpaqeSrpg3a/ndZutlcwBXvSw1uRGwZUjlV5GHvXmf9rzP6', null, '2017-10-13 03:26:26', '1', '1');

-- ----------------------------
-- Table structure for `user_access_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `user_access_tokens`;
CREATE TABLE `user_access_tokens` (
  `UserAccessTokenId` int(11) NOT NULL AUTO_INCREMENT,
  `Token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UserId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EndDate` datetime DEFAULT NULL,
  `AccessTokenTypeId` int(10) NOT NULL,
  PRIMARY KEY (`UserAccessTokenId`),
  UNIQUE KEY `Unique_Token` (`Token`) USING BTREE,
  KEY `FK_UserAccessTokens_Users` (`UserId`) USING BTREE,
  KEY `TypeId` (`AccessTokenTypeId`) USING BTREE,
  CONSTRAINT `user_access_tokens_ibfk_1` FOREIGN KEY (`AccessTokenTypeId`) REFERENCES `access_token_types` (`AccessTokenTypeId`) ON UPDATE CASCADE,
  CONSTRAINT `user_access_tokens_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for `user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `UserRoleId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  PRIMARY KEY (`UserRoleId`),
  KEY `FK_UserRoles_Users` (`UserId`) USING BTREE,
  KEY `FK_UserRoles_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('1', '1', '1');
INSERT INTO `user_roles` VALUES ('2', '2', '2');
INSERT INTO `user_roles` VALUES ('3', '3', '2');
INSERT INTO `user_roles` VALUES ('4', '4', '2');
INSERT INTO `user_roles` VALUES ('5', '5', '2');
INSERT INTO `user_roles` VALUES ('6', '6', '2');
INSERT INTO `user_roles` VALUES ('7', '7', '2');
INSERT INTO `user_roles` VALUES ('8', '8', '2');
INSERT INTO `user_roles` VALUES ('9', '9', '2');
INSERT INTO `user_roles` VALUES ('10', '10', '2');
INSERT INTO `user_roles` VALUES ('11', '11', '2');
INSERT INTO `user_roles` VALUES ('12', '12', '2');
INSERT INTO `user_roles` VALUES ('13', '13', '2');
INSERT INTO `user_roles` VALUES ('14', '14', '2');
INSERT INTO `user_roles` VALUES ('15', '15', '2');
INSERT INTO `user_roles` VALUES ('16', '16', '2');
INSERT INTO `user_roles` VALUES ('17', '17', '2');
INSERT INTO `user_roles` VALUES ('18', '18', '2');
