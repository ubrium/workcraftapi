<?php


namespace Data\Repositories;

use Business\Enums\RolesEnum;
use Business\Models\UserModel;
use Data\Database\MysqliDb;

/**
 * Class UsersRepository
 * @package Data\Repositories
 * @method static UserModel[] Get
 * @method static UserModel GetOne
 */
class UsersRepository extends BaseRepository {

	const COLUMN_USER_ID = 'UserId';
	const COLUMN_USERNAME = 'Username';
	const COLUMN_NAME = 'Name';
	const COLUMN_EMAIL = "Email";
	const COLUMN_REGISTRATION_DATE = 'RegistrationDate';
	const COLUMN_PASSWORD = 'Password';
	const COLUMN_IMAGE = 'Image';
	const COLUMN_CONFIRMATION_MAIL = 'ConfirmationMail';
	const COLUMN_ACTIVE = 'Active';

	public static function GetUserPermissions($userId) {
		$userPermissions = [];

		$db = MysqliDb::getInstance();

		$db->join(UserRolesRepository::GetTableName() . " ur", "u.UserId = ur.UserId");
		$db->join(RolePermissionsRepository::GetTableName() . " rp", "rp.RoleId = ur.RoleId");
		$db->where("u.UserId", $userId);

		$result = $db->get(self::GetTableName() . " u", null, "DISTINCT(rp.PermissionId)");

		if (count($result) > 0) {
			foreach ($result as $permission) {
				$userPermissions[] = $permission["PermissionId"];
			}
		}

		return $userPermissions;
	}

	public static function GetAdmins() {
		$admins = [];

		$db = MysqliDb::getInstance();

		$db->join(UserRolesRepository::GetTableName() . " ur", "u.UserId = ur.UserId");
		$db->where("ur.RoleId", RolesEnum::Admin);

		$result = $db->get(self::GetTableName() . " u");

		if (count($result) > 0) {
			foreach ($result as $row) {
				$admin = new UserModel();
				$admin->UserId = $row[self::COLUMN_USER_ID];
				$admin->Username = $row[self::COLUMN_USERNAME];
				$admin->Email = $row[self::COLUMN_EMAIL];
				$admin->Image = $row[self::COLUMN_IMAGE];
				$admin->Name = $row[self::COLUMN_NAME];
				$admin->RegistrationDate = $row[self::COLUMN_REGISTRATION_DATE];
				$admins[] = $admin;
			}
		}

		return $admins;
	}

	public static function CountAdmins() {
		$db = MysqliDb::getInstance();

		$db->join(UserRolesRepository::GetTableName() . " ur", "u.UserId = ur.UserId");
		$db->where("ur.RoleId", RolesEnum::Admin);

		$result = $db->getOne(self::GetTableName() . " u", "COUNT(u.UserId) AS AdminCount");

		return $result["AdminCount"];
	}

}