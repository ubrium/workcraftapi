<?php

namespace Data\Repositories;

use Business\Models\UserAccessTokenModel;
use Data\Database\MysqliDb;

/**
 * Class UserAccessTokensRepository
 * @package Data\Repositories
 * @method static UserAccessTokenModel[] Get
 * @method static UserAccessTokenModel GetOne
 */
class UserAccessTokensRepository extends BaseRepository {

    const COLUMN_USER_ACCESS_TOKEN_ID = 'UserAccessTokenId';
    const COLUMN_TOKEN = 'Token';
    const COLUMN_USER_ID = 'UserId';
    const COLUMN_START_DATE = 'StartDate';
    const COLUMN_END_DATE = 'EndDate';
    const COLUMN_ACCESS_TOKEN_TYPE_ID = 'AccessTokenTypeId';


    public static function RemoveToken($userId, $accessTokenTypeId) {
        $db = MysqliDb::getInstance();

        $db->where(self::COLUMN_USER_ID, $userId);
        $db->where(self::COLUMN_END_DATE . " IS NULL");
        $db->where(self::COLUMN_ACCESS_TOKEN_TYPE_ID, $accessTokenTypeId);
        return $db->update(self::GetTableName(), [
            self::COLUMN_END_DATE => date("Y-m-d H:i:s")
        ]);
    }
}