<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.21
 */

namespace Data\Repositories;


use Data\Database\Protocol\Join;

class ResumeSectionsRepository extends BaseRepository {
    public static function Joins()
    {
        return [new Join(new ContentSectionsRepository(), null, null, [], "LEFT")];
    }
}