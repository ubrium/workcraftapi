<?php


namespace Data\Repositories;
use Data\Database\Protocol\Join;
use Business\Models\ResumeModel;

/**
 * Class ResumesRepository
 * @package Data\Repositories
 * @method static ResumeModel[] Get
 * @method static ResumeModel GetOne
 */
class ResumesRepository extends BaseRepository {

	/*public static function Joins(){
		return [new Join(new ResumeBasicsRepository())];
	}*/
}