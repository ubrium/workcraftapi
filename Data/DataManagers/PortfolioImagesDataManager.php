<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;


use Data\Repositories\PortfolioImagesRepository;


class PortfolioImagesDataManager {

	public static function GetPortfolioItemImages($resumePortfolioId) {
		return PortfolioImagesRepository::Get(["ResumePortfolioId" => $resumePortfolioId]);
	}

	public static function InsertImage($model) {
		return PortfolioImagesRepository::Insert($model);
	}

	public static function DeleteImage($portfolioImageId) {
		return PortfolioImagesRepository::Delete($portfolioImageId);
	}
}