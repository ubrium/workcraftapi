<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;



use Data\Repositories\ResumeExperiencesRepository;

class ResumeExperiencesDataManager {

	public static function GetResumeExperiencesByResumeId($resumeId) {
		return ResumeExperiencesRepository::Get(["ResumeId" => $resumeId], ["DateFrom" => "DESC"]);
	}

	public static function InsertResumeExperience($model) {
		return ResumeExperiencesRepository::Insert($model);
	}

	public static function UpdateResumeExperience($model) {
		return ResumeExperiencesRepository::Update($model);
	}

	public static function DeleteResumeExperience($resumeExperienceId) {
		return ResumeExperiencesRepository::Delete($resumeExperienceId);
	}

}