<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;

use Data\Repositories\ResumeInterestsRepository;

class ResumeInterestsDataManager {

	public static function GetResumeInterestsByResumeId($resumeId) {
		return ResumeInterestsRepository::Get(["ResumeId" => $resumeId]);
	}

	public static function InsertResumeInterest($model) {
		return ResumeInterestsRepository::Insert($model);
	}

	public static function UpdateResumeInterest($model) {
		return ResumeInterestsRepository::Update($model);
	}

	public static function DeleteResumeInterest($resumeInterestId) {
		return ResumeInterestsRepository::Delete($resumeInterestId);
	}
}