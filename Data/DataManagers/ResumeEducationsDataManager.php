<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;



use Data\Repositories\ResumeEducationsRepository;

class ResumeEducationsDataManager {

	public static function GetResumeEducationByResumeId($resumeId) {
		return ResumeEducationsRepository::Get(["ResumeId" => $resumeId], ["DateFrom" => "DESC"]);
	}

	public static function InsertResumeEducation($model) {
		return ResumeEducationsRepository::Insert($model);
	}

	public static function UpdateResumeEducation($model) {
		return ResumeEducationsRepository::Update($model);
	}

	public static function DeleteResumeEducation($resumeEducationId) {
		return ResumeEducationsRepository::Delete($resumeEducationId);
	}

}