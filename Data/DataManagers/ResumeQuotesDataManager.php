<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;


use Data\Repositories\ResumeQuotesRepository;

class ResumeQuotesDataManager {

	public static function GetResumeQuotesByResumeId($resumeId) {
		return ResumeQuotesRepository::Get(["ResumeId" => $resumeId]);
	}

	public static function InsertResumeQuote($model) {
		return ResumeQuotesRepository::Insert($model);
	}

	public static function UpdateResumeQuote($model) {
		return ResumeQuotesRepository::Update($model);
	}

	public static function DeleteResumeQuote($resumeQuoteId) {
		return ResumeQuotesRepository::Delete($resumeQuoteId);
	}
}