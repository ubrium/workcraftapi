<?php

namespace Data\DataManagers;

use Data\Repositories\CommentsRepository;

class CommentsDataManager {


	public static function GetArticleComment($articleId) {
		return CommentsRepository::GetOne(["ArticleId" => $articleId]);
	}

	public static function GetArticleComments($articleId) {
		return CommentsRepository::Get(["ArticleId" => $articleId]);
	}

	public static function InsertArticleComment($model) {
		return CommentsRepository::Insert($model);
	}

	public static function UpdateArticleComment($model) {
		return CommentsRepository::Update($model);
	}

	public static function DeleteArticleComment($commentId) {
		return CommentsRepository::Delete($commentId);
	}


}