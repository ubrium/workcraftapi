<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;




use Data\Repositories\ResumeLanguagesRepository;

class ResumeLanguagesDataManager {

	public static function GetResumeLanguagesByResumeId($resumeId) {
		return ResumeLanguagesRepository::Get(["ResumeId" => $resumeId]);
	}

	public static function InsertResumeLanguage($model) {
		return ResumeLanguagesRepository::Insert($model);
	}

	public static function UpdateResumeLanguage($model) {
		return ResumeLanguagesRepository::Update($model);
	}

	public static function DeleteResumeLanguage($resumeLanguageId) {
		return ResumeLanguagesRepository::Delete($resumeLanguageId);
	}

}