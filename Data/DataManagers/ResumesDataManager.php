<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;


use Data\Repositories\ResumesRepository;

class ResumesDataManager {

	public static function GetResumeById($resumeId) {

		return ResumesRepository::GetOne(["ResumeId" => $resumeId]);
	}

	public static function GetResumeByIdAndLangCode($userId, $langCode) {

		return ResumesRepository::GetOne(["UserId" => $userId, "LanguageCode" => $langCode]);
	}

	public static function GetResumeDefault($userId, $statusId) {
		return ResumesRepository::GetOne(["UserId" => $userId, "StatusId" => $statusId]);
	}

	public static function GetUserResumes($userId) {
		return ResumesRepository::Get(["UserId" => $userId]);
	}

	public static function DeleteResume($resumeId) {
		return ResumesRepository::Delete($resumeId);
	}

	public static function UpdateResume($model) {


		return ResumesRepository::Update($model);
	}

	public static function InsertResume($model) {

		return ResumesRepository::Insert($model);
	}


	public static function GetAboutByResumeId($resumeId) {
		return ResumesRepository::GetOne(["ResumeId" => $resumeId]);
	}

	public static function GetResumes() {
		return ResumesRepository::Get();
	}

	public static function CountResumes() {
		return ResumesRepository::Count();
	}


}