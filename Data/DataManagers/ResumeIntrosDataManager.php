<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;

use Data\Repositories\ResumeIntrosRepository;

class ResumeIntrosDataManager {

	public static function GetResumeIntroByResumeId($resumeId) {
		return ResumeIntrosRepository::GetOne(["ResumeId" => $resumeId]);
	}

	public static function InsertResumeIntro($model) {
		return ResumeIntrosRepository::Insert($model);
	}

	public static function UpdateResumeIntro($model) {
		return ResumeIntrosRepository::Update($model);
	}

	public static function DeleteResumeIntro($resumeIntroId) {
		return ResumeIntrosRepository::Delete($resumeIntroId);
	}

}