<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;


use Data\Repositories\ResumeBasicsRepository;

class ResumeBasicsDataManager {

	public static function GetResumeBasicsDefault($resumeId) {

		return ResumeBasicsRepository::GetOne(["ResumeId" => $resumeId]);
	}

	public static function UpdateResumeBasics($model) {

		return ResumeBasicsRepository::Update($model);
	}

	public static function InsertResumeBasics($model) {

		return ResumeBasicsRepository::Insert($model);
	}


}