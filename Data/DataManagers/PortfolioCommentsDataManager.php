<?php

namespace Data\DataManagers;

use Data\Repositories\PortfolioCommentsRepository;

class PortfolioCommentsDataManager {


	public static function GetPortfolioComment($portfolioId) {
		return PortfolioCommentsRepository::GetOne(["PortfolioId" => $portfolioId]);
	}

	public static function GetPortfolioComments($portfolioId) {
		return PortfolioCommentsRepository::Get(["PortfolioId" => $portfolioId]);
	}

	public static function InsertPortfolioComment($model) {
		return PortfolioCommentsRepository::Insert($model);
	}

	public static function UpdatePortfolioeComment($model) {
		return PortfolioCommentsRepository::Update($model);
	}

	public static function DeletePortfolioComment($commentId) {
		return PortfolioCommentsRepository::Delete($commentId);
	}


}