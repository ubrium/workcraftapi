<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;


use Data\Repositories\ResumePortfoliosRepository;

class ResumePortfoliosDataManager {

	public static function GetResumePortfoliosByResumeId($resumeId, $status = null) {
		return ResumePortfoliosRepository::Get(["ResumeId" => $resumeId, "PublishStatusId" => $status]);
	}

	public static function GetPortfolioItem($resumePortfolioId) {
		return ResumePortfoliosRepository::GetOne(["ResumePortfolioId" => $resumePortfolioId]);
	}

	public static function GetPortfolioNew($resumeId) {
		return ResumePortfoliosRepository::GetOne(["ResumeId" => $resumeId, "PublishStatusId" => 1]);
	}

	public static function InsertPortfolioNew($model) {
		return ResumePortfoliosRepository::Insert($model);
	}

	public static function PublishPortfolioItem($model) {
		return ResumePortfoliosRepository::Update($model);
	}

	public static function InsertResumePortfolio($model) {
		return ResumePortfoliosRepository::Insert($model);
	}

	public static function UpdateResumePortfolio($model) {
		return ResumePortfoliosRepository::Update($model);
	}

	public static function DeleteResumePortfolio($resumeQuoteId) {
		return ResumePortfoliosRepository::Delete($resumeQuoteId);
	}
}