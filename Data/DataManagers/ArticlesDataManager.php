<?php

namespace Data\DataManagers;

use Business\ApiControllers\ArticlesApiController;
use Data\Repositories\ArticlesRepository;

class ArticlesDataManager {

	public static function GetArticle($articleId) {
		return ArticlesRepository::GetOne(["ArticleId" => $articleId]);
	}

	public static function GetArticles() {
		return ArticlesRepository::Get();
	}

	public static function InsertArticle($model) {
		return ArticlesRepository::Insert($model);
	}

	public static function UpdateArticle($model) {
		return ArticlesRepository::Update($model);
	}

	public static function DeleteArticle($articleId) {
		return ArticlesRepository::Delete($articleId);
	}


	public static function GetArticleComment($articleId) {
		return ArticlesRepository::GetOne(["ArticleId" => $articleId]);
	}

	public static function GetArticleComments() {
		return ArticlesRepository::Get();
	}

	public static function InsertArticleComment($model) {
		return ArticlesRepository::Insert($model);
	}

	public static function UpdateArticleComment($model) {
		return ArticlesRepository::Update($model);
	}

	public static function DeleteArticleComment($articleId) {
		return ArticlesRepository::Delete($articleId);
	}

	public static function CountArticles() {
		return ArticlesRepository::Count();
	}


}