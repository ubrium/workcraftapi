<?php

namespace Data\DataManagers;

use Data\Repositories\JobsRepository;

class JobsDataManager {

	public static function GetJob($jobId) {
		return JobsRepository::GetOne(["JobId" => $jobId]);
	}

	public static function GetJobs() {
		return JobsRepository::Get();
	}

	public static function InsertJob($model) {
		return JobsRepository::Insert($model);
	}

	public static function UpdateJob($model) {
		return JobsRepository::Update($model);
	}

	public static function DeleteJob($jobId) {
		return JobsRepository::Delete($jobId);
	}

	public static function CountJobs() {
		return JobsRepository::Count();
	}


}