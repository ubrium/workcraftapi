<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;

use Data\Repositories\ResumeVolunteerWorksRepository;

class ResumeVolunteerWorksDataManager {

	public static function GetResumeVolunteerWorksByResumeId($resumeId) {
		return ResumeVolunteerWorksRepository::Get(["ResumeId" => $resumeId], ["DateFrom" => "DESC"]);
	}

	public static function InsertResumeVolunteerWork($model) {
		return ResumeVolunteerWorksRepository::Insert($model);
	}

	public static function UpdateResumeVolunteerWork($model) {
		return ResumeVolunteerWorksRepository::Update($model);
	}

	public static function DeleteResumeVolunteerWork($resumeVolunteerWorkId) {
		return ResumeVolunteerWorksRepository::Delete($resumeVolunteerWorkId);
	}

}