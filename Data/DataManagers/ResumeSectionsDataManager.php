<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.13
 */

namespace Data\DataManagers;



use Data\Repositories\ResumeSectionsRepository;

class ResumeSectionsDataManager {

	public static function GetResumeSectionsByResumeId($resumeId) {
		return ResumeSectionsRepository::Get(["ResumeId" => $resumeId]);
	}

	public static function InsertResumeSection($model) {
		return ResumeSectionsRepository::Insert($model);
	}

	public static function DeleteResumeSection($resumeSectionId) {
		return ResumeSectionsRepository::Delete($resumeSectionId);
	}

}