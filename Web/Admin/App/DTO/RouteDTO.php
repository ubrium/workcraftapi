<?php

/**
 * Class RouteDTO
 *
 * @property string $Pattern
 * @property string $Controller
 * @property string $Action
 * @property array $Params
 */
class RouteDTO {
    public $Pattern;
    public $Controller;
    public $Action;
    public $Params;

    public function __construct($pattern, $controller, $action = false, $params = array()) {
        $this->Pattern = $pattern;
        $this->Controller = $controller;
        $this->Action = $action;
        $this->Params = $params;
    }
} 