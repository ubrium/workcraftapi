<?php

/**
 * Class StatisticDTO
 * @property integer $UserCount
 * @property integer $AdminCount
 * @property integer $ResumeCount
 * @property integer $ArticlesCount
 * @property integer $PortfolioCount
 * @property integer $JobsCount
 */
class StatisticDTO {

	public $UserCount;
	public $AdminCount;
	public $ResumeCount;
	public $ArticlesCount;
	public $PortfolioCount;
	public $JobsCount;

}