<?php

use Business\ApiControllers\JobsApiController;

class JobsController extends MVCController {

	public function GetJobs() {

		$model = new JobsViewModel();
		$model->Jobs = JobsApiController::GetJobs();

		$this->RenderView("Jobs/Jobs", ["model" => $model]);
	}
}