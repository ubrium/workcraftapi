<?php


use Business\ApiControllers\ArticlesApiController;
use Business\ApiControllers\JobsApiController;
use Business\ApiControllers\ResumesApiController;
use Business\ApiControllers\UsersApiController;
use Business\Enums\PermissionsEnum;

class HomeController extends MVCController {

	public function GetIndex($permissions = [PermissionsEnum::Dashboard]) {
		$model = new DashboardViewModel();
		$statisticsDto = new StatisticDTO();
		$statisticsDto->UserCount = UsersApiController::CountUsers();
		$statisticsDto->AdminCount = UsersApiController::CountAdmins();
		$statisticsDto->ResumeCount = ResumesApiController::CountResumes();
		$statisticsDto->JobsCount = JobsApiController::CountJobs();
		$statisticsDto->ArticlesCount = ArticlesApiController::CountArticles();

		$model->Statistics = $statisticsDto;

		$this->RenderView("Home/Dashboard", ["model" => $model]);
	}
}