<?php
use Business\ApiControllers\ResumesApiController;

class ResumesController extends MVCController {

	public function GetResumes() {
		$model = new ResumesViewModel();
		$model->Resumes = ResumesApiController::GetResumes();

		$this->RenderView("Resumes/Resumes", ["model" => $model]);
	}
}