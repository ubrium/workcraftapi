<?php

class Config
{
    const baseurl = ADMIN_URL;
    const maintenanceMode = MAINTENANCE_MODE_ADMIN;
    const debugMode = DEBUG_MODE_ADMIN;
    const useCache = USE_CACHE_ADMIN;
}