<?php

/**
 * Class RoutesConfig
 */
class RoutesConfig {

	/**
	 * @return RouteDTO[]
	 */
	public static function GetRoutes() {
		$routes = array(
			"example" => new RouteDTO("example/{Id:integer}/{Slug:string}", "Home", "Example"),

			// Home
			"home" => new RouteDTO("home", "Home", "Home"),
			"cdn" => new RouteDTO("cdn", "Home", "CDN"),
			"index" => new RouteDTO("", "Home", "Home"),
			"about" => new RouteDTO("about", "Pages", "About"),

			// Resume/portfolio
			"editor" => new RouteDTO("editor/{langCode:string}", "Resumes", "Resume"),
			"editor-no-code" => new RouteDTO("editor", "Resumes", "Resume"),
			"user-resume-id" => new RouteDTO("user/{userId:integer}", "Resumes", "Resume"),
			"user-resume-lang-code" => new RouteDTO("user/{userId:integer}/{langCode:string}", "Resumes", "Resume"),

			"select-primary-language" => new RouteDTO("select-primary-language", "Resumes", "PrimaryLanguage"),
			"create-primary-resume" => new RouteDTO("create-primary-resume/{langCode:string}", "Resumes", "CreateResume"),
			"create-new-resume" => new RouteDTO("create-new-resume/{langCode:string}", "Resumes", "CreateNewResume"),
			"update-resume" => new RouteDTO("update-resume", "Resumes", "UpdateResume"),

			"update-resume-basics" => new RouteDTO("update-resume-basics", "Resumes", "UpdateResumeBasics"),
			"add-resume-intro" => new RouteDTO("add-resume-intro", "Resumes", "AddResumeIntro"),
			"update-resume-intro" => new RouteDTO("update-resume-intro", "Resumes", "UpdateResumeIntro"),
			"upload-resume-image" => new RouteDTO("upload-resume-image", "Resumes", "UserProfileImage"),
			"upload-intro-image" => new RouteDTO("upload-intro-image", "Resumes", "IntroImage"),

			"add-resume-section" => new RouteDTO("add-resume-section", "Resumes", "AddResumeSection"),
			"delete-resume-section" => new RouteDTO("delete-resume-section", "Resumes", "DeleteResumeSection"),

			// Education
			"add-resume-education" => new RouteDTO("add-resume-education", "Resumes", "AddResumeEducation"),
			"update-resume-education" => new RouteDTO("update-resume-education", "Resumes", "UpdateResumeEducation"),
			"delete-resume-education" => new RouteDTO("delete-resume-education", "Resumes", "DeleteResumeEducation"),

			// Experience
			"add-resume-experience" => new RouteDTO("add-resume-experience", "Resumes", "AddResumeExperience"),
			"update-resume-experience" => new RouteDTO("update-resume-experience", "Resumes", "UpdateResumeExperience"),
			"delete-resume-experience" => new RouteDTO("delete-resume-experience", "Resumes", "DeleteResumeExperience"),

			// Language
			"add-resume-language" => new RouteDTO("add-resume-language", "Resumes", "AddResumeLanguage"),
			"update-resume-language" => new RouteDTO("update-resume-language", "Resumes", "UpdateResumeLanguage"),
			"delete-resume-language" => new RouteDTO("delete-resume-language", "Resumes", "DeleteResumeLanguage"),

			// Skills
			"add-resume-skill" => new RouteDTO("add-resume-skill", "Resumes", "AddResumeSkill"),
			"update-resume-skill" => new RouteDTO("update-resume-skill", "Resumes", "UpdateResumeSkill"),
			"delete-resume-skill" => new RouteDTO("delete-resume-skill", "Resumes", "DeleteResumeSkill"),

			// Certificates
			"add-resume-certificate" => new RouteDTO("add-resume-certificate", "Resumes", "AddResumeCertificate"),
			"update-resume-certificate" => new RouteDTO("update-resume-certificate", "Resumes", "UpdateResumeCertificate"),
			"delete-resume-certificate" => new RouteDTO("delete-resume-certificate", "Resumes", "DeleteResumeCertificate"),

			// Volunteer work
			"add-resume-volunteer-work" => new RouteDTO("add-resume-volunteer-work", "Resumes", "AddResumeVolunteerWork"),
			"update-resume-volunteer-work" => new RouteDTO("update-resume-volunteer-work", "Resumes", "UpdateResumeVolunteerWork"),
			"delete-resume-volunteer-work" => new RouteDTO("delete-resume-volunteer-work", "Resumes", "DeleteResumeVolunteerWork"),

			// Interests
			"add-resume-interest" => new RouteDTO("add-resume-interest", "Resumes", "AddResumeInterest"),
			"update-resume-interest" => new RouteDTO("update-resume-interest", "Resumes", "UpdateResumeInterest"),
			"delete-resume-interest" => new RouteDTO("delete-resume-interest", "Resumes", "DeleteResumeInterest"),

			// Quotes
			"add-resume-quote" => new RouteDTO("add-resume-quote", "Quotes", "AddResumeQuote"),
			"update-resume-quote" => new RouteDTO("update-resume-quote", "Quotes", "UpdateResumeQuote"),
			"delete-resume-quote" => new RouteDTO("delete-resume-quote", "Quotes", "DeleteResumeQuote"),
			"upload-quote-image" => new RouteDTO("upload-quote-image", "Quotes", "QuoteImage"),

			// Portfolio
			"portfolio-item" => new RouteDTO("portfolio/{portfolioId:integer}", "Portfolio", "PortfolioItem"),
			"edit-portfolio-item" => new RouteDTO("portfolio/{portfolioId:integer}/{edit:string}", "Portfolio", "PortfolioItem"),
			"create-portfolio-item" => new RouteDTO("create-portfolio-item/{resumeId:integer}", "Portfolio", "CreateResumePortfolio"),
			"publish-portfolio-item" => new RouteDTO("publish-portfolio-item", "Portfolio", "PublishPortfolioItem"),
			"delete-portfolio-item" => new RouteDTO("delete-portfolio-item", "Portfolio", "DeleteResumePortfolio"),
			"upload-portfolio-header" => new RouteDTO("upload-portfolio-header", "Portfolio", "PortfolioHeaderImage"),
			"upload-portfolio-images" => new RouteDTO("upload-portfolio-images", "Portfolio", "PortfolioGalleryImage"),
			"delete-portfolio-images" => new RouteDTO("delete-portfolio-images", "Portfolio", "DeleteGalleryImage"),

			// Contact
			"contact-user" => new RouteDTO("contact-user", "Resumes", "SendCustomEmail"),

			// Users
			"user-pages" => new RouteDTO("user-pages/{userId:integer}", "Users", "UserPages"),
			"login" => new RouteDTO("login", "Security", "Login"),
			"registration" => new RouteDTO("registration", "Users", "Registration"),
			"new-user-confirmation-link" => new RouteDTO("new-user-confirmation-link/{guid:string}", "Users", "ActivateUser"),
			"confirmation-link-sent" => new RouteDTO("confirmation-link", "Users", "ConfirmationLinkSent"),
			"successful-registration" => new RouteDTO("successful-register", "Users", "SuccessfulRegistration"),
			"unsuccessful-registration" => new RouteDTO("unsuccessful-register", "Users", "UnsuccessfulRegistration"),
			"compare-password-and-email" => new RouteDTO("compare-password-and-email", "Users", "ComparePasswordAndEmail"),
			"validate-email" => new RouteDTO("validation-email", "Users", "ValidateEmail"),
			"validate-password-user-change" => new RouteDTO("validation-password-user-change", "Users", "ValidatePasswordWhenUserChange"),
			"logout" => new RouteDTO("logout", "Security", "Logout"),
			"reset-user-password" => new RouteDTO("reset-password/{guid:string}", "Security", "ResetPassword"),
			"reset-user-password-form" => new RouteDTO("reset-password-form", "Security", "ResetPassword"),
			"send-email-to-change-password" => new RouteDTO("send-email-to-change-password", "Security", "SendEmailToChangePassword"),
			"reset-password-email-successfully-sent" => new RouteDTO("reset-password-email-successfully-sent", "Users", "SuccessfulPasswordReset"),
			"reset-password-email-unsuccessfully-sent" => new RouteDTO("reset-password-email-unsuccessfully-sent", "Users", "UnSuccessfulPasswordReset"),
			"incorrect-link" => new RouteDTO("incorrect-link", "Users", "IncorrectRessetPasswordLink"),
			"reset-password-successful" => new RouteDTO("reset-password-successful", "Users", "ResetPasswordSuccessful"),
			"reset-password-unsuccessful" => new RouteDTO("reset-password-unsuccessful", "Users", "ResetPasswordUnSuccessful"),
			"account" => new RouteDTO("account", "Users", "EditProfile"),
			"change-password" => new RouteDTO("change-password", "Users", "ChangePassword"),
			"change-email" => new RouteDTO("change-email", "Users", "ChangeEmail"),
			"delete-user" => new RouteDTO("delete-user", "Users", "DeleteUser"),
			"deactivate-user" => new RouteDTO("deactivate-user", "Users", "DeactivateUser"),
			"send-confirmation-email" => new RouteDTO("send-confirmation-email/{userId:integer}", "Users", "SendConfirmationEmail"),

			// Blog
			"blog" => new RouteDTO("blog", "Articles", "Articles"),
			"article" => new RouteDTO("article/{articleId:integer}", "Articles", "Article"),
			"add-article" => new RouteDTO("add-article", "Articles", "InsertArticle"),
			"update-article" => new RouteDTO("update-article/{articleId:integer}", "Articles", "UpdateArticle"),
			"delete-article" => new RouteDTO("delete-article/{articleId:integer}", "Articles", "DeleteArticle"),

			// Comments
			"add-article-comment" => new RouteDTO("add-article-comment", "Articles", "InsertArticleComment"),
			"update-article-comment" => new RouteDTO("update-article-comment/{articleId:integer}", "Articles", "UpdateArticleComment"),
			"delete-article-comment" => new RouteDTO("delete-article-comment/{articleId:integer}", "Articles", "DeleteArticleComment"),

			// Job
			"jobs" => new RouteDTO("jobs", "Jobs", "Jobs"),
			"job" => new RouteDTO("jobs/{jobId:integer}", "Jobs", "Job"),
			"add-job" => new RouteDTO("add-job", "Jobs", "InsertJob"),
			"update-job" => new RouteDTO("update-job/{jobId:integer}", "Jobs", "UpdateJob"),
			"delete-job" => new RouteDTO("delete-job/{jobId:integer}", "Jobs", "DeleteJob"),


			// Maintenance
			"maintenance" => new RouteDTO("maintenance", "Maintenance", "Index"),
		);
		return $routes;
	}

}