<?php

/**
 * Class CurrentUserDTO
 *
 */
class CurrentUserDTO {

    public $Name;
    public $Email;
    public $UserId;
    public $Image;

    public $RoleId;
    public $Permissions = [];

} 