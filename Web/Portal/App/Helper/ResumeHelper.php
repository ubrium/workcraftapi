<?php
use Business\Enums\LanguageLevelsEnum;

/**
 * Created by PhpStorm.
 * User: Nino
 * Date: 31.1.2017
 * Time: 16:16
 */
class ResumeHelper
{

    public static function LanguageLevel($level)
    {

        $label = [
            1 => "Beginner",
            2 => "Intermediate",
            3 => "Advanced",
            4 => "Proficient",
            5 => "Native"
        ];

        return $label[$level];
    }


}