<?php

/**
 * Created by PhpStorm.
 * User: Nino
 * Date: 31.1.2017
 * Time: 16:16
 */
class IconHelper
{

    public static function GetIcons()
    {

        $icons = [
           "fa fa-anchor",
           "fa fa-apple",
           "fa fa-asterisk",
           "fa fa-automobile",
           "fa fa-balance-scale",
           "fa fa-bank",
           "fa fa-bar-chart",
           "fa fa-bed",
           "fa fa-beer",
           "fa fa-birthday-cake",
           "fa fa-bicycle",
           "fa fa-binoculars",
           "fa fa-black-tie",
           "fa fa-bitcoin",
           "fa fa-bolt",
           "fa fa-bug",
           "fa fa-bus",
           "fa fa-camera-retro",
           "fa fa-circle",
           "fa fa-code",
           "fa fa-coffee",
           "fa fa-comments",
           "fa fa-compass",
           "fa fa-credit-card",
           "fa fa-cube",
           "fa fa-cubes",
           "fa fa-diamond",
           "fa fa-dribbble",
           "fa fa-facebook",
           "fa fa-female",
           "fa fa-fighter-jet",
           "fa fa-fire-extinguisher",
           "fa fa-gavel",
           "fa fa-first-order",
           "fa fa-flask",
           "fa fa-futbol-o",
           "fa fa-gamepad",
           "fa fa-gears",
           "fa fa-gift",
           "fa fa-graduation-cap",
           "fa fa-hand-spock-o",
           "fa fa-hand-scissors-o",
           "fa fa-hand-peace-o",
           "fa fa-heart",
           "fa fa-hashtag",
           "fa fa-hourglass-start",
           "fa fa-html5",
           "fa fa-industry",
           "fa fa-instagram",
           "fa fa-laptop",
           "fa fa-life-bouy",
           "fa fa-lightbulb-o",
           "fa fa-male",
           "fa fa-motorcycle",
           "fa fa-music",
           "fa fa-mobile-phone",
           "fa fa-newspaper-o",
           "fa fa-paint-brush",
           "fa fa-pinterest",
           "fa fa-plane",
           "fa fa-ra",
           "fa fa-road",
           "fa fa-scissors",
           "fa fa-shopping-basket",
           "fa fa-stethoscope",
           "fa fa-suitcase",
           "fa fa-twitter",
           "fa fa-venus",
           "fa fa-mars",
           "fa fa-venus-mars",
           "fa fa-venus-double",
           "fa fa-mars-double",
           "fa fa-wrench"
        ];

        return $icons;
    }


}