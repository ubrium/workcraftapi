<?php
use Business\Enums\LanguageCodesEnum;
use Business\Enums\LanguageLevelsEnum;

class MainHelper {

	public static function IsLoggedIn() {
		return Security::IsLoggedIn();
	}

	public static function GetCurrentUser() {
		return Security::GetCurrentUser();
	}

	public static function EnumDescription($enum, $value) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::Description($value);
	}

	public static function EnumCaption($enum, $value) {
		$type = "Business\\Enums" . '\\' . $enum;
		$enum = new $type();
		return $enum->Caption($value);
	}

	public static function EnumConstant($enum, $constant) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::GetConstant($constant);
	}

	public static function Enum($enum) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::enum();
	}

	public static function ActiveButton($controller, $action = false, $class = "active") {
		return HtmlHelper::ActiveButton($controller, $action, $class);
	}

	public static function LanguageLevel($level) {
		return ResumeHelper::LanguageLevel($level);
	}

	public static function GetIcons() {
		return IconHelper::GetIcons();
	}

	public static function Countries() {
		return SelectableHelper::Countries();
	}

	public static function CountryByCode($key) {
		return SelectableHelper::CountryByCode($key);
	}

	public static function Languages() {
		return SelectableHelper::Languages();
	}

	public static function LanguageName($key) {
		return SelectableHelper::LanguageName($key);
	}

	public static function LanguageFlags($key) {
		return SelectableHelper::LanguageFlags($key);
	}

	public static function GetTheme($key = null) {
		return ThemesHelper::GetTheme($key);
	}

	public static function LabelsByLanguageCode($langCode, $label) {
		return LabelsHelper::LabelsByLanguageCode($langCode, $label);
	}

	public static function GetSection($key, $info) {
		return SectionsHelper::GetSection($key, $info);
	}

	public static function GetLanguageId() {
		global $router;
		return LanguageCodesEnum::GetConstant($router->Language);
	}
}