<?php
use Business\Enums\LanguageLevelsEnum;

/**
 * Created by PhpStorm.
 * User: Nino
 * Date: 31.1.2017
 * Time: 16:16
 */
class SectionsHelper
{



    public static function GetSection($key, $info)
    {

        $sections = [
            1 => ["target" => "#educationEditor", "label" => "Education", "template" => "EducationSection.twig", "class" => "add-item"],
            2 => ["target" => "#expEditor", "label" => "Experience", "template" => "ExperienceSection.twig", "class" => "add-item"],
            3 => ["target" => "modalResumeSkillEditor", "label" => "Skills", "template" => "SkillsSection.twig", "class" => "add-skill-item"],
            4 => ["target" => "#langEditor", "label" => "Languages", "template" => "LanguageSection.twig", "class" => "add-item"],
            5 => ["target" => "#certEditor", "label" => "Certificates", "template" => "CertificateSection.twig", "class" => "add-item"],
            6 => ["target" => "#volEditor", "label" => "Volunteering", "template" => "VolunteerWorkSection.twig", "class" => "add-item"],
            7 => ["target" => "#intEditor", "label" => "Interests", "template" => "InterestsSections.twig", "class" => "add-item"],
        ];


            return $sections[$key][$info];

    }

}