<?php
use Business\Enums\LanguageLevelsEnum;

/**
 * Created by PhpStorm.
 * User: Nino
 * Date: 31.1.2017
 * Time: 16:16
 */
class ThemesHelper
{

    public static function GetTheme($key = null)
    {

        $theme = [
            1 => ['pc_red', ["#ff0303"]],
            2 => ['pc_blue', ["#0042ff"]],
            3 => ['pc_cyan', ["#1ce6b9"]],
            4 => ['pc_purple', ["#540081"]],
            5 => ['pc_yellow', ["#fffc01"]],
            6 => ['pc_orange', ["#fe8a0e"]],
            7 => ['pc_green', ["#20c000"]],
            8 => ['pc_pink', ["#e55bb0"]],
            9 => ['pc_silver', ["#959697"]],
            10 => ['pc_sky', ["#7ebff1"]],
            11 => ['pc_teal', ["#106246"]],
            12 => ['pc_brown', ["#4e2a04"]],
        ];

        if($key == null) {
            return $theme;
        } else {
            return $theme[$key];
        }

    }

}