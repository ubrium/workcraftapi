<?php

use Business\ApiControllers\ResumeBasicsApiController;
use Business\ApiControllers\ResumeCertificatesApiController;
use Business\ApiControllers\ResumeEducationsApiController;
use Business\ApiControllers\ResumeExperiencesApiController;
use Business\ApiControllers\ResumeInterestsApiController;
use Business\ApiControllers\ResumeLanguagesApiController;
use Business\ApiControllers\ResumePortfoliosApiController;
use Business\ApiControllers\ResumeQuotesApiController;
use Business\ApiControllers\ResumesApiController;
use Business\ApiControllers\ResumeSectionsApiController;
use Business\ApiControllers\ResumeSkillsApiController;
use Business\ApiControllers\ResumeVolunteerWorksApiController;
use Business\ApiControllers\UsersApiController;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Helper\NotificationHelper;
use Business\Models\ResumeBasicModel;
use Business\Models\ResumeCertificateModel;
use Business\Models\ResumeEducationModel;
use Business\Models\ResumeExperienceModel;
use Business\Models\ResumeInterestModel;
use Business\Models\ResumeLanguageModel;
use Business\Models\ResumeModel;
use Business\Models\ResumeQuoteModel;
use Business\Models\ResumeSectionModel;
use Business\Models\ResumeSkillModel;
use Business\Models\ResumeVolunteerWorkModel;

class PagesController extends MVCController
{

    public function GetAbout()
    {
        $this->RenderView("Pages/About");
    }

}