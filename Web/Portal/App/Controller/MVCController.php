<?php

abstract class MVCController {

	/* Properties */

	/* Constructor */
	public function __construct($callFunc = true) {
		if ($callFunc) {
			global $router;
			if (!$router->Action) {
				$router->Action = "Index";
			}

			$functionParameters = array();
			if ($_SERVER['REQUEST_METHOD'] === 'POST') {
				$functionPrefix = "Post";
				$parameterCollection = array_merge($_POST, $_FILES, $router->Parameters);
			} else {
				$functionPrefix = "Get";
				$parameterCollection = array_merge($_GET, $_FILES, $router->Parameters);
			}

			$reflector = new ReflectionClass($router->Controller . "Controller");
			$reflectionMethod = $reflector->getMethod($functionPrefix . $router->Action);
			$parameters = $reflectionMethod->getParameters();

			foreach ($parameters as $parameter) {
				$paramName = $parameter->name;
				if ($paramName === "permissions") {
					$permissions = $parameter->getDefaultValue();
					continue;
				}

				if (!isset($parameterCollection[$paramName])) {
					if ($parameter->isDefaultValueAvailable()) {
						$functionParameters[$paramName] = $parameter->getDefaultValue();
					} else {
						throw new Exception("Missing required parameter: " . $paramName);
					}
				} else {
					if ($paramClass = $parameter->getClass()) {
						$param = new $paramClass->name();
						if (is_array($parameterCollection[$paramName])) {
							foreach ($parameterCollection[$paramName] as $paramPropertyName => $paramPropertyValue) {
								$param->$paramPropertyName = $paramPropertyValue;
							}
						}
						$functionParameters[$paramName] = $param;
					} else {
						$functionParameters[$paramName] = $parameterCollection[$paramName];
					}
				}
			}

			if (isset($permissions)) {
				if (!Security::CheckPermissions($permissions)) {
					throw new AccessDeniedException();
				}
			}
			$reflectionMethod->invokeArgs($this, $functionParameters);
		}
	}

	public function RenderView($template, $parameters = [], $render = true) {
		require_once '../../../Business/Lib/Twig/Autoloader.php';
		Twig_Autoloader::register();

		$loader = new Twig_Loader_Filesystem('View');
		$twig = new Twig_Environment($loader, array(
			'cache' => Config::useCache === true ? "cache" : false,
			'auto_reload' => Config::useCache,
			'debug' => Config::debugMode
		));

		if (Config::debugMode === true) {
			$twig->addExtension(new Twig_Extension_Debug());
		}
		$twig->addExtension(new Twig_Extension_Breakpoint());

		global $router;
		$twig->addGlobal('Router', $router);
		$twig->addGlobal('Config', new Config());
		$twig->addGlobal('Session', $_SESSION);
		$twig->addGlobal('Helper', new MainHelper());

		$multiLanguageFunction = new Twig_SimpleFunction('__', function ($label) {
			return __($label);
		});
		$twig->addFunction($multiLanguageFunction);

		$template = $twig->loadTemplate($template . '.twig');

		if ($render === true) {
			echo $template->render($parameters);
			return true;
		} else {
			return $template->render($parameters);
		}
	}

}