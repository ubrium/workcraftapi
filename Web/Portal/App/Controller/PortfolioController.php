<?php

use Business\ApiControllers\PortfolioImagesApiController;
use Business\ApiControllers\ResumeBasicsApiController;
use Business\ApiControllers\ResumeCertificatesApiController;
use Business\ApiControllers\ResumeEducationsApiController;
use Business\ApiControllers\ResumeExperiencesApiController;
use Business\ApiControllers\ResumeInterestsApiController;
use Business\ApiControllers\ResumeLanguagesApiController;
use Business\ApiControllers\ResumePortfoliosApiController;
use Business\ApiControllers\ResumesApiController;
use Business\ApiControllers\ResumeSectionsApiController;
use Business\ApiControllers\ResumeSkillsApiController;
use Business\ApiControllers\ResumeVolunteerWorksApiController;
use Business\Models\PortfolioImageModel;
use Business\Models\ResumeBasicModel;
use Business\Models\ResumeCertificateModel;
use Business\Models\ResumeEducationModel;
use Business\Models\ResumeExperienceModel;
use Business\Models\ResumeInterestModel;
use Business\Models\ResumeLanguageModel;
use Business\Models\ResumeModel;
use Business\Models\ResumePortfolioModel;
use Business\Models\ResumeSectionModel;
use Business\Models\ResumeSkillModel;
use Business\Models\ResumeVolunteerWorkModel;

class PortfolioController extends MVCController
{

    public function GetPortfolioItem($portfolioId, $edit = false)
    {

        $model = new UserViewModel();

        $model->Portfolio = ResumePortfoliosApiController::GetPortfolioItem($portfolioId);


        $resume = ResumesApiController::GetResumeById($model->Portfolio->ResumeId);
        $model->BasicInfo = ResumeBasicsApiController::GetResumeBasicsDefault($model->Portfolio->ResumeId);
        $model->Resume = $resume;
        $categories = explode(", ", $model->Portfolio->Category);

        $gallery = PortfolioImagesApiController::GetPortfolioItemImages($model->Portfolio->ResumePortfolioId);

        $editorType = true;

        $this->RenderView("Resumes/Templates/Timeline/PortfolioItem", ["model" => $model, "gallery" => $gallery, "editorMode" => $edit, "categories" => $categories, "editorType" => $editorType]);
    }

    public function GetCreateResumePortfolio($resumeId)
    {

        $model = new UserViewModel();


        $resume = ResumesApiController::GetResumeById($resumeId);
        $model->BasicInfo = ResumeBasicsApiController::GetResumeBasicsDefault($resumeId);

        $model->Portfolio = ResumePortfoliosApiController::GetPortfolioNew($resumeId);


        if ($model->Portfolio == null) {
            $item = new ResumePortfolioModel();
            $item->ResumeId = $resumeId;
            $item->Title = null;
            $item->CaseStudy = null;
            $item->Category = null;
            $item->Picture = null;
            $item->ShortDescription = null;
            $item->PublishStatusId = 1;
            $item->ParentId = null;
            $model->Portfolio = ResumePortfoliosApiController::InsertPortfolioNew($item);
            $gallery = PortfolioImagesApiController::GetPortfolioItemImages($model->Portfolio);
            $categories = "";
        } else {
            $gallery = PortfolioImagesApiController::GetPortfolioItemImages($model->Portfolio->ResumePortfolioId);
            $categories = explode(", ", $model->Portfolio->Category);
        }

        $model->Resume = $resume;

        $this->RenderView("Resumes/Templates/Timeline/PortfolioItem", ["model" => $model, "gallery" => $gallery, "editorMode" => true, "categories" => $categories]);
    }

    public function PostPublishPortfolioItem($resumePortfolioId, $resumeId, $title, $caseStudy, $category, $picture, $shortDescription, $publishStatus) {

        $model = new ResumePortfolioModel();
        $model->ResumePortfolioId = $resumePortfolioId;
        $model->ResumeId = $resumeId;
        $model->Title = $title;
        $model->CaseStudy = $caseStudy;
        $model->Category = $category;
        $model->Picture = $picture;
        $model->ShortDescription = $shortDescription;
        $model->PublishStatusId = $publishStatus;
        $model->ParentId = null;
        $model->DatePublished = date("Y-m-d H:i:s");
        $model->DateUpdated = null;

        ResumePortfoliosApiController::PublishPortfolioItem($model);
    }

    public function PostDeleteResumePortfolio($resumePortfolioId, $langCode) {

        ResumePortfoliosApiController::DeleteResumePortfolio($resumePortfolioId);
        Router::Redirect('editor', ["langCode" => $langCode]);
    }

    public function PostDeleteGalleryImage($portfolioImageId) {
        PortfolioImagesApiController::DeleteImage($portfolioImageId);
    }

    public function PostPortfolioGalleryImage()
    {
        $newName = self::_generatePictureName($_FILES["images"]["name"][0], "blog");
        move_uploaded_file($_FILES["images"]["tmp_name"][0], self::_generatePictureFullPath($newName)); //uploading picture
        $location = self::_generatePictureFullPath($newName); //uploading picture

        $model = new PortfolioImageModel();
        $model->Picture = $newName;
        $model->ResumePortfolioId = $_POST['itemId'];

        PortfolioImagesApiController::InsertImage($model);

        if($_POST['state'] == false) {
            Router::Redirect('create-portfolio-item', ["resumeId" => $_POST['resumeId']]);
        } else {
            Router::Redirect('edit-portfolio-item', ["portfolioId" => $_POST['itemId'], "edit" => true]);
        }

    }

    public function PostPortfolioHeaderImage()
    {
        $newName = self::_generatePictureName($_FILES["images"]["name"][0], "blog");
        move_uploaded_file($_FILES["images"]["tmp_name"][0], self::_generatePictureFullPath($newName)); //uploading picture
        $location = self::_generatePictureFullPath($newName); //uploading picture

        $this->RenderView("Resumes/Templates/Default/Partials/PostPortfolioHeader", ["location" => self::_picturePath($newName), 'name' => 'image', 'img_name' => $newName]);
    }

    private static function _generatePictureName($logo, $name)
    {
        return CommonHelper::StringToFilename(sprintf("project-%s%s.%s", $name, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($logo)));
    }

    private static function _generatePictureFullPath($pictureName)
    {
        return sprintf("%s/Media/Projects/%s", CDN_PATH, $pictureName);
    }

    public static function _picturePath($image)
    {
        return sprintf("%s/Media/Projects/%s", CDN_URL, $image);
    }

}