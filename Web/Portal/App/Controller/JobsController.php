<?php

use Business\ApiControllers\JobsApiController;
use Business\Models\JobModel;

class JobsController extends MVCController {

	public function GetJobs() {

		$model = new JobViewModel();
		$model->Job = JobsApiController::GetJobs();

		echo json_encode($model);
	}

	public function GetJob($jobId) {
		$model = new JobViewModel();
		$model->Job = JobsApiController::GetJob($jobId);

		echo json_encode($model);
	}

	public function PostInsertJob($type, $experienceLevel, $role, $technologies, $description) {
		$model = new JobModel();
		$currentUser = Security::GetCurrentUser();

		$model->UserId = $currentUser->UserId;
		$model->Type = $type;
		$model->ExperienceLevel = $experienceLevel;
		$model->Role = $role;
		$model->Technologies = $technologies;
		$model->Description = $description;

		JobsApiController::InsertJob($model);
	}

	public function PostUpdateJob($jobId, $type, $experienceLevel, $role, $technologies, $description) {

		$model = JobsApiController::GetJob($jobId);

		$model->Type = $type;
		$model->ExperienceLevel = $experienceLevel;
		$model->Role = $role;
		$model->Technologies = $technologies;
		$model->Description = $description;

		JobsApiController::UpdateJob($model);
	}

	public function GetDeleteJob($jobId) {
		JobsApiController::DeleteJob($jobId);
	}
}