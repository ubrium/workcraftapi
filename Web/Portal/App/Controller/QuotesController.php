<?php

use Business\ApiControllers\ResumeBasicsApiController;
use Business\ApiControllers\ResumeCertificatesApiController;
use Business\ApiControllers\ResumeEducationsApiController;
use Business\ApiControllers\ResumeExperiencesApiController;
use Business\ApiControllers\ResumeInterestsApiController;
use Business\ApiControllers\ResumeLanguagesApiController;
use Business\ApiControllers\ResumePortfoliosApiController;
use Business\ApiControllers\ResumeQuotesApiController;
use Business\ApiControllers\ResumesApiController;
use Business\ApiControllers\ResumeSectionsApiController;
use Business\ApiControllers\ResumeSkillsApiController;
use Business\ApiControllers\ResumeVolunteerWorksApiController;
use Business\Models\ResumeBasicModel;
use Business\Models\ResumeCertificateModel;
use Business\Models\ResumeEducationModel;
use Business\Models\ResumeExperienceModel;
use Business\Models\ResumeInterestModel;
use Business\Models\ResumeLanguageModel;
use Business\Models\ResumeModel;
use Business\Models\ResumeQuoteModel;
use Business\Models\ResumeSectionModel;
use Business\Models\ResumeSkillModel;
use Business\Models\ResumeVolunteerWorkModel;

class QuotesController extends MVCController
{



    public function PostAddResumeQuote($resumeId, $name, $description, $position, $picture, $langCode, $userId = null)
    {

        $model = new ResumeQuoteModel();
        $model->ResumeId = $resumeId;
        $model->Name = $name;
        $model->Description = $description;
        $model->Position = $position;
        $model->Picture = $picture;
        $model->UserId = $userId;
        ResumeQuotesApiController::InsertResumeQuote($model);
        Router::Redirect('editor', ["langCode" => $langCode]);

    }

    public function PostUpdateResumeQuote($resumeId, $resumeQuoteId, $name, $picture, $description, $position, $langCode, $userId = null)
    {

        $model = new ResumeQuoteModel();
        $model->ResumeQuoteId = $resumeQuoteId;
        $model->ResumeId = $resumeId;
        $model->Name = $name;
        $model->Description = $description;
        $model->Position = $position;
        $model->Picture = $picture;
        $model->UserId = $userId;
        ResumeQuotesApiController::UpdateResumeQuote($model);
        Router::Redirect('editor', ["langCode" => $langCode]);

    }

    public function PostDeleteResumeQuote($resumeQuoteId, $langCode)
    {

        ResumeQuotesApiController::DeleteResumeQuote($resumeQuoteId);

        Router::Redirect('editor', ["langCode" => $langCode]);

    }

    public function PostQuoteImage()
    {
        $newName = self::_generatePictureName($_FILES["images"]["name"][0], "blog");
        move_uploaded_file($_FILES["images"]["tmp_name"][0], self::_generatePictureFullPath($newName)); //uploading picture
        $location = self::_generatePictureFullPath($newName); //uploading picture

        $this->RenderView("Users/Partials/PostUserImage", ["location" => self::_picturePath($newName), 'name' => 'quoteImageName', 'img_name' => $newName]);
    }

    private static function _generatePictureName($logo, $name)
    {
        return CommonHelper::StringToFilename(sprintf("project-%s%s.%s", $name, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($logo)));
    }

    private static function _generatePictureFullPath($pictureName)
    {
        return sprintf("%s/Media/Quotes/%s", CDN_PATH, $pictureName);
    }

    public static function _picturePath($image)
    {
        return sprintf("%s/Media/Quotes/%s", CDN_URL, $image);
    }

}