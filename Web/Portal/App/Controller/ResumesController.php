<?php

use Business\ApiControllers\ResumeBasicsApiController;
use Business\ApiControllers\ResumeCertificatesApiController;
use Business\ApiControllers\ResumeEducationsApiController;
use Business\ApiControllers\ResumeExperiencesApiController;
use Business\ApiControllers\ResumeInterestsApiController;
use Business\ApiControllers\ResumeIntrosApiController;
use Business\ApiControllers\ResumeLanguagesApiController;
use Business\ApiControllers\ResumePortfoliosApiController;
use Business\ApiControllers\ResumeQuotesApiController;
use Business\ApiControllers\ResumesApiController;
use Business\ApiControllers\ResumeSectionsApiController;
use Business\ApiControllers\ResumeSkillsApiController;
use Business\ApiControllers\ResumeVolunteerWorksApiController;
use Business\ApiControllers\UsersApiController;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Helper\NotificationHelper;
use Business\Models\ResumeBasicModel;
use Business\Models\ResumeCertificateModel;
use Business\Models\ResumeEducationModel;
use Business\Models\ResumeExperienceModel;
use Business\Models\ResumeInterestModel;
use Business\Models\ResumeIntroModel;
use Business\Models\ResumeLanguageModel;
use Business\Models\ResumeModel;
use Business\Models\ResumeQuoteModel;
use Business\Models\ResumeSectionModel;
use Business\Models\ResumeSkillModel;
use Business\Models\ResumeVolunteerWorkModel;

class ResumesController extends MVCController
{

	public function GetResume($userId = null, $langCode = null)
	{

		header("Access-Control-Allow-Origin: *");
		$currentUser = Security::GetCurrentUser();


		if ($userId == null && $langCode == null) {
			$editorMode = true;
			$resume = ResumesApiController::GetResumeDefault($currentUser->UserId);
		} elseif ($userId == null && $langCode !== null) {
			$editorMode = true;
			$resume = ResumesApiController::GetResumeByIdAndLangCode($currentUser->UserId, $langCode);
		} elseif ($userId !== null && $langCode == null) {
			$editorMode = false;
			$resume = ResumesApiController::GetResumeDefault($userId);
			self::_countVisit($resume->ResumeId);
		} else {
			$editorMode = false;
			$resume = ResumesApiController::GetResumeByIdAndLangCode($userId, $langCode);
			self::_countVisit($resume->ResumeId);
		}
		if ($resume == null) {
			$model = null;
		} else {
			$model = new UserViewModel();
			$model->BasicInfo = ResumeBasicsApiController::GetResumeBasicsDefault($resume->ResumeId);
			$model->BasicInfo->Picture = "http://workcraft.ubr/CDN/Media/Users/" . $model->BasicInfo->Picture;
			$model->ResumeSections = ResumeSectionsApiController::GetResumeSectionsByResumeId($resume->ResumeId);
			$model->Education = ResumeEducationsApiController::GetResumeEducationByResumeId($resume->ResumeId);
			$model->Experience = ResumeExperiencesApiController::GetResumeExperiencesByResumeId($resume->ResumeId);
			$model->Skills = ResumeSkillsApiController::GetResumeSkillsByResumeId($resume->ResumeId);
			$model->Languages = ResumeLanguagesApiController::GetResumeLanguagesByResumeId($resume->ResumeId);
			$model->Certificates = ResumeCertificatesApiController::GetResumeCertificatesByResumeId($resume->ResumeId);
			$model->VolunteerWork = ResumeVolunteerWorksApiController::GetResumeVolunteerWorksByResumeId($resume->ResumeId);
			$model->Interests = ResumeInterestsApiController::GetResumeInterestsByResumeId($resume->ResumeId);
			$model->Portfolio = ResumePortfoliosApiController::GetResumePortfoliosByResumeId($resume->ResumeId, 2);
			$model->Portfolio->Picture = "http://workcraft.ubr/CDN/Media/Projects/" . $model->Portfolio->Picture;
			//$model->Quotes = ResumeQuotesApiController::GetResumeQuotesByResumeId($resume->ResumeId);
			$model->Intro = ResumeIntrosApiController::GetResumeIntroByResumeId($resume->ResumeId);
			$model->Intro->Picture = "http://workcraft.ubr/CDN/Media/Users/" . $model->Intro->Picture;
			$model->Resume = $resume;
			$model->Resume->ThemeId = ThemesHelper::GetTheme($model->Resume->ThemeId)[0];
		}

		if ($userId == null) {
			$userId = $currentUser->UserId;
		}

		echo json_encode($model);


	}

	public function PostUpdateResume($resumeId, $themeId)
	{
		$currentUser = Security::GetCurrentUser();
		$resumeDefault = ResumesApiController::GetResumeById($resumeId);

		$model = new ResumeModel();
		$model->UserId = $currentUser->UserId;
		$model->ResumeId = $resumeDefault->ResumeId;
		$model->TemplateId = $resumeDefault->TemplateId;
		$model->ThemeId = $themeId;
		$model->StatusId = $resumeDefault->StatusId;
		$model->Views = $resumeDefault->Views;
		$model->LanguageCode = $resumeDefault->LanguageCode;

		ResumesApiController::UpdateResume($model);
	}

	public function PostAddResumeIntro($resumeId)
	{

		$model = new ResumeIntroModel();
		$model->ResumeId = $resumeId;
		$model->Text = null;
		$model->BackgroundColor = null;
		$model->Picture = "abg4.jpg";


		ResumeIntrosApiController::InsertResumeIntro($model);
	}

	public function PostUpdateResumeIntro($resumeId, $resumeIntroId, $text, $backgroundColor = null, $picture)
	{

		$model = new ResumeIntroModel();
		$model->ResumeId = $resumeId;
		$model->ResumeIntroId = $resumeIntroId;
		$model->Text = $text;
		$model->BackgroundColor = $backgroundColor;
		$model->Picture = $picture;

		ResumeIntrosApiController::UpdateResumeIntro($model);
	}

	public function GetPrimaryLanguage()
	{
		$this->RenderView("Resumes/PrimaryLanguage");
	}

	public function GetCreateResume($langCode)
	{

		$currentUser = Security::GetCurrentUser();
		$resumeDefault = ResumesApiController::GetResumeDefault($currentUser->UserId);

		$model = new ResumeModel();
		$model->UserId = $currentUser->UserId;
		$model->ResumeId = $resumeDefault->ResumeId;
		$model->TemplateId = 1;
		$model->ThemeId = $resumeDefault->ThemeId;;
		$model->StatusId = 1;
		$model->Views = 0;
		$model->LanguageCode = $langCode;

		ResumesApiController::UpdateResume($model);

		$model = new ResumeBasicModel();
		$model->ResumeId = $resumeDefault->ResumeId;
		$model->Name = null;
		$model->Description = null;
		$model->About = null;
		$model->ContactEmail = null;
		$model->Website = null;
		$model->CountryCode = null;
		$model->Phone = null;
		$model->Picture = null;
		ResumeBasicsApiController::InsertResumeBasics($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function GetCreateNewResume($langCode)
	{

		$currentUser = Security::GetCurrentUser();
		$resumeDefault = ResumesApiController::GetResumeDefault($currentUser->UserId);

		$model = new ResumeModel();
		$model->UserId = $currentUser->UserId;
		$model->TemplateId = 1;
		$model->ThemeId = $resumeDefault->ThemeId;
		$model->StatusId = 2;
		$model->Views = 0;
		$model->LanguageCode = $langCode;

		$resume = ResumesApiController::InsertResume($model);

		$model = new ResumeBasicModel();
		$model->ResumeId = $resume;
		$model->Name = null;
		$model->Description = null;
		$model->About = null;
		$model->ContactEmail = null;
		$model->Website = null;
		$model->CountryCode = null;
		$model->Phone = null;
		$model->Picture = null;
		ResumeBasicsApiController::InsertResumeBasics($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostUpdateResumeBasics($resumeBasicId, $resumeId, $name, $description, $about, $contactEmail, $website, $countryCode, $phone, $picture)
	{

		$model = new ResumeBasicModel();
		$model->ResumeId = $resumeId;
		$model->ResumeBasicId = $resumeBasicId;
		$model->Name = $name;
		$model->Description = $description;
		$model->About = $about;
		$model->ContactEmail = $contactEmail;
		$model->Website = $website;
		$model->CountryCode = $countryCode;
		$model->Phone = $phone;
		$model->Picture = $picture;

		ResumeBasicsApiController::UpdateResumeBasics($model);

	}

	public function PostAddResumeSection($resumeId, $contentSectionId, $name, $langCode)
	{
		$model = new ResumeSectionModel();
		$model->ResumeId = $resumeId;
		$model->ContentSectionId = $contentSectionId;
		$model->Name = $name;
		ResumeSectionsApiController::InsertResumeSection($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostDeleteResumeSection($resumeSectionId, $langCode)
	{
		ResumeSectionsApiController::DeleteResumeSection($resumeSectionId);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function GetAddResumeEducation() {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type');
	}

	public function PostAddResumeEducation()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type');
		$postdata = file_get_contents("php://input");
		$data = json_decode($postdata, true);

		if ($data["langCode"] == null) {
			$resume = ResumesApiController::GetResumeDefault($data["userId"]);
		} else {
			$resume = ResumesApiController::GetResumeByIdAndLangCode($data["userId"], $data["langCode"]);
		}
		$model = new ResumeEducationModel();

		$model->ResumeId = $resume->ResumeId;
		$model->Subject = $data["subject"];
		$model->University = $data["university"];
		$model->DateFrom = $data["yearStarted"] . "-" . $data["monthStarted"] . "-01";
		if ($data["yearFinished"]) {
			$model->DateTo = "0000-00-00";
		} else {
			$model->DateTo = $data["yearFinished"] . "-" . $data["monthFinished"] . "-01";
		}
		$model->AverageAchieved = $data["averageAchieved"];
		$model->AverageTotal = $data["averageTotal"];
		ResumeEducationsApiController::InsertResumeEducation($model);

	}

	public function PostUpdateResumeEducation($langCode, $resumeId, $resumeEducationId, $subject, $university, $dateFrom, $dateTo = "0000-00-00", $averageAchieved, $averageTotal)
	{
		$model = new ResumeEducationModel();
		$model->ResumeId = $resumeId;
		$model->ResumeEducationId = $resumeEducationId;
		$model->Subject = $subject;
		$model->University = $university;
		$model->DateFrom = $dateFrom;
		if ($dateTo == null || $dateTo == "") {
			$model->DateTo = "0000-00-00";
		} else {
			$model->DateTo = $dateTo;
		}
		$model->AverageAchieved = $averageAchieved;
		$model->AverageTotal = $averageTotal;
		ResumeEducationsApiController::UpdateResumeEducation($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostDeleteResumeEducation($resumeExperienceId, $langCode)
	{
		ResumeEducationsApiController::DeleteResumeEducation($resumeExperienceId);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostAddResumeExperience($resumeId, $langCode, $position, $company, $dateFrom, $dateTo = "0000-00-00", $description)
	{
		$model = new ResumeExperienceModel();
		$model->ResumeId = $resumeId;
		$model->Position = $position;
		$model->Company = $company;
		$model->Description = $description;
		$model->DateFrom = $dateFrom;
		if ($dateTo == null || $dateTo == "") {
			$model->DateTo = "0000-00-00";
		} else {
			$model->DateTo = $dateTo;
		}

		ResumeExperiencesApiController::InsertResumeExperience($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostUpdateResumeExperience($resumeId, $langCode, $resumeExperienceId, $position, $company, $dateFrom, $dateTo = "0000-00-00", $description)
	{
		$model = new ResumeExperienceModel();
		$model->ResumeId = $resumeId;
		$model->ResumeExperienceId = $resumeExperienceId;
		$model->Position = $position;
		$model->Company = $company;
		$model->Description = $description;
		$model->DateFrom = $dateFrom;
		if ($dateTo == null || $dateTo == "") {
			$model->DateTo = "0000-00-00";
		} else {
			$model->DateTo = $dateTo;
		}
		ResumeExperiencesApiController::UpdateResumeExperience($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostDeleteResumeExperience($resumeExperienceId, $langCode)
	{
		ResumeExperiencesApiController::DeleteResumeExperience($resumeExperienceId);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostAddResumeLanguage($resumeId, $name, $level, $langCode)
	{
		$model = new ResumeLanguageModel();
		$model->ResumeId = $resumeId;
		$model->Name = $name;
		$model->Level = $level;

		ResumeLanguagesApiController::InsertResumeLanguage($model);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostUpdateResumeLanguage($resumeId, $name, $level, $resumeLanguageId, $langCode)
	{
		$model = new ResumeLanguageModel();
		$model->ResumeId = $resumeId;
		$model->ResumeLanguageId = $resumeLanguageId;
		$model->Name = $name;
		$model->Level = $level;

		ResumeLanguagesApiController::UpdateResumeLanguage($model);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostDeleteResumeLanguage($resumeLanguageId, $langCode)
	{
		ResumeLanguagesApiController::DeleteResumeLanguage($resumeLanguageId);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostAddResumeSkill($resumeId, $name, $level, $langCode)
	{
		$model = new ResumeSkillModel();
		$model->ResumeId = $resumeId;
		$model->Name = $name;
		$model->Level = $level;

		ResumeSkillsApiController::InsertResumeSkill($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostUpdateResumeSkill($resumeId, $name, $level, $resumeSkillId, $langCode)
	{
		$model = new ResumeSkillModel();
		$model->ResumeId = $resumeId;
		$model->ResumeSkillId = $resumeSkillId;
		$model->Name = $name;
		$model->Level = $level;

		ResumeSkillsApiController::UpdateResumeSkill($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostDeleteResumeSkill($resumeSkillId, $langCode)
	{
		ResumeSkillsApiController::DeleteResumeSkill($resumeSkillId);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostAddResumeCertificate($resumeId, $name, $date, $organisation, $langCode)
	{
		$model = new ResumeCertificateModel();
		$model->ResumeId = $resumeId;
		$model->Name = $name;
		$model->Organisation = $organisation;
		$model->Date = $date;

		ResumeCertificatesApiController::InsertResumeCertificate($model);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostUpdateResumeCertificate($resumeId, $name, $date, $organisation, $resumeCertificateId, $langCode)
	{
		$model = new ResumeCertificateModel();
		$model->ResumeId = $resumeId;
		$model->ResumeCertificateId = $resumeCertificateId;
		$model->Name = $name;
		$model->Organisation = $organisation;
		$model->Date = $date;

		ResumeCertificatesApiController::UpdateResumeCertificate($model);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostDeleteResumeCertificate($resumeCertificateId, $langCode)
	{
		ResumeCertificatesApiController::DeleteResumeCertificate($resumeCertificateId);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostAddResumeVolunteerWork($resumeId, $position, $organisation, $dateFrom, $dateTo = "0000-00-00", $description, $langCode)
	{
		$model = new ResumeVolunteerWorkModel();
		$model->ResumeId = $resumeId;
		$model->Position = $position;
		$model->Organisation = $organisation;
		$model->Description = $description;
		$model->DateFrom = $dateFrom;
		if ($dateTo == null || $dateTo == "") {
			$model->DateTo = "0000-00-00";
		} else {
			$model->DateTo = $dateTo;
		}
		ResumeVolunteerWorksApiController::InsertResumeVolunteerWork($model);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostUpdateResumeVolunteerWork($resumeId, $position, $organisation, $dateFrom, $dateTo = "0000-00-00", $description, $resumeVolunteerWorkId, $langCode)
	{
		$model = new ResumeVolunteerWorkModel();
		$model->ResumeId = $resumeId;
		$model->ResumeVolunteerWorkId = $resumeVolunteerWorkId;
		$model->Position = $position;
		$model->Organisation = $organisation;
		$model->Description = $description;
		$model->DateFrom = $dateFrom;
		if ($dateTo == null || $dateTo == "") {
			$model->DateTo = "0000-00-00";
		} else {
			$model->DateTo = $dateTo;
		}
		ResumeVolunteerWorksApiController::UpdateResumeVolunteerWork($model);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostDeleteResumeVolunteerWork($resumeVolunteerWorkId, $langCode)
	{
		ResumeVolunteerWorksApiController::DeleteResumeVolunteerWork($resumeVolunteerWorkId);
		Router::Redirect('editor', ["langCode" => $langCode]);
	}

	public function PostAddResumeInterest($resumeId, $name, $description, $icon, $langCode)
	{

		$model = new ResumeInterestModel();
		$model->ResumeId = $resumeId;
		$model->Name = $name;
		$model->Description = $description;
		$icon = str_replace("modal-close", "", $icon);
		$model->Icon = $icon;
		ResumeInterestsApiController::InsertResumeInterest($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostUpdateResumeInterest($resumeId, $name, $description, $icon, $resumeInterestId, $langCode)
	{

		$model = new ResumeInterestModel();
		$model->ResumeId = $resumeId;
		$model->ResumeInterestId = $resumeInterestId;
		$model->Name = $name;
		$model->Description = $description;
		$icon = str_replace("modal-close", "", $icon);
		$model->Icon = $icon;
		ResumeInterestsApiController::UpdateResumeInterest($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostDeleteResumeInterest($resumeInterestId, $langCode)
	{

		ResumeInterestsApiController::DeleteResumeInterest($resumeInterestId);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostAddResumeQuote($resumeId, $name, $description, $address = null, $authorTitle, $langCode, $userId = null)
	{

		$model = new ResumeQuoteModel();
		$model->ResumeId = $resumeId;
		$model->Name = $name;
		$model->Description = $description;
		$model->Address = $address;
		$model->AuthorTitle = $authorTitle;
		$model->UserId = $userId;
		ResumeQuotesApiController::InsertResumeQuote($model);
		Router::Redirect('editor', ["langCode" => $langCode]);

	}

	public function PostUserProfileImage()
	{
		$newName = self::_generatePictureName($_FILES["images"]["name"][0], "blog");
		move_uploaded_file($_FILES["images"]["tmp_name"][0], self::_generatePictureFullPath($newName)); //uploading picture
		$location = self::_generatePictureFullPath($newName); //uploading picture

		$this->RenderView("Users/Partials/PostUserImage", ["location" => self::_picturePath($newName), 'name' => 'userImagePath', 'img_name' => $newName]);
	}

	public function PostIntroImage()
	{
		$newName = self::_generatePictureName($_FILES["images"]["name"][0], "blog");
		move_uploaded_file($_FILES["images"]["tmp_name"][0], self::_generatePictureFullPath($newName)); //uploading picture
		$location = self::_generatePictureFullPath($newName); //uploading picture

		$this->RenderView("Resumes/Templates/Default/Partials/IntroImage", ["location" => self::_picturePath($newName), 'name' => 'userImagePath', 'img_name' => $newName]);
	}

	private static function _generatePictureName($logo, $name)
	{
		return CommonHelper::StringToFilename(sprintf("project-%s%s.%s", $name, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($logo)));
	}

	private static function _generatePictureFullPath($pictureName)
	{
		return sprintf("%s/Media/Users/%s", CDN_PATH, $pictureName);
	}

	public static function _picturePath($image)
	{
		return sprintf("%s/Media/Users/%s", CDN_URL, $image);
	}

	public function PostSendCustomEmail($subject, $body, $name, $email, $recName, $recEmail)
	{

		$mailModel = new CustomEmailViewModel();
		$mailModel->Body = $body;
		$mailModel->Name = $name;
		$mailModel->Email = $email;

		NotificationHelper::Send(
			$subject,
			$this->RenderView("Mail/CustomMail", ["model" => $mailModel], false),
			"",
			[new SenderDTO("Workcraft notification", "workcraft@ubrium.com")],
			[new RecipientDTO($recName, $recEmail)]
		);

	}

	/**
	 * @param $resumeId
	 */
	public static function _countVisit($resumeId)
	{

		$currentUser = Security::GetCurrentUser();
		$resume = ResumesApiController::GetResumeById($resumeId);

		$model = new ResumeModel();
		$model->UserId = $resume->UserId;
		$model->ResumeId = $resume->ResumeId;
		$model->TemplateId = $resume->TemplateId;
		$model->ThemeId = $resume->ThemeId;
		$model->StatusId = $resume->StatusId;
		$model->Views = $resume->Views + 1;
		$model->LanguageCode = $resume->LanguageCode;

		if ($resume->UserId == $currentUser->UserId) {

		} else {
			ResumesApiController::UpdateResume($model);
		}
	}

}