<?php
use Business\ApiControllers\ResumeBasicsApiController;
use Business\ApiControllers\ResumesApiController;
use Business\ApiControllers\UsersApiController;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Enums\AccessTokenTypesEnum;
use Business\Enums\RolesEnum;
use Business\Helper\NotificationHelper;
use Business\Models\PasswordResetLinkModel;
use Business\Models\UserModel;
use Business\Security\Crypt;

class SecurityController extends MVCController
{

    public function GetLogin()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Content-Type');
    }

    public function PostLogin()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Content-Type');

        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);

        $email = $data['email'];
        $password = $data['password'];
        $user = UsersApiController::CheckLogin($email, $password);
        if ($user !== null) {
            $resume = ResumesApiController::GetResumeDefault($user->UserId);
            $resumeBasics = ResumeBasicsApiController::GetResumeBasicsDefault($resume->ResumeId);


            Security::SetCurrentUser($user, $resumeBasics->Name, "http://workcraft.ubr/CDN/Media/Users/" . $resumeBasics->Picture, UsersApiController::GetUserPermissions($user->UserId));
            $currentUser = Security::GetCurrentUser();

            echo json_encode($currentUser);
        } else {
            echo "Login failed! Username or password are not valid!";
        }
    }

    public function GetLogout()
    {
        UsersApiController::RemoveUserAccessToken(Security::GetCurrentUser()->UserId, AccessTokenTypesEnum::Portal);
        Session::Destroy();
        Router::Redirect(Security::RoleDefaultPageRoute(RolesEnum::Visitor));
    }

    public function PostSendEmailToChangePassword($email)
    {
        $user = UsersApiController::GetUserByEmail($email);

        if (!empty($user)) {
            $oldResetPasswordLink = UsersApiController::GetUserResetPasswordLinkByUserId($user->UserId);
            UsersApiController::DeleteUserResetPasswordLink($oldResetPasswordLink->PasswordResetLinkId);

            $resetPasswordLinkModel = new PasswordResetLinkModel();

            $resetPasswordLinkModel->ResetLink = CommonHelper::GenerateGUID();
            $resetPasswordLinkModel->UserId = $user->UserId;

            UsersApiController::InsertPasswordResetLink($resetPasswordLinkModel);

            $mailModel = new EmailToResetPasswordViewModel();

            $mailModel->Name = $user->Name;
            $mailModel->PasswordResetLink = $resetPasswordLinkModel->ResetLink;

            NotificationHelper::Send(
                "Reset password email",
                $this->RenderView("Mail/ResetPasswordEmail", ["model" => $mailModel], false),
                "",
                [new SenderDTO("gynny", "support@gynny.de")],
                [new RecipientDTO($user->Name, $user->Email)]
            );

            Router::Redirect("reset-password-email-successfully-sent");
        } else {
            Router::Redirect("reset-password-email-unsuccessfully-sent");
        }
    }

    public function GetResetPassword($guid)
    {
        $user = UsersApiController::GetUserResetPasswordLink($guid);

        if (!empty($user)) {
            $this->RenderView("Registration/ResetPassword", ["model" => $user]);
        } else {
            Router::Redirect("incorrect-link");
        }
    }

    public function PostResetPassword($password, $confirmPassword, $userId)
    {
        if ($password == $confirmPassword) {
            $user = UsersApiController::GetUser($userId);
            $user->Password = Crypt::HashPassword($confirmPassword);
            UsersApiController::UpdateUser($user);

            Router::Redirect("reset-password-successful");
        } else {
            Router::Redirect("reset-password-unsuccessful");
        }
    }

    public function PostValidateUsername($username)
    {
        $user = UsersApiController::GetUserByUsername($username);

        if ($user === null) {
            $userExists = false;
        } else {
            $userExists = true;
        }

        echo json_encode(
            (object)[
                "Success" => $userExists
            ]
        );
    }

    public function PostValidateEmail($email)
    {
        $user = UsersApiController::GetUserByEmail($email);

        if ($user === null) {
            $userExists = false;
        } else {
            $userExists = true;
        }

        echo json_encode(
            (object)[
                "Success" => $userExists
            ]
        );
    }


    /**
     * @param UserModel|null $user
     * @param boolean $rememberMe
     * @param null|integer $timeout
     */
    public function LoginUser($user, $name, $image, $timeout = null)
    {

        Security::SetCurrentUser($user, $name, $image, UsersApiController::GetUserPermissions($user->UserId));
        $currentUser = Security::GetCurrentUser();

        echo json_encode($currentUser);

    }

    private function _renderLoginPage()
    {
        $this->RenderView("Home/Login");
    }

} 