<?php

use Business\ApiControllers\ArticlesApiController;
use Business\ApiControllers\CommentsApiController;
use Business\Models\ArticleModel;
use Business\Models\CommentModel;

class ArticlesController extends MVCController {

	public function GetArticles() {

		$model = ArticlesApiController::GetArticles();
		echo json_encode($model);
	}

	public function GetArticle($articleId) {

		$model = new ArticleViewModel();
		$model->Article = ArticlesApiController::GetArticle($articleId);
		$model->Comments = CommentsApiController::GetArticleComments($articleId);

		echo json_encode($model);
	}

	public function PostInsertArticle($title, $subtitle, $description, $picture, $tags) {
		$model = new ArticleModel();

		$currentUser = Security::GetCurrentUser();

		if ($picture != null) {
			if ($picture['name'] !== "") {
				$newName = self::_generatePictureName($title);
				$imagePath = self::_generatePictureFullPath($newName);
				move_uploaded_file($picture['tmp_name'], $imagePath);
				$model->Picture = $newName;
			}
		}
		$model->UserId = $currentUser->UserId;
		$model->Title = $title;
		$model->Subtitle = $subtitle;
		$model->Description = $description;
		$model->DateCreated = new DateTime();
		$model->Tags = $tags;

		ArticlesApiController::InsertArticle($model);
	}

	public function PostUpdateArticle($articleId, $title, $subtitle, $description, $picture, $tags) {

		$model = ArticlesApiController::GetArticle($articleId);

		if ($picture != null) {
			if ($picture['name'] !== "") {
				$newName = self::_generatePictureName($title);
				$imagePath = self::_generatePictureFullPath($newName);
				move_uploaded_file($picture['tmp_name'], $imagePath);
				$model->Picture = $newName;
			}
		}

		$model->Title = $title;
		$model->Subtitle = $subtitle;
		$model->Description = $description;
		$model->Tags = $tags;

		ArticlesApiController::UpdateArticle($model);
	}

	public function GetDeleteArticle($articleId) {
		ArticlesApiController::DeleteArticle($articleId);
	}


	public function PostInsertArticleComment($articleId, $comment) {
		$model = new CommentModel();
		$currentUser = Security::GetCurrentUser();

		$model->ArticleId = $articleId;
		$model->UserId = $currentUser->UserId;
		$model->Comment = $comment;
		$model->DateCreated = new DateTime();

		CommentsApiController::InsertArticleComment($model);
	}

	public function PostUpdateArticleComment($articleId, $comment) {

		$model = CommentsApiController::GetArticleComment($articleId);
		$model->Comment = $comment;

		CommentsApiController::UpdateArticleComment($model);
	}

	public function GetDeleteArticleComment($commentId) {
		CommentsApiController::DeleteArticleComment($commentId);
	}


	private static function _generatePictureName($name) {
		return CommonHelper::StringToFilename(sprintf("workcraft-%s-%s.%s", $name, CommonHelper::GenerateRandomString(3), "jpg"));
	}

	private static function _generatePictureFullPath($pictureName, $thumb = null) {
		return sprintf("%s/Media/Blog/%s/%s", CDN_PATH, is_null($thumb) ? "" : $thumb . "/", $pictureName);
	}
}