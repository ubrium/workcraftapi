<?php
/**
 * Class ArticleViewModel
 * property ArticleModel $Article
 * property CommentModel $Comments[]
 */

class ArticleViewModel {
	public $Article;
	public $Comments = [];
}