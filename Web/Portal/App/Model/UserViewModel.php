<?php

use Business\Models\UserModel;

/**
 * Class UserViewModel
 * @property UserModel $User
 */
class UserViewModel {

	public $Resume;
	public $ResumeSections;
	public $BasicInfo;
	public $Education;
	public $Experience;
	public $Certificates;
	public $VolunteerWork;
	public $Languages;
	public $Skills;
	public $Interests;
	public $Intro;

	public $Portfolio;
	public $PortfolioComments;
	public $Quotes;

}