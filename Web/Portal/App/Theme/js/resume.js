/**
 * Created by mihaj on 10/12/2017.
 */

// contact user
$('#contactMessage').materialnote({
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
    ]
});


$(document).on("click", '#contactUser', function (e) {
    e.preventDefault();

    var contactName = $("#contactName").val();
    var contactEmail = $("#contactEmail").val();
    var contactSubject = $("#contactSubject").val();
    var recEmail = $("#recEmail").val();
    var recName = $("#recName").val();
    var contactMessage = $("#contactBody").find('.note-editable').html();


    $("#modalContact").modal("close");
    $("#modalPreloader").modal("open");

    $.ajax({
        url: "contact-user",
        method: "POST",
        data: {
            subject: contactSubject,
            body: contactMessage,
            name: contactName,
            email: contactEmail,
            recName: recName,
            recEmail: recEmail
        },
        success: function (data) {
            console.log(data);
            $("#modalPreloader").modal("close");
            Materialize.toast('Message sent', 4000);
        }
    });
});

// PDF download
function pdfDownload(name) {
    $(document).on("click", "#download", function (e) {
        e.preventDefault();

        html2canvas($('#resumePaper'), {
            onrendered: function (canvas) {
                canvas.id = "resumeCanvas";
                document.body.appendChild(canvas);

            }
        });

        $("#modalResumePdfDownload").modal("open");

    });
    $(document).on("click", "#pdfDownload", function (e) {
        e.preventDefault();

        var canvas = document.getElementById('resumeCanvas');
        var resume = $('#resumePaper');

        var width = Math.floor(resume.outerWidth() * 0.264583);
        var height = Math.floor(resume.outerHeight() * 0.264583);

        console.log(width);
        console.log(height);

        var imgData = canvas.toDataURL("image/jpeg", 1.0);
        var pdf = new jsPDF('p', 'mm', [height, width]);


        pdf.addImage(imgData, 'JPEG', 0, 0);
        pdf.save(name + " CV.pdf");

        $("#modalResumePdfDownload").modal("close");

    });
}
// PDF download end