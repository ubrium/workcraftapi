/**
 * Created by mihaj on 9/29/2017.
 */

function languagesEditor(resumeId, langCode) {

    $("#langLevel1").hover(
        function () {

            $("#languageLevelLabel").html("Beginner");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $("#languageLevelLabel").html("Level");
            }

        }
    );

    $("#langLevel2").hover(
        function () {

            $("#langLevel1").addClass('active');
            $("#languageLevelLabel").html("Intermediate");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $("#langLevel1").removeClass('active');
                $("#languageLevelLabel").html("Level");
            }

        }
    );

    $("#langLevel3").hover(
        function () {

            $("#langLevel1").addClass('active');
            $("#langLevel2").addClass('active');
            $("#languageLevelLabel").html("Advanced");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $(".language-level-indicator").removeClass('active');
                $("#languageLevelLabel").html("Level");
            }

        }
    );

    $("#langLevel4").hover(
        function () {

            $("#langLevel1").addClass('active');
            $("#langLevel2").addClass('active');
            $("#langLevel3").addClass('active');
            $("#languageLevelLabel").html("Proficient");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $(".language-level-indicator").removeClass('active');
                $("#languageLevelLabel").html("Level");
            }

        }
    );
    $("#langLevel5").hover(
        function () {

            $("#langLevel1").addClass('active');
            $("#langLevel2").addClass('active');
            $("#langLevel3").addClass('active');
            $("#langLevel4").addClass('active');
            $("#languageLevelLabel").html("Native");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $(".language-level-indicator").removeClass('active');
                $("#languageLevelLabel").html("Level");
            }
        }
    );


    $(document).on("click", '#addLanguage', function (e) {
        e.preventDefault();

        var name = $("#languageName").html();
        var level = $("#languageLevel").val();

        $("#modalPreloader").modal("open");

        $.ajax({
            url: "add-resume-language",
            method: "POST",
            data: {
                resumeId: resumeId,
                name: name,
                level: level,
                langCode: langCode
            },
            success: function (data) {
                console.log(data);
                $('#languageRow').html($(data).find('#languageRow').html());
                $("#modalPreloader").modal("close");
                langIndicators();
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.add-level', function (e) {
        e.preventDefault();

        var level = $(this).data("level");
        $("input#languageLevel").val(level);

        var languageLevelLabel = $("#languageLevelLabel");
        var langIndicator = $(this);
        var langIndicator1 = $("#langLevel1");
        var langIndicator2 = $("#langLevel2");
        var langIndicator3 = $("#langLevel3");
        var langIndicator4 = $("#langLevel4");
        var langIndicator5 = $("#langLevel5");

        switch (level) {
            case 1:
                langIndicator.removeClass('selected');
                langIndicator1.addClass('selected');
                languageLevelLabel.html("Beginner");
                break;
            case 2:
                langIndicator.removeClass('selected');
                langIndicator1.addClass('selected');
                langIndicator2.addClass('selected');
                languageLevelLabel.html("Intermediate");
                break;
            case 3:
                langIndicator.removeClass('selected');
                langIndicator1.addClass('selected');
                langIndicator2.addClass('selected');
                langIndicator3.addClass('selected');
                languageLevelLabel.html("Advanced");
                break;
            case 4:
                langIndicator.removeClass('selected');
                langIndicator1.addClass('selected');
                langIndicator2.addClass('selected');
                langIndicator3.addClass('selected');
                langIndicator4.addClass('selected');
                languageLevelLabel.html("Proficient");
                break;
            case 5:
                langIndicator.removeClass('selected');
                langIndicator1.addClass('selected');
                langIndicator2.addClass('selected');
                langIndicator3.addClass('selected');
                langIndicator4.addClass('selected');
                langIndicator5.addClass('selected');
                languageLevelLabel.html("Native");

        }

    });

    $(document).on("click", '.update-lang-item', function (e) {
        e.preventDefault();

        var itemId = $("#languageItemId").val();
        var name = $("#updateLanguage").val();
        var level = $("#updateLanguageLevel").val();

        $("#modalPreloader").modal("open");

        $.ajax({
            url: "update-resume-language",
            method: "POST",
            data: {
                resumeId: resumeId,
                resumeLanguageId: itemId,
                name: name,
                level: level,
                langCode: langCode
            },
            success: function (data) {
                console.log(data);
                $('#languageRow').html($(data).find('#languageRow').html());
                $("#modalPreloader").modal("close");
                overlayHide();
                langIndicators();
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.delete-lang', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        $("#modalLangUpdate").modal("close");

        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal("open");
                    $.ajax({
                        url: "delete-resume-language",
                        method: "POST",
                        data: {
                            resumeLanguageId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            console.log(data);
                            $('#languageRow').html($(data).find('#languageRow').html());
                            $("#modalPreloader").modal("close");
                            overlayHide();
                            langIndicators();
                            Materialize.toast('Item deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    });

    $(document).on("click", '.edit-resume-lang', function (e) {
        e.preventDefault();

        $('#modalLangUpdate').modal('open');
        var id = $(this).data('item-id');
        var langName = $("#langName" + id).html()
        var langLevel = $("#langLevel" + id).html()
        $("input#updateLanguage").val(langName);
        $("input#updateLanguageLevel").val(langLevel);
        $('input#updateLanguageLevel [value="' + langLevel + '"]').prop({selected: true});
        $('select').material_select();
        $("input#languageItemId").val(id);

    });

}