/**
 * Created by mihaj on 9/30/2017.
 */

function volunteerEditor(resumeId, langCode) {

    $(document).on('keydown', "h3[id^=volPosition]:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#addVol" + itemId).click();
                console.log("b" + itemId)
            } else {
                $('#addVol').click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on('keydown', "span[id^=volOrganisation]:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#addVol" + itemId).click();
                console.log("b" + itemId)
            } else {
                $('#addVol').click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on("click", '#addVol', function (e) {
        e.preventDefault();

        var position = $("#volPosition").html();
        var organisation = $("#volOrganisation").html();
        var description = $("#volDescription").html();
        var dateFrom = $("#formatVolDateFrom").data('format-date');
        var dateTo = $("#formatVolDateTo").data('format-date');

        $("#modalPreloader").modal("open");

        $.ajax({
            url: "add-resume-volunteer-work",
            method: "POST",
            data: {
                resumeId: resumeId,
                langCode: langCode,
                position: position,
                organisation: organisation,
                description: description,
                dateFrom: dateFrom,
                dateTo: dateTo
            },
            success: function (data) {
                console.log(data);
                $('#volunteerRow').html($(data).find('#volunteerRow').html());
                $("#modalPreloader").modal("close");
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.update-vol-btn', function (e) {
        e.preventDefault();

        var itemId = $(this).data('id');
        var position = $("#volPosition" + itemId).html();
        var organisation = $("#volOrganisation" + itemId).html();
        var description = $("#volDescription" + itemId).html();
        var dateFrom = $("#formatVolDateFrom" + itemId).data('format-date');
        var dateTo = $("#formatVolDateTo" + itemId).data('format-date');

        $("#modalPreloader").modal("open");

        $.ajax({
            url: "update-resume-volunteer-work",
            method: "POST",
            data: {
                resumeId: resumeId,
                langCode: langCode,
                resumeVolunteerWorkId: itemId,
                position: position,
                organisation: organisation,
                description: description,
                dateFrom: dateFrom,
                dateTo: dateTo
            },
            success: function (data) {
                $('#volunteerRow').html($(data).find('#volunteerRow').html());
                $("#modalPreloader").modal("close");
                overlayHide();
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.delete-vol', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal("open");
                    $.ajax({
                        url: "delete-resume-volunteer-work",
                        method: "POST",
                        data: {
                            resumeVolunteerWorkId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            $('#volunteerRow').html($(data).find('#volunteerRow').html());
                            $("#modalPreloader").modal("close");
                            overlayHide();
                            Materialize.toast('Item deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    });

    var onVolDateModalHide = function () {
        var startMonth = $("#volStartMonth").val();
        var startYear = $("#volStartYear").val();

        var item = $("#volStartDateUpdate").val();

        var formatStartDate = startYear + "-" + startMonth + "-01";

        if (startMonth == null || startYear == null) {
            if (item == 0) {
                $.confirm({
                    title: 'Oopss!!',
                    content: 'You need to add start date!',
                    buttons: {
                        confirm: function () {
                            $("#modalVolDate").modal("open");
                        }
                    }
                });
            }
        } else {
            var formatDateFromMonth = startMonth.replace('01', 'Jan');
            var formatDateFromMonth = formatDateFromMonth.replace('02', 'Feb');
            var formatDateFromMonth = formatDateFromMonth.replace('03', 'Mar');
            var formatDateFromMonth = formatDateFromMonth.replace('04', 'Apr');
            var formatDateFromMonth = formatDateFromMonth.replace('05', 'May');
            var formatDateFromMonth = formatDateFromMonth.replace('06', 'June');
            var formatDateFromMonth = formatDateFromMonth.replace('07', 'July');
            var formatDateFromMonth = formatDateFromMonth.replace('08', 'Aug');
            var formatDateFromMonth = formatDateFromMonth.replace('09', 'Sep');
            var formatDateFromMonth = formatDateFromMonth.replace('10', 'Oct');
            var formatDateFromMonth = formatDateFromMonth.replace('11', 'Nov');
            var formatDateFromMonth = formatDateFromMonth.replace('12', 'Dec');

            if (item == 0) {
                $("#formatVolDateFrom").html(formatDateFromMonth + "/" + startYear.slice(-2)).attr('data-format-date', formatStartDate);
            } else {
                $("#formatVolDateFrom" + item).html(formatDateFromMonth + "/" + startYear.slice(-2)).attr('data-format-date', formatStartDate);
            }
        }

        var endMonth = $("#volEndMonth").val();
        var endYear = $("#volEndYear").val();

        var formatEndDate = endYear + "-" + endMonth + "-01";

        if ($("#volTimeStatus").is(':checked')) {
            if (startMonth == null || startYear == null) {

            } else {
                if (item == 0) {
                    $("#formatVolDateTo").html('Present');
                } else {
                    $("#formatVolDateTo" + item).html('Present');
                }
            }
        } else {
            if (endMonth == null || endYear == null) {

                if (item == 0) {
                    $.confirm({
                        title: 'Oopss!!',
                        content: 'You need to add start date!',
                        buttons: {
                            confirm: function () {
                                $("#modalVolDate").modal("open");
                            }
                        }
                    });
                }

            } else {
            var formatDateToMonth = endMonth.replace('01', 'Jan');
            var formatDateToMonth = formatDateToMonth.replace('02', 'Feb');
            var formatDateToMonth = formatDateToMonth.replace('03', 'Mar');
            var formatDateToMonth = formatDateToMonth.replace('04', 'Apr');
            var formatDateToMonth = formatDateToMonth.replace('05', 'May');
            var formatDateToMonth = formatDateToMonth.replace('06', 'June');
            var formatDateToMonth = formatDateToMonth.replace('07', 'July');
            var formatDateToMonth = formatDateToMonth.replace('08', 'Aug');
            var formatDateToMonth = formatDateToMonth.replace('09', 'Sep');
            var formatDateToMonth = formatDateToMonth.replace('10', 'Oct');
            var formatDateToMonth = formatDateToMonth.replace('11', 'Nov');
            var formatDateToMonth = formatDateToMonth.replace('12', 'Dec');

            if (item == 0) {
                $("#formatVolDateTo").html(formatDateToMonth + "/" + endYear.slice(-2)).attr('data-format-date', formatEndDate);
            } else {
                $("#formatVolDateTo" + item).html(formatDateToMonth + "/" + endYear.slice(-2)).attr('data-format-date', formatEndDate);
            }
        }
        }
    };

    $("#modalVolDate").modal({
        complete: onVolDateModalHide
    });

    $(function () {

        $('#volTimeStatus').click(function () {
            if ($(this).is(':checked'))
                $("#volEndDate").hide();
            else
                $("#volEndDate").show();
        });

    });

    $(document).on("click", '.update-vol-date', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        $("input#volStartDateUpdate").val(id);

    });
}