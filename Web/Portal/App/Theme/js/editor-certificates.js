/**
 * Created by mihaj on 9/29/2017.
 */

function certificatesEditor(resumeId, langCode) {

    $(document).on('keydown', "h3[id^=certName]:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#updateCertificate" + itemId).click();
                console.log("b" + itemId)
            } else {
                $('#addCertificate').click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on('keydown', "p[id^=certOrganisation]:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#updateCertificate" + itemId).click();
                console.log("b" + itemId)
            } else {
                $('#addCertificate').click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on("click", '#addCertificate', function (e) {
        e.preventDefault();

        var name = $("#certName").html();
        var organisation = $("#certOrganisation").html();
        var date = $("#formatCertDate").data('format-date');
        $("#modalPreloader").modal("open");
        $.ajax({
            url: "add-resume-certificate",
            method: "POST",
            data: {
                resumeId: resumeId,
                langCode: langCode,
                name: name,
                organisation: organisation,
                date: date
            },
            success: function (data) {
                $('#certificateRow').html($(data).find('#certificateRow').html());
                $("#modalPreloader").modal("close");
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.update-cert-btn', function (e) {
        e.preventDefault();

        var itemId = $(this).data('id');
        var name = $("#certName" + itemId).html();
        var organisation = $("#certOrganisation" + itemId).html();
        var date = $("#formatCertDate" + itemId).data('format-date');
        $("#modalPreloader").modal("open");

        $.ajax({
            url: "update-resume-certificate",
            method: "POST",
            data: {
                resumeId: resumeId,
                langCode: langCode,
                resumeCertificateId: itemId,
                name: name,
                organisation: organisation,
                date: date
            },
            success: function (data) {
                console.log(data);
                $('#certificateRow').html($(data).find('#certificateRow').html());
                $("#modalPreloader").modal("close");
                overlayHide();
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.delete-cert', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal("open");
                    $.ajax({
                        url: "delete-resume-certificate",
                        method: "POST",
                        data: {
                            resumeCertificateId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            $('#certificateRow').html($(data).find('#certificateRow').html());
                            $("#modalPreloader").modal("close");
                            overlayHide();
                            Materialize.toast('Item deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    });

    var onCertDateModalHide = function () {
        var startMonth = $("#certStartMonth").val();
        var startYear = $("#certStartYear").val();

        var item = $("#certStartDateUpdate").val();

        var formatStartDate = startYear + "-" + startMonth + "-01";

        if (startMonth == null || startYear == null) {
            if (item == 0) {
                $.confirm({
                    title: 'Oopss!!',
                    content: 'You need to add start date!',
                    buttons: {
                        confirm: function () {
                            $("#modalCertDate").modal("open");
                        }
                    }
                });
            }
        } else {
            var formatDateFromMonth = startMonth.replace('01', 'Jan');
            var formatDateFromMonth = formatDateFromMonth.replace('02', 'Feb');
            var formatDateFromMonth = formatDateFromMonth.replace('03', 'Mar');
            var formatDateFromMonth = formatDateFromMonth.replace('04', 'Apr');
            var formatDateFromMonth = formatDateFromMonth.replace('05', 'May');
            var formatDateFromMonth = formatDateFromMonth.replace('06', 'June');
            var formatDateFromMonth = formatDateFromMonth.replace('07', 'July');
            var formatDateFromMonth = formatDateFromMonth.replace('08', 'Aug');
            var formatDateFromMonth = formatDateFromMonth.replace('09', 'Sep');
            var formatDateFromMonth = formatDateFromMonth.replace('10', 'Oct');
            var formatDateFromMonth = formatDateFromMonth.replace('11', 'Nov');
            var formatDateFromMonth = formatDateFromMonth.replace('12', 'Dec');

            if (item == 0) {
                $("#formatCertDate").html(formatDateFromMonth + "/" + startYear.slice(-2)).attr('data-format-date', formatStartDate);
            } else {
                $("#formatCertDate" + item).html(formatDateFromMonth + "/" + startYear.slice(-2)).attr('data-format-date', formatStartDate);
            }
        }
    };

    $("#modalCertDate").modal({
        complete: onCertDateModalHide
    });

    $(document).on("click", '.update-cert-date', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        $("input#certStartDateUpdate").val(id);

    });
}