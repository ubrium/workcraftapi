/**
 * Created by mihaj on 9/30/2017.
 */

function interestsEditor(resumeId, langCode) {

    $(document).on("click", '.update-int-icon', function (e) {
        e.preventDefault();

        $('#modalIcons').modal('open');
        var id = $(this).data('id');
        $("input#interestIconUpdateSelect").val(id);

    });

    $(document).on("click", '.icon-select-list i', function (e) {
        e.preventDefault();

        var icon = $(this).data('icon');
        var id = $("#interestIconUpdateSelect").val();

        if (id > 0) {
            $("input#addInterestIcon" + id).val(icon);
            $("#itemIconContainer" + id).html('<i class="update-int-icon item-icon ' + icon + '" data-id="' + id + '"></i>');

        } else {
            $("input#addInterestIcon").val(icon);
            $("#itemIconContainer").html('<i class="update-int-icon item-icon ' + icon + '" data-id="' + id + '"></i>');
        }


    });

    $(document).on('keydown', "h3[id^=interestName]:focus", function(e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if(e.keyCode == 13)
        {
            e.preventDefault();
            if(itemId > 0){
                $("#addInterest" + itemId).click();
                console.log("b" + itemId)
            } else {
                $('#addInterest').click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on('keydown', "p[id^=interestDescription]:focus", function(e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if(e.keyCode == 13)
        {
            e.preventDefault();
            if(itemId > 0){
                $("#addInterest" + itemId).click();
                console.log("b" + itemId)
            } else {
                $('#addInterest').click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on("click", '#addInterest', function (e) {
        e.preventDefault();

        var name = $("#interestName").html();
        var description = $("#interestDescription").html();
        var icon = $("#addInterestIcon").val();

        $("#modalPreloader").modal("open");

        $.ajax({
            url: "add-resume-interest",
            method: "POST",
            data: {
                resumeId: resumeId,
                langCode: langCode,
                name: name,
                description: description,
                icon: icon
            },
            success: function (data) {
                $('#interestRow').html($(data).find('#interestRow').html());
                $("#modalPreloader").modal("close");
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.update-int-btn', function (e) {
        e.preventDefault();

        var itemId = $(this).data('id');
        var name = $("#interestName" + itemId).html();
        var description = $("#interestDescription" + itemId).html();
        var icon = $("#addInterestIcon" + itemId).val();

        $("#modalPreloader").modal("open");

        $.ajax({
            url: "update-resume-interest",
            method: "POST",
            data: {
                resumeId: resumeId,
                langCode: langCode,
                resumeInterestId: itemId,
                name: name,
                description: description,
                icon: icon
            },
            success: function (data) {
                $('#interestRow').html($(data).find('#interestRow').html());
                $("#modalPreloader").modal("close");
                overlayHide();
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.delete-int', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal("open");
                    $.ajax({
                        url: "delete-resume-interest",
                        method: "POST",
                        data: {
                            resumeInterestId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            $('#interestRow').html($(data).find('#interestRow').html());
                            $("#modalPreloader").modal("close");
                            overlayHide();
                            Materialize.toast('Item deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    });

}