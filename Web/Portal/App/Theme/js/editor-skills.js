/**
 * Created by mihaj on 9/29/2017.
 */

function skillEditor(resumeId, langCode) {

    var slider = document.getElementById('test-slider');
    noUiSlider.create(slider, {
        start: 40,
        connect: [true, false],
        step: 1,
        range: {
            'min': 0,
            'max': 100
        },
        format: wNumb({
            decimals: 0
        })
    });

    $(document).on('keydown', "h3[id^=skillName]:focus", function(e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if(e.keyCode == 13)
        {
            e.preventDefault();
            $('#addSkill').click();
        }

    });


    $(document).on("click", '#addSkill', function (e) {
        e.preventDefault();

        var name = $("#skillName").html();
        var level = slider.noUiSlider.get();
        var itemId =  $("input#skillItemId").val();

        $("#modalPreloader").modal("open");
        $("#modalResumeSkillEditor").modal('close');

        if(itemId == 0){
            $.ajax({
                url: "add-resume-skill",
                method: "POST",
                data: {
                    resumeId: resumeId,
                    langCode: langCode,
                    name: name,
                    level: level
                },
                success: function (data) {
                    $('#skillsRow').html($(data).find('#skillsRow').html());
                    $("#modalPreloader").modal("close");
                    Materialize.toast('Saved', 4000);
                }
            });
        } else {
            $.ajax({
                url: "update-resume-skill",
                method: "POST",
                data: {
                    resumeId: resumeId,
                    langCode: langCode,
                    resumeSkillId: itemId,
                    name: name,
                    level: level
                },
                success: function (data) {
                    console.log(data);
                    $('#skillsRow').html($(data).find('#skillsRow').html());
                    $("#modalPreloader").modal('close');
                    Materialize.toast('Saved', 4000);
                }
            });
        }


    });

    $(document).on("click", '.delete-skill', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal("open");
                    $.ajax({
                        url: "delete-resume-skill",
                        method: "POST",
                        data: {
                            resumeSkillId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            console.log(data);
                            $('#skillsRow').html($(data).find('#skillsRow').html());
                            $("#modalPreloader").modal("close");
                            overlayHide();
                            Materialize.toast('Item deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    });


    $(document).on("click", '.add-skill-item', function (e) {
        e.preventDefault();

        $("#modalResumeSkillEditor").modal('open');
        var itemId = $(this).data('id');
        var val = $(this).data('val');

        if(itemId > 0){
            $("#skillName").html($("#skillName" + itemId).html());
        } else {
            $("#skillName").html("");
        }

        $("input#skillItemId").val(itemId);
        slider.noUiSlider.set(val);

    });

}