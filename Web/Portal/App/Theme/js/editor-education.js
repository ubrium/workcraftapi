/**
 * Created by mihaj on 9/29/2017.
 */

function educationEditor(resumeId, langCode) {

    $(document).on('keydown', "#educationRow h3:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#addEducation" + itemId).click();
                console.log("b" + itemId)
            } else {
                $("#addEducation").click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on('keydown', "#educationRow p.item-subtitle:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#addEducation" + itemId).click();
                console.log("b" + itemId)
            } else {
                $("#addEducation").click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on('keydown', "#educationRow span[id^=averageAchieved]:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#addEducation" + itemId).click();
                console.log("b" + itemId)
            } else {
                $("#addEducation").click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on('keydown', "#educationRow span[id^=averageTotal]:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#addEducation" + itemId).click();
                console.log("b" + itemId)
            } else {
                $("#addEducation").click();
                console.log("c" + itemId)
            }
        }

    });


    $(document).on("click", '.update-edu-btn', function (e) {
        e.preventDefault();

        var itemId = $(this).data('id');
        var subject = $("#subject" + itemId).html();
        var university = $("#university" + itemId).html();
        var averageAchieved = $("#averageAchieved" + itemId).html();
        var averageTotal = $("#averageTotal" + itemId).html();
        var dateFrom = $("#formatEduDateFrom" + itemId).data('format-date');
        var dateTo = $("#formatEduDateTo" + itemId).data('format-date');

        $("#modalPreloader").modal("open");

        $.ajax({
            url: "update-resume-education",
            method: "POST",
            data: {
                resumeId: resumeId,
                resumeEducationId: itemId,
                subject: subject,
                university: university,
                averageAchieved: averageAchieved,
                averageTotal: averageTotal,
                dateFrom: dateFrom,
                dateTo: dateTo,
                langCode: langCode
            },
            success: function (data) {
                $('#educationRow').html($(data).find('#educationRow').html());
                $("#modalPreloader").modal("close");
                overlayHide();
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '#addEducation', function (e) {
        e.preventDefault();

        var subject = $("#subject").html();
        var university = $("#university").html();
        var averageAchieved = $("#averageAchieved").html();
        var averageTotal = $("#averageTotal").html();
        var dateFrom = $("#formatEduDateFrom").data('format-date');
        var dateTo = $("#formatEduDateTo").data('format-date');

        $("#modalPreloader").modal("open");

        $.ajax({
            url: "add-resume-education",
            method: "POST",
            data: {
                resumeId: resumeId,
                subject: subject,
                university: university,
                averageAchieved: averageAchieved,
                averageTotal: averageTotal,
                dateFrom: dateFrom,
                dateTo: dateTo,
                langCode: langCode
            },
            success: function (data) {
                $('#educationRow').html($(data).find('#educationRow').html());
                $("#modalPreloader").modal("close");
                Materialize.toast('Saved', 4000);
            }
        });
    });

    $(document).on("click", '.delete-edu', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal("open");
                    $.ajax({
                        url: "delete-resume-education",
                        method: "POST",
                        data: {
                            resumeExperienceId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            $('#educationRow').html($(data).find('#educationRow').html());
                            $("#modalPreloader").modal("close");
                            overlayHide();
                            Materialize.toast('Item deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });


    });

    var onEduDateModalHide = function () {
        var startMonth = $("#eduStartMonth").val();
        var startYear = $("#eduStartYear").val();

        var item = $("#eduStartDateUpdate").val();

        var formatStartDate = startYear + "-" + startMonth + "-01";

        console.log(startMonth + " " + startYear);

        if (startMonth == null || startYear == null) {

            if (item == 0) {
                $.confirm({
                    title: 'Oopss!!',
                    content: 'You need to add start date!',
                    buttons: {
                        confirm: function () {
                            $("#modalEducationDate").modal("open");
                        }
                    }
                });
            }

        } else {

            var formatDateFromMonth = startMonth.replace('01', 'Jan');
            var formatDateFromMonth = formatDateFromMonth.replace('02', 'Feb');
            var formatDateFromMonth = formatDateFromMonth.replace('03', 'Mar');
            var formatDateFromMonth = formatDateFromMonth.replace('04', 'Apr');
            var formatDateFromMonth = formatDateFromMonth.replace('05', 'May');
            var formatDateFromMonth = formatDateFromMonth.replace('06', 'June');
            var formatDateFromMonth = formatDateFromMonth.replace('07', 'July');
            var formatDateFromMonth = formatDateFromMonth.replace('08', 'Aug');
            var formatDateFromMonth = formatDateFromMonth.replace('09', 'Sep');
            var formatDateFromMonth = formatDateFromMonth.replace('10', 'Oct');
            var formatDateFromMonth = formatDateFromMonth.replace('11', 'Nov');
            var formatDateFromMonth = formatDateFromMonth.replace('12', 'Dec');

            if (item == 0) {
                $("#formatEduDateFrom").html(formatDateFromMonth + "/" + startYear.slice(-2)).attr('data-format-date', formatStartDate);
            } else {
                $("#formatEduDateFrom" + item).html(formatDateFromMonth + "/" + startYear.slice(-2)).attr('data-format-date', formatStartDate);
            }

        }

        var endMonth = $("#eduEndMonth").val();
        var endYear = $("#eduEndYear").val();

        var formatEndDate = endYear + "-" + endMonth + "-01";

        if ($("#eduTimeStatus").is(':checked')) {
            if (startMonth == null || startYear == null) {

            } else {
                if (item == 0) {
                    $("#formatEduDateTo").html('Present');
                } else {
                    $("#formatEduDateTo" + item).html('Present');
                }
            }

        } else {

            if (endMonth == null || endYear == null) {

                if (item == 0) {
                    $.confirm({
                        title: 'Oopss!!',
                        content: 'You need to add start date!',
                        buttons: {
                            confirm: function () {
                                $("#modalEducationDate").modal("open");
                            }
                        }
                    });
                }

            } else {
                var formatDateToMonth = endMonth.replace('01', 'Jan');
                var formatDateToMonth = formatDateToMonth.replace('02', 'Feb');
                var formatDateToMonth = formatDateToMonth.replace('03', 'Mar');
                var formatDateToMonth = formatDateToMonth.replace('04', 'Apr');
                var formatDateToMonth = formatDateToMonth.replace('05', 'May');
                var formatDateToMonth = formatDateToMonth.replace('06', 'June');
                var formatDateToMonth = formatDateToMonth.replace('07', 'July');
                var formatDateToMonth = formatDateToMonth.replace('08', 'Aug');
                var formatDateToMonth = formatDateToMonth.replace('09', 'Sep');
                var formatDateToMonth = formatDateToMonth.replace('10', 'Oct');
                var formatDateToMonth = formatDateToMonth.replace('11', 'Nov');
                var formatDateToMonth = formatDateToMonth.replace('12', 'Dec');

                if (item == 0) {
                    $("#formatEduDateTo").html(formatDateToMonth + "/" + endYear.slice(-2)).attr('data-format-date', formatEndDate);
                } else {
                    $("#formatEduDateTo" + item).html(formatDateToMonth + "/" + endYear.slice(-2)).attr('data-format-date', formatEndDate);
                }
            }
        }
    };

    $("#modalEducationDate").modal({
        complete: onEduDateModalHide
    });


    $(function () {

        $('#eduTimeStatus').click(function () {
            if ($(this).is(':checked'))
                $("#eduEndDate").hide();
            else
                $("#eduEndDate").show();
        });

    });

    $(document).on("click", '.update-edu-date', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        $("input#eduStartDateUpdate").val(id);

    });
}