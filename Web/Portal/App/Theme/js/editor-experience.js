/**
 * Created by mihaj on 9/29/2017.
 */

function experienceEditor(resumeId, langCode) {

    $(document).on('keydown', "h3[id^=jobPosition]:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#addExperience" + itemId).click();
                console.log("b" + itemId)
            } else {
                $("#addExperience").click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on('keydown', "span[id^=company]:focus", function (e) {
        var itemId = $(this).data('id');
        console.log("a" + itemId);
        if (e.keyCode == 13) {
            e.preventDefault();
            if (itemId > 0) {
                $("#addExperience" + itemId).click();
                console.log("b" + itemId)
            } else {
                $("#addExperience").click();
                console.log("c" + itemId)
            }
        }

    });

    $(document).on("click", '#addExperience', function (e) {
        e.preventDefault();

        var position = $("#jobPosition").html();
        var company = $("#company").html();
        var description = $("#jobDesc").html();
        var dateFrom = $("#formatExpDateFrom").data("format-date");
        var dateTo = $("#formatExpDateTo").data("format-date");

        $("#modalPreloader").modal("open");

        if (position == "") {
            $.alert({
                title: 'Oops!',
                content: '<p>You need to fill in your job position!</p>'
            });
        } else if (company == "") {
            $.alert({
                title: 'Oops!',
                content: '<p>You need to add a company, office or an organisation where you worked!</p>'
            });
        } else if (dateFrom == "") {
            $.alert({
                title: 'Oops!',
                content: '<p>You need to add a date when you started working here</p>'
            });
        } else {
            $.ajax({
                url: "add-resume-experience",
                method: "POST",
                data: {
                    resumeId: resumeId,
                    langCode: langCode,
                    position: position,
                    company: company,
                    description: description,
                    dateFrom: dateFrom,
                    dateTo: dateTo
                },
                success: function (data) {
                    $('#experienceRow').html($(data).find('#experienceRow').html());
                    $("#modalPreloader").modal("close");
                    Materialize.toast('Saved', 4000);
                }
            });
        }

    });

    $(document).on("click", '.update-exp-btn', function (e) {
        e.preventDefault();

        var itemId = $(this).data('id');
        var position = $("#jobPosition" + itemId).html();
        var company = $("#company" + itemId).html();
        var description = $("#jobDesc" + itemId).html();
        var dateFrom = $("#formatExpDateFrom" + itemId).data("format-date");
        var dateTo = $("#formatExpDateTo" + itemId).data("format-date");

        $("#modalPreloader").modal("open");

        if (position == "") {
            $.alert({
                title: 'Oops!',
                content: '<p>You need to fill in your job position!</p>'
            });
        } else if (company == "") {
            $.alert({
                title: 'Oops!',
                content: '<p>You need to add a company, office or an organisation where you worked!</p>'
            });
        } else if (dateFrom == "") {
            $.alert({
                title: 'Oops!',
                content: '<p>You need to add a date when you started working here</p>'
            });
        } else {
            $.ajax({
                url: "update-resume-experience",
                method: "POST",
                data: {
                    resumeId: resumeId,
                    langCode: langCode,
                    resumeExperienceId: itemId,
                    position: position,
                    company: company,
                    description: description,
                    dateFrom: dateFrom,
                    dateTo: dateTo
                },
                success: function (data) {
                    $('#experienceRow').html($(data).find('#experienceRow').html());
                    $("#modalPreloader").modal("close");
                    overlayHide();
                    Materialize.toast('Saved', 4000);
                }
            });
        }

    });

    $(document).on("click", '.update-exp-date', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        $("input#expStartDateUpdate").val(id);

    });

    $(document).on("click", '.delete-exp', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal("open");
                    $.ajax({
                        url: "delete-resume-experience",
                        method: "POST",
                        data: {
                            resumeExperienceId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            $('#experienceRow').html($(data).find('#experienceRow').html());
                            $("#modalPreloader").modal("close");
                            overlayHide();
                            Materialize.toast('Item deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });


    });

    $(function () {

        $('#expTimeStatus').click(function () {
            if ($(this).is(':checked'))
                $("#expEndDate").hide();
            else
                $("#expEndDate").show();
        });

    });

    var onExpDateModalHide = function () {
        var startMonth = $("#expStartMonth").val();
        var startYear = $("#expStartYear").val();

        var item = $("#expStartDateUpdate").val();

        var formatStartDate = startYear + "-" + startMonth + "-01";

        if (startMonth == null || startYear == null) {
            if (item == 0) {
                $.confirm({
                    title: 'Oopss!!',
                    content: 'You need to add start date!',
                    buttons: {
                        confirm: function () {
                            $("#modalExpDate").modal("open");
                        }
                    }
                });
            }
        } else {
            var formatDateFromMonth = startMonth.replace('01', 'Jan');
            var formatDateFromMonth = formatDateFromMonth.replace('02', 'Feb');
            var formatDateFromMonth = formatDateFromMonth.replace('03', 'Mar');
            var formatDateFromMonth = formatDateFromMonth.replace('04', 'Apr');
            var formatDateFromMonth = formatDateFromMonth.replace('05', 'May');
            var formatDateFromMonth = formatDateFromMonth.replace('06', 'June');
            var formatDateFromMonth = formatDateFromMonth.replace('07', 'July');
            var formatDateFromMonth = formatDateFromMonth.replace('08', 'Aug');
            var formatDateFromMonth = formatDateFromMonth.replace('09', 'Sep');
            var formatDateFromMonth = formatDateFromMonth.replace('10', 'Oct');
            var formatDateFromMonth = formatDateFromMonth.replace('11', 'Nov');
            var formatDateFromMonth = formatDateFromMonth.replace('12', 'Dec');

            if (item == 0) {
                $("#formatExpDateFrom").html(formatDateFromMonth + "/" + startYear.slice(-2)).attr('data-format-date', formatStartDate);
            } else {
                $("#formatExpDateFrom" + item).html(formatDateFromMonth + "/" + startYear.slice(-2)).attr('data-format-date', formatStartDate);
            }
        }

        var endMonth = $("#expEndMonth").val();
        var endYear = $("#expEndYear").val();

        if (endMonth == null || endYear == null) {
            var formatEndDate = "0000-00-00";
        } else {
             formatEndDate = endYear + "-" + endMonth + "-01";
        }

        console.log(formatEndDate);

        if ($("#expTimeStatus").is(':checked')) {
            if (startMonth == null || startYear == null) {

            } else {
                if (item == 0) {
                    $("#formatExpDateTo").html('Present');
                } else {
                    $("#formatExpDateTo" + item).html('Present');
                }
            }
        } else {

            if (endMonth == null || endYear == null) {

                if (item == 0) {
                    $.confirm({
                        title: 'Oopss!!',
                        content: 'You need to add start date!',
                        buttons: {
                            confirm: function () {
                                $("#modalExpDate").modal("open");
                            }
                        }
                    });
                }

            } else {
                var formatDateToMonth = endMonth.replace('01', 'Jan');
                var formatDateToMonth = formatDateToMonth.replace('02', 'Feb');
                var formatDateToMonth = formatDateToMonth.replace('03', 'Mar');
                var formatDateToMonth = formatDateToMonth.replace('04', 'Apr');
                var formatDateToMonth = formatDateToMonth.replace('05', 'May');
                var formatDateToMonth = formatDateToMonth.replace('06', 'June');
                var formatDateToMonth = formatDateToMonth.replace('07', 'July');
                var formatDateToMonth = formatDateToMonth.replace('08', 'Aug');
                var formatDateToMonth = formatDateToMonth.replace('09', 'Sep');
                var formatDateToMonth = formatDateToMonth.replace('10', 'Oct');
                var formatDateToMonth = formatDateToMonth.replace('11', 'Nov');
                var formatDateToMonth = formatDateToMonth.replace('12', 'Dec');

                if (item == 0) {
                    $("#formatExpDateTo").html(formatDateToMonth + "/" + endYear.slice(-2)).attr('data-format-date', formatEndDate);
                } else {
                    $("#formatExpDateTo" + item).html(formatDateToMonth + "/" + endYear.slice(-2)).attr('data-format-date', formatEndDate);
                }
            }
        }
    };

    $("#modalExpDate").modal({
        complete: onExpDateModalHide
    });

}