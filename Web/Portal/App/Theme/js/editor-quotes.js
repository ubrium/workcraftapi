/**
 * Created by mihaj on 10/8/2017.
 */

function quotesEditor(resumeId, langCode) {

    $(document).on("click", '#saveQuote', function (e) {
        e.preventDefault();

        var itemId = $(this).data('id');
        var description = $("#quoteDesc").html();
        var name = $("#quoteAuthor").html();
        var position = $("#quotePosition").html();
        var picture = $("#quoteImageName").val();

        $("#modalAddQuote").modal('close');
        $("#modalPreloader").modal("open");

        if(itemId == 0) {
            $.ajax({
                url: "add-resume-quote",
                method: "POST",
                data: {
                    resumeId: resumeId,
                    description: description,
                    name: name,
                    position: position,
                    picture: picture,
                    langCode: langCode
                },
                success: function (data) {
                    $('#quotesRow').html($(data).find('#quotesRow').html()).slick({
                        infinite: true,
                        slidesToShow: 2,
                        slidesToScroll: 1
                    });
                    $("#modalPreloader").modal('close');
                    Materialize.toast('Saved', 4000);
                }
            });
        } else {
            $.ajax({
                url: "update-resume-quote",
                method: "POST",
                data: {
                    resumeId: resumeId,
                    resumeQuoteId: itemId,
                    description: description,
                    name: name,
                    position: position,
                    picture: picture,
                    langCode: langCode
                },
                success: function (data) {
                    $('#quotesRow').html($(data).find('#quotesRow').html()).slick({
                        infinite: true,
                        slidesToShow: 2,
                        slidesToScroll: 1
                    });
                    $("#modalPreloader").modal('close');
                    Materialize.toast('Saved', 4000);
                }
            });
        }
    });

    $(document).on("click", '.delete-quote', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal('open');
                    $.ajax({
                        url: "delete-resume-quote",
                        method: "POST",
                        data: {
                            resumeQuoteId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            $('#quotesRow').html($(data).find('#quotesRow').html()).slick({
                                infinite: true,
                                slidesToShow: 2,
                                slidesToScroll: 1
                            });
                            $("#modalPreloader").modal('close');
                            Materialize.toast('Quote deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    });

    (function () {
        var input = document.getElementById("imageQuote"),
            formdata = false;

        function showUploadedItem(source) {
            var list = document.getElementById("image-list"),
                li = document.createElement("li"),
                img = document.createElement("img");
            img.src = source;
            li.appendChild(img);
            list.appendChild(li);
        }

        if (window.FormData) {
            formdata = new FormData();
            document.getElementById("btn2").style.display = "none";
        }

        input.addEventListener("change", function (evt) {
            $(".paper").append(
                "<img src='Theme/AjaxLoader/squares.gif' />"
            );
            var i = 0, len = this.files.length, img, reader, file;

            for (; i < len; i++) {
                file = this.files[i];

                if (!!file.type.match(/image.*/)) {
                    if (window.FileReader) {
                        reader = new FileReader();
                        reader.onloadend = function (e) {
                            showUploadedItem(e.target.result, file.fileName);
                        };
                        reader.readAsDataURL(file);
                    }
                    if (formdata) {
                        formdata.append("images[]", file);
                    }
                }
            }

            if (formdata) {
                $.ajax({
                    url: "upload-quote-image",
                    type: "POST",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $("#quoteImageContainer").html(data);
                    }
                });
            }
        }, false);
    }());

}