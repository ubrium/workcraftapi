/**
 * Created by mihaj on 9/21/2017.
 */

function editor(resumeId, langCode) {

    /* Resume basics */

    $(document).on("click", "ul.template-selector li a", function (e) {
        e.preventDefault();
        var currentTheme = $('body').attr('class');
        var theme = $(this).data('theme');
        var themeId = $(this).data('theme-id');
        $("body").removeClass(currentTheme).addClass(theme);
        $('.theme-selector ul li').removeClass('active');
        $(this).addClass('active');
        $("#modalThemeChanger").modal('close');

        $.ajax({
            url: "update-resume",
            method: "POST",
            data: {
                themeId: themeId,
                resumeId: resumeId
            },
            success: function (data) {
                Materialize.toast('Changes saved', 4000);
            }
        });

    });

    $(document).on("click", "#themeChanger", function (e) {
        e.preventDefault();

        $("#modalThemeChanger").modal('open');

    });

    $(document).on("click", "#addSectionTrigger", function (e) {
        e.preventDefault();

        if ($("#educationRow").length > 0) {
            $("[data-div=#educationRow]").hide();
        } else {
            $("[data-div=#educationRow]").show();
        }

        if ($("#experienceRow").length > 0) {
            $("[data-div=#experienceRow]").hide();
        } else {
            $("[data-div=#experienceRow]").show();
        }

        if ($("#skillsRow").length > 0) {
            $("[data-div=#skillsRow]").hide();
        } else {
            $("[data-div=#skillsRow]").show();
        }

        if ($("#languageRow").length > 0) {
            $("[data-div=#languageRow]").hide();
        } else {
            $("[data-div=#languageRow]").show();
        }

        if ($("#certificateRow").length > 0) {
            $("[data-div=#certificateRow]").hide();
        } else {
            $("[data-div=#certificateRow]").show();
        }

        if ($("#volunteerRow").length > 0) {
            $("[data-div=#volunteerRow]").hide();
        } else {
            $("[data-div=#volunteerRow]").show();
        }

        if ($("#interestRow").length > 0) {
            $("[data-div=#interestRow]").hide();
        } else {
            $("[data-div=#interestRow]").show();
        }

        if ($("#portfolioRow").length > 0) {
            $("[data-div=#portfolioRow]").hide();
        } else {
            $("[data-div=#portfolioRow]").show();
        }

        if ($("#quotesRow").length > 0) {
            $("[data-div=#quotesRow]").hide();
        } else {
            $("[data-div=#quotesRow]").show();
        }

        if ($("#introRow").length > 0) {
            $("[data-div=#introRow]").hide();
        } else {
            $("[data-div=#introRow]").show();
        }

        $("#modalSectionsUpdate").modal('open');

    });

    $(document).on("click", "a.disabled", function (e) {
        e.preventDefault();
    });

    $('#userAbout').materialnote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

    $('#introMessage').materialnote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
        ]
    });

    $('#caseBody').materialnote({
        toolbar: [
            // [groupName, [list of button]]
            ['insert', ['picture', 'video', 'link']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph', 'style']],
            ['height', ['height']],
        ],
        minHeight: null,             // set minimum height of editor
        maxHeight: null,
        lang: 'en-US',
        callback: {
            onImageUpload: testFunc('Test radi')
        }
    });

    $('.note-toolbar .btn').tooltip({delay: 50});

    $('#aboutMe').find('.note-editable').focusout(function () {
        updateBasics(resumeId);
    });

    $("#userDesc").focusout(function () {
        updateBasics(resumeId);
    });

    $("#userName").focusout(function () {
        updateBasics(resumeId);
    });

    $("#userPhone").focusout(function () {
        updateBasics(resumeId);
    });

    $("#userWebsite").focusout(function () {
        updateBasics(resumeId);
    });

    $("#userEmail").focusout(function () {
        updateBasics(resumeId);
    });

    $(document).on('keydown', "#userName, #userPhone, #userEmail, #userWebsite, #userDesc", function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            var id = $(this).attr('id');
            console.log(id);
            //updateBasics(resumeId);
            if (id == "userName") {
                $("#userName").blur();
            }

            if (id == "userPhone") {
                $("#userPhone").blur();
            }

            if (id == "userEmail") {
                $("#userEmail").blur();
            }

            if (id == "userWebsite") {
                $("#userEmail").blur();
            }

            if (id == "userDesc") {
                $("#userEmail").blur();
            }
        }

    });

    $("h1#userName:focus, #userDesc:focus, #userPhone:focus, #userEmail:focus, #userWebsite:focus").on('keydown', function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            updateBasics(resumeId);
        }

    });

    /*Resume sections*/

    $(document).on("click", '.add-section', function (e) {
        e.preventDefault();

        var sectionId = $(this).data("id");
        var sectionName = $(this).data('name');
        var sectionDiv = $(this).data('div');
        var sectionType = $(this).data('type');


        $.ajax({
            url: "add-resume-section",
            method: "POST",
            data: {
                resumeId: resumeId,
                contentSectionId: sectionId,
                langCode: langCode,
                name: sectionName
            },
            success: function (data) {
                if (sectionType == 1) {
                    console.log(data);
                    $('#resumeContentItems').html($(data).find('#resumeContentItems').html());
                    $('#modalSectionsUpdate').modal('close');

                    if (sectionId == 4) {
                        langIndicators();
                    }

                    $('html, body').animate({
                        scrollTop: $(sectionDiv).offset().top - 120
                    }, 1000);
                    Materialize.toast('Section added', 4000);
                } else if (sectionType == 3) {
                    $.ajax({
                        url: "add-resume-intro",
                        method: "POST",
                        data: {
                            resumeId: resumeId
                        },
                        success: function (data) {
                            Materialize.toast('Section added', 4000);
                        }
                    });
                    $('#introSection').html($(data).find('#introSection').html());
                    $('html, body').animate({
                        scrollTop: $(sectionDiv).offset().top - 120
                    }, 1000);
                    $('nav').addClass('welcome');
                } else {
                    console.log(data);
                    $('#secondarySections').html($(data).find('#secondarySections').html());
                    $('#userNav').html($(data).find('#userNav').html());
                    $('#modalSectionsUpdate').modal('close');
                    $('.tooltipped').tooltip({delay: 50});
                    $('html, body').animate({
                        scrollTop: $(sectionDiv).offset().top - 120
                    }, 1000);
                    Materialize.toast('Section added', 4000);
                }
            }
        });
    });

    $(document).on("click", '.delete-section', function (e) {
        e.preventDefault();

        var sectionId = $(this).data("id");
        var sectionType = $(this).data('type');

        $.confirm({
            title: 'Are you sure?',
            content: "<p>Are you sure you want to delete this section? Items you've added to this section won't be deleted.</p>",
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal("open");
                    $.ajax({
                        url: "delete-resume-section",
                        method: "POST",
                        data: {
                            resumeSectionId: sectionId,
                            langCode: langCode
                        },
                        success: function (data) {
                            if (sectionType == 1) {
                                console.log(data);
                                $('#resumeContentItems').html($(data).find('#resumeContentItems').html());
                                $('#modalSectionsUpdate').modal('close');

                                if (sectionId == 4) {
                                    langIndicators();
                                }

                                Materialize.toast('Section deleted', 4000);
                            } else {
                                console.log(data);
                                $('#secondarySections').html($(data).find('#secondarySections').html());
                                $('#userNav').html($(data).find('#userNav').html());
                                $('#modalSectionsUpdate').modal('close');

                                $('.tooltipped').tooltip({delay: 50});
                                Materialize.toast('Section deleted', 4000);
                            }
                            $("#modalPreloader").modal("close");
                        }
                    });
                },
                cancel: function () {
                }
            }
        });

    });

    $(document).on("click", '.add-item', function (e) {
        e.preventDefault();

        var target = $(this).data("target");
        $(target).toggle();
        $('html, body').animate({
            scrollTop: $(target).offset().top - 120
        }, 1000);
    });

    $(document).on("click", '#saveIntro', function (e) {
        e.preventDefault();

        var picture = $("input#introPicture").val();
        var text = $("#introRow").find(".note-editable").html();
        var resumeIntroId = $(this).data('id');


        $.ajax({
            url: "update-resume-intro",
            method: "POST",
            data: {
                resumeId: resumeId,
                resumeIntroId: resumeIntroId,
                picture: picture,
                text: text
            },
            success: function (data) {
                console.log(data);
                Materialize.toast('Saved', 4000);
            }
        });
    });

    /*Initiate section scripts*/
    educationEditor(resumeId, langCode);
    experienceEditor(resumeId, langCode);
    languagesEditor(resumeId, langCode);
    skillEditor(resumeId, langCode);
    certificatesEditor(resumeId, langCode);
    volunteerEditor(resumeId, langCode);
    interestsEditor(resumeId, langCode);
    quotesEditor(resumeId, langCode);

    /* edit items */
    $(document).on("click", '.edit-item', function (e) {
        e.preventDefault();

        $(this).addClass('editor-activated');
        var item = $(this).data('item-id');
        $('#editExpBtn' + item).addClass('show');
        $('#editEduBtn' + item).addClass('show');
        $('.editor-overlay').addClass('activated');
        $('.navbar-fixed').addClass('absolute');

    });

    $(document).on("click", '.editor-overlay', function (e) {
        e.preventDefault();

        $(this).removeClass('activated');
        $('.edit-item .edit-btn').removeClass('show');
        $('.edit-item').removeClass('editor-activated');
        $('.navbar-fixed').removeClass('absolute');

    });


    // portfolio items

    $(document).on("click", '.delete-portfolio-item', function (e) {
        e.preventDefault();

        var id = $(this).data('id');


        $.confirm({
            title: 'Are you sure?',
            content: '<p>Are you sure you want to delete this item?</p>',
            theme: 'material',
            buttons: {
                confirm: function () {
                    $("#modalPreloader").modal('open');
                    $.ajax({
                        url: "delete-portfolio-item",
                        method: "POST",
                        data: {
                            resumePortfolioId: id,
                            langCode: langCode
                        },
                        success: function (data) {
                            console.log(data);
                            $('#portfolioRow').html($(data).find('#portfolioRow').html());
                            $("#modalPreloader").modal('close');
                            Materialize.toast('Article deleted', 4000);
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    });

}

function updateBasics(resumeId) {
    var about = $('#aboutMe').find('.note-editable').html();
    var description = $('#userDesc').html();
    var name = $('#userName').html();
    var phone = $('#userPhone').html();
    var website = $('#userWebsite').html();
    var country = $('#countryCode').data('code');
    var email = $('#userEmail').html();
    var picture = $('input#userImagePath').val();
    var resumeBasicId = $('input#resumeBasicId').val();

    $.ajax({
        url: "update-resume-basics",
        method: "POST",
        data: {
            resumeBasicId: resumeBasicId,
            about: about,
            name: name,
            phone: phone,
            website: website,
            description: description,
            contactEmail: email,
            countryCode: country,
            picture: picture,
            resumeId: resumeId
        },
        success: function (data) {
            console.log(data);
            $('#pageName').html(name);
            $('title').html(name + ' - Workcraft.co');
            Materialize.toast('Changes saved', 4000);
        }
    });
}

function overlayHide() {
    $('.editor-overlay').removeClass('activated');
    $('.edit-item .edit-btn').removeClass('show');
    $('.edit-item').removeClass('editor-activated');
    $('.navbar-fixed').removeClass('absolute');
}

function skillLevelUpdate(value) {

    var itemId = $('.editor-activated').data('item-id');
    console.log(itemId);
    if (itemId > 0) {
        $("input#skillLevel" + itemId).val(value);
    } else {
        $("input#skillLevel").val(value);
    }
}

function langIndicators() {
    $("#langLevel1").hover(
        function () {

            $("#languageLevelLabel").html("Beginner");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $("#languageLevelLabel").html("Level");
            }

        }
    );

    $("#langLevel2").hover(
        function () {

            $("#langLevel1").addClass('active');
            $("#languageLevelLabel").html("Intermediate");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $("#langLevel1").removeClass('active');
                $("#languageLevelLabel").html("Level");
            }

        }
    );

    $("#langLevel3").hover(
        function () {

            $("#langLevel1").addClass('active');
            $("#langLevel2").addClass('active');
            $("#languageLevelLabel").html("Advanced");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $(".language-level-indicator").removeClass('active');
                $("#languageLevelLabel").html("Level");
            }

        }
    );

    $("#langLevel4").hover(
        function () {

            $("#langLevel1").addClass('active');
            $("#langLevel2").addClass('active');
            $("#langLevel3").addClass('active');
            $("#languageLevelLabel").html("Proficient");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $(".language-level-indicator").removeClass('active');
                $("#languageLevelLabel").html("Level");
            }

        }
    );
    $("#langLevel5").hover(
        function () {

            $("#langLevel1").addClass('active');
            $("#langLevel2").addClass('active');
            $("#langLevel3").addClass('active');
            $("#langLevel4").addClass('active');
            $("#languageLevelLabel").html("Native");

        }, function () {
            var levelValue = $("input#languageLevel").val();
            if (levelValue != "" || levelValue < 1) {
                $(".language-level-indicator").removeClass('active');
                $("#languageLevelLabel").html("Level");
            }
        }
    );
}

function uploadPortfolioGallery(itemId, resumeId, edit) {

    /*(function () {
        var input = document.getElementById("imageGallery"),
            formdata = false;


        if (window.FormData) {
            formdata = new FormData();
            document.getElementById("btn2").style.display = "none";
        }

        input.addEventListener("change", function (evt) {

            $("#modalPreloader").modal("open");

            var file = this.files[0];

            if (!!file.type.match(/image.*!/)) {
                if (window.FileReader) {
                    reader = new FileReader();

                    reader.readAsDataURL(file);
                }
                if (formdata) {
                    formdata.append("images[0]", file);
                    formdata.append("itemId", itemId);
                    formdata.append("resumeId", resumeId);
                    formdata.append("state", edit);
                }
            }

            if (formdata) {
                $.ajax({
                    url: "upload-portfolio-images",
                    type: "POST",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $('.portfolio-gallery').html($(data).find('.portfolio-gallery').html());
                        $('.materialboxed').materialbox();
                        uploadPortfolioGallery(itemId, resumeId)
                        $("#modalPreloader").modal("close");
                    }
                });
            }
        }, false);
    }());*/

}


function uploadIntroImage(resumeId){
    (function () {
        var input = document.getElementById("imageIntro"),
            formdata = false;


        if (window.FormData) {
            formdata = new FormData();
            document.getElementById("btn3").style.display = "none";
        }

        input.addEventListener("change", function (evt) {
            $("#modalPreloader").modal("open");

            var file = this.files[0];

            if (!!file.type.match(/image.*/)) {
                if (window.FileReader) {
                    reader = new FileReader();

                    reader.readAsDataURL(file);
                }
                if (formdata) {
                    formdata.append("images[0]", file);
                    formdata.append("resumeId", resumeId);
                }
            }

            if (formdata) {
                $.ajax({
                    url: "upload-intro-image",
                    type: "POST",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $("#introImageContainer").html(data);
                        $('.parallax').parallax();
                        $("#modalPreloader").modal("close");
                    }
                });
            }
        }, false);
    }());
}



function uploadPortfolioHeader(itemId, resumeId) {

    (function () {
        var input = document.getElementById("imageHeader"),
            formdata = false;


        if (window.FormData) {
            formdata = new FormData();
            document.getElementById("btn2").style.display = "none";
        }

        input.addEventListener("change", function (evt) {

            $("#modalPreloader").modal("open");

            var file = this.files[0];

            if (!!file.type.match(/image.*/)) {
                if (window.FileReader) {
                    reader = new FileReader();

                    reader.readAsDataURL(file);
                }
                if (formdata) {
                    formdata.append("images[0]", file);
                    formdata.append("itemId", itemId);
                    formdata.append("resumeId", resumeId);
                }
            }

            if (formdata) {
                $.ajax({
                    url: "upload-portfolio-header",
                    type: "POST",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $("#headerImageContainer").html(data);
                        $("#modalPreloader").modal("close");
                    }
                });
            }
        }, false);
    }());

}

function uploadImage(image) {
    var data = new FormData();
    data.append("image", image);
    $.ajax({
        url: 'summernote-upload',
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        type: "post",
        success: function (url) {
            var image = $('<img>').attr('src', 'http://' + url);
            $('.summernote').materialnote("insertNode", image[0]);
            //$("#showError").html(url);
        },
        error: function (data) {
            console.log(data);
            //$("#showError").html(data);
        }
    });
}

function testFunc(para) {
    console.log(para);
}