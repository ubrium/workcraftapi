<?php

use Business\ApiControllers\UsersApiController;
use Business\Enums\RolesEnum;
use Business\Models\UserModel;

class Security
{

	const UserAccessTokenName = "UserAccessToken";

	public static function CheckPermissions(array $permissions) {
		$currentUser = self::GetCurrentUser();
		if (property_exists($currentUser, "RoleId") && $currentUser->RoleId === RolesEnum::Visitor) {
			global $router;
			Session::Set("Route", $router->Route);
			Router::Redirect(self::RoleDefaultPageRoute(RolesEnum::Visitor));
		}
		if (array_intersect($permissions, $currentUser->Permissions) == $permissions) {
			return true;
		} else {
			return false;
		}
	}

	public static function RoleDefaultPageRoute($roleId = null) {
		switch ($roleId) {
			case RolesEnum::Visitor:
				$route = "login";
				break;
			case RolesEnum::User:
				$route = "home";
				break;
			case RolesEnum::Admin:
				$route = "home";
				break;
			default:
				$route = "home";
				break;
		}

		return $route;
	}

	public static function SetCurrentVisitor() {
		$currentUserDto = new CurrentUserDTO();
		$currentUserDto->RoleId = RolesEnum::Visitor;
		$currentUserDto->Permissions = UsersApiController::GetRolePermissions(RolesEnum::Visitor);

		Session::Set("CurrentUser", $currentUserDto);
	}

	/**
	 * @param UserModel $user
	 * @param $name
	 * @param int[] $permissions
	 */
	public static function SetCurrentUser($user, $name, $image, $permissions) {
		// Packing CurrentUserDTO from user
		$currentUserDto = new CurrentUserDTO();

		$currentUserDto->UserId = $user->UserId;
		$currentUserDto->Name = $name;
		$currentUserDto->Email = $user->Email;
		$currentUserDto->Image = $image;
		$currentUserDto->RoleId = RolesEnum::User;
		$currentUserDto->Permissions = $permissions;

		Session::Set("CurrentUser", $currentUserDto);
	}

	/**
	 * @return CurrentUserDTO|bool
	 */
	public static function GetCurrentUser() {
		return Session::Get("CurrentUser");
	}

	/**
	 * @return bool
	 */
	public static function UserAccessTokenExists() {
		return Cookie::Exists(self::UserAccessTokenName);
	}

	/**
	 * @return mixed
	 */
	public static function GetUserAccessToken() {
		return Cookie::Get(self::UserAccessTokenName);
	}

	/**
	 * @return bool
	 */
	public static function SetUserAccessToken($userAccessToken) {
		return Cookie::Set(self::UserAccessTokenName, $userAccessToken, Cookie::ThirtyDays);
	}

	/**
	 * @return bool
	 */
	public static function RemoveUserAccessToken() {
		return Cookie::Delete(self::UserAccessTokenName, "/", false, true);
	}

	/**
	 * @return bool
	 */
	public static function IsLoggedIn() {
		$currentUser = self::GetCurrentUser();
		if ($currentUser === false || $currentUser->RoleId === RolesEnum::Visitor) {
			return false;
		}
		return true;
	}

	public static function CheckLogin() {
		if (self::GetCurrentUser() === false) {
			self::SetCurrentVisitor();
			if (self::UserAccessTokenExists() === true) {
				$user = UsersApiController::CheckLogin(null, null, self::GetUserAccessToken());
				if (!is_null($user)) {
					self::SetCurrentUser($user, UsersApiController::GetUserPermissions($user->UserId));
				} else {
					self::RemoveUserAccessToken();
				}
			}
		}
	}

} 