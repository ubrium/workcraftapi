<?php

// Debug, maintenance and cache
define('DEBUG_MODE_PORTAL', false);
define('DEBUG_MODE_ADMIN', false);
define('MAINTENANCE_MODE_PORTAL', false);
define('MAINTENANCE_MODE_ADMIN', false);
define('USE_CACHE_PORTAL', false);
define('USE_CACHE_ADMIN', false);

// Clients
define('PORTAL_URL', 'http://workcraftapi.test/');
define('ADMIN_URL', 'http://admin.workcraftapi.test/');
define("CDN_URL", "http://workcraftapi.test/CDN/");
define("CDN_PATH", dirname(__FILE__) . "/Web/Portal/App/CDN");

// Languages and gettext
define('PROJECT_DIR', realpath('./'));
define('LOCALE_DIR', PROJECT_DIR . '/Locale');
define('DEFAULT_LOCALE', 'en_US');
define('DEFAULT_LANGUAGE', 'en');

// SMTP
define('USE_SMTP', true);
define('SMTP_HOST', 'localhost');
define('SMTP_PORT', 25);
define('SMTP_USERNAME', '');
define('SMTP_PASSWORD', '');