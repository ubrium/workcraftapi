<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 17.09
 */

namespace Business\Models;

/**
 * Class ResumeModel
 * @package Business\Modles
 * @property integer $ResumeId;
 * @property integer $UserId;
 * @property integer $LanguageCode;
 * @property integer $ThemeId;
 * @property integer $Status;
 * @property integer $DateCreated;
 * @property integer $Name;
 * @property integer $Description;
 * @property integer $About;
 */
class ResumeInterestModel
{

    public $ResumeInterestId;
    public $ResumeId;
    public $Name;
    public $Description;
    public $Icon;

}