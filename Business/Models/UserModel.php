<?php
namespace Business\Models;

/**
 * Class UserModel
 * @package Business\Models
 * @property integer $UserId
 * @property string $Email
 * @property string $Name
 * @property string $Username
 * @property string $Password
 * @property string $RegistrationDate
 * @property string $Image
 * @property int $Active
 * @property int $ConfirmRegistration
 */
class UserModel
{
    public $UserId;
    public $Email;
    public $Password;
	public $Image;
	public $RegistrationDate;
	public $ConfirmRegistration;
    public $Active;

    public function GetImageUrl() {
        if (!empty($this->Image)) {
        	if(file_exists($this->GetImagePath($this->Image)) === true) {
				return sprintf("%sMedia/Users/%s", CDN_URL, $this->Image);
			}
        }
		return sprintf("%s/Media/DefaultImages/user-default.jpg", CDN_URL);
    }

	public function GetImagePath($imageName) {
		return sprintf("%s/Media/Users/%s", CDN_PATH, $imageName);
	}
}