<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 17.09
 */

namespace Business\Models;


class ResumePortfolioModel
{

    public $ResumePortfolioId;
    public $ResumeId;
    public $Title;
    public $CaseStudy;
    public $Category;
    public $Picture;
    public $ShortDescription;
    public $PublishStatusId;
    public $ParentId;
    public $DatePublished;
    public $DateUpdated;

    public function GetImageUrl() {
        if (!empty($this->Picture)) {
            if(file_exists($this->GetImagePath($this->Picture)) === true) {
                return sprintf("%sMedia/Projects/%s", CDN_URL, $this->Picture);
            }
        }
        return sprintf("%s/Media/DefaultImages/user-default.jpg", CDN_URL);
    }

    public function GetImagePath($imageName) {
        return sprintf("%s/Media/Projects/%s", CDN_PATH, $imageName);
    }

}