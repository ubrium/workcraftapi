<?php

/**
 * Class JobModel
 * @package Business\Models
 * @property integer $JobId
 * @property integer $UserId
 * @property string $Type
 * @property string $ExperienceLevel
 * @property string $Role
 * @property string $Technologies
 * @property string $Description
 */

namespace Business\Models;

class JobModel {

	public $JobId;
	public $UserId;
	public $Type;
	public $ExperienceLevel;
	public $Role;
	public $Technologies;
	public $Description;
}