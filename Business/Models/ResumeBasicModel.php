<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 17.09
 */

namespace Business\Models;

/**
 * Class ResumeModel
 * @package Business\Modles
 * @property integer $ResumeId;
 * @property integer $UserId;
 * @property integer $LanguageCode;
 * @property integer $ThemeId;
 * @property integer $Status;
 * @property integer $DateCreated;
 * @property integer $Name;
 * @property integer $Description;
 * @property integer $About;
 */
class ResumeBasicModel
{

    public $ResumeBasicId;
    public $Name;
    public $Description;
    public $About;
    public $ContactEmail;
    public $Website;
    public $CountryCode;
    public $Phone;
    public $ResumeId;
    public $Picture;

    public function PictureSource() {
        if (!empty($this->Picture)) {
            if(file_exists($this->ImagePath($this->Picture)) === true) {
                return sprintf("%sMedia/Users/%s", CDN_URL, $this->Picture);
            }
        }
        return sprintf("%s/Media/Users/default.jpg", CDN_URL);
    }

    public function ImagePath($name) {
        return sprintf("%s/Media/Users/%s", CDN_PATH, $name);
    }

    public function DownloadImage($name) {
        file_put_contents($this->ImagePath($name), file_get_contents($this->Picture));
    }

}