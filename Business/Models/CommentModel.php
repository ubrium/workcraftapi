<?php

namespace Business\Models;

/**
 * Class CommentModel
 * @package Business\Models
 * @property integer $CommentId
 * @property integer $ArticleId
 * @property integer $UserId
 * @property string $Comment
 * @property string $DateCreated
 */
class CommentModel {
	public $CommentId;
	public $ArticleId;
	public $UserId;
	public $Comment;
	public $DateCreated;
}