<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 17.09
 */

namespace Business\Models;

class ResumeModel
{

    public $ResumeId;
    public $UserId;
    public $TemplateId;
    public $ThemeId;
    public $StatusId;
    public $Views;
    public $LanguageCode;

}