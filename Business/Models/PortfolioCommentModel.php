<?php

namespace Business\Models;

/**
 * Class PortfolioCommentModel
 * @package Business\Models
 * property int $PortfolioId
 * property int $UserId
 * property string $Comment
 */

class PortfolioCommentModel {

	public $PortfolioId;
	public $UserId;
	public $Comment;
}