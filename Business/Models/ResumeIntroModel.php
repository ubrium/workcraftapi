<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 17.09
 */

namespace Business\Models;

class ResumeIntroModel
{

    public $ResumeIntroId;
    public $Picture;
    public $BackgroundColor;
    public $Text;
    public $ResumeId;

    public function PictureSource() {
        if (!empty($this->Picture)) {
            if(file_exists($this->ImagePath($this->Picture)) === true) {
                return sprintf("%sMedia/Users/%s", CDN_URL, $this->Picture);
            }
        }
        return sprintf("%s/Media/Users/abg4.jpg", CDN_URL);
    }

    public function ImagePath($name) {
        return sprintf("%s/Media/Users/%s", CDN_PATH, $name);
    }

    public function DownloadImage($name) {
        file_put_contents($this->ImagePath($name), file_get_contents($this->Picture));
    }

}