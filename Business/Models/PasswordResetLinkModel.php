<?php
namespace Business\Models;

use DateTime;
/**
 * Class PasswordResetLinkModel
 * @package Business\Models
 * @property integer $PasswordResetLinkId
 * @property integer $UserId
 * @property String $ExpirationDate
 * @property String $ResetLink
 */
class PasswordResetLinkModel {

	public $PasswordResetLinkId;
	public $UserId;
	public $ExpirationDate;
	public $ResetLink;

	function __construct()
	{
		$this->ExpirationDate = new DateTime();
		$this->ExpirationDate->modify("+168 hours"); //seven days
		$this->ExpirationDate = $this->ExpirationDate->format("Y-m-d H:i:s");
	}
}