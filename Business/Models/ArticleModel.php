<?php

namespace Business\Models;

/**
 * Class ArticleModel
 * @package Business\Models
 * @property integer $ArticleId
 * @property integer $UserId
 * @property string $Title
 * @property string $Subtitle
 * @property string $Description
 * @property string $Picture
 * @property string $DateCreated
 * @property string $Tags
 */
class ArticleModel {
	public $ArticleId;
	public $UserId;
	public $Title;
	public $Subtitle;
	public $Description;
	public $Picture;
	public $DateCreated;
	public $Tags;

	public function GetPictureUrl() {
		if (!empty($this->Picture)) {
			if (file_exists($this->GetPicturePath($this->Picture)) === true) {
				return sprintf("%sMedia/Blog/%s", CDN_URL, $this->Picture);
			}
		}
		return sprintf("%s/Media/Blog/blog-default.jpg", CDN_URL);
	}

	public function GetPicturePath($pictureName) {
		return sprintf("%s/Media/Blog/%s", CDN_PATH, $pictureName);
	}
}