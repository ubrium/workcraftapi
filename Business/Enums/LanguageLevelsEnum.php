<?php

namespace Business\Enums;


class LanguageLevelsEnum extends BaseEnum {

    const Beginner = 1;
    const Intermediate = 2;
    const Advanced = 3;
    const Proficient = 4;
    const Native = 5;

    public $Descriptions = [];

    public function __construct() {
        $this->Descriptions = [
            1 => "Beginner",
            2 => "Intermediate",
            3 => "Advanced",
            4 => "Proficient",
            5 => "Native"

        ];
    }

}