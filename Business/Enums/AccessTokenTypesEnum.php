<?php

namespace Business\Enums;


class AccessTokenTypesEnum extends BaseEnum {

    const Admin = 1;
    const Portal = 2;

}