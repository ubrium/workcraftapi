<?php

namespace Business\Security;

use Business\Models\UserModel;
use Business\Models\UserRoleModel;
use Data\Repositories\UserAccessTokensRepository;
use Data\Repositories\UserRolesRepository;
use Data\Repositories\UsersRepository;

/**
 * Class Users
 * @package Security
 *
 * Handles user management tasks
 */
class Users {

    /**
     * Login based on username and password. Returns complete user data.
     *
     * @param string $username
     * @param string $password
     *
     * @return UserModel|null
     */
    public static function CheckUsernameAndPassword($username, $password) {
        $user = UsersRepository::GetOne([UsersRepository::COLUMN_USERNAME => $username]);
        if (!is_null($user)) {
            if (Crypt::CheckPassword($password, $user->Password)) {
                return $user;
            }
        }

        return null;
    }

    /**
     * Login based on email and password. Returns complete user data.
     *
     * @param string $email
     * @param string $password
     *
     * @return UserModel|null
     */
    public static function CheckEmailAndPassword($email, $password) {
        $user = UsersRepository::GetOne([UsersRepository::COLUMN_EMAIL => $email]);
        if (!is_null($user)) {
            if (Crypt::CheckPassword($password, $user->Password)) {
                return $user;
            }
        }

        return null;
    }

    /**
     * Log in based on access token. Returns complete user data.
     *
     * @param $token
     * @return UserModel|null
     */
    public static function CheckToken($token) {
        $token = UserAccessTokensRepository::GetOne([UserAccessTokensRepository::COLUMN_TOKEN => $token]);
        if (!is_null($token) && is_null($token->EndDate)) {
            return UsersRepository::GetOne(['UserId' => $token->UserId]);
        }

        return null;
    }


    /**
     * Creates new User, assigns new created UserId and returns User object. Returns false if unsuccessful.
     *
     * @param string $name
     * @param string $email
     * @param string $username
     * @param string $password
     * @param string $image
     * @param int $confirmRegistration
     * @param int $active
     * @param int $roleId
     *
     * @return bool|UserModel
     */
    public static function Register($email, $password, $image, $roleId, $confirmRegistration = 1, $active = 1) {
		$user = new UserModel;
		$user->Email = $email;
		$user->Image = $image;
		$user->Password = Crypt::HashPassword($password);
		$user->ConfirmRegistration = $confirmRegistration;
		$user->Active = $active;
		$user->RegistrationDate = date("Y-m-d H:i:s");

		$userId = UsersRepository::Insert($user);

        if ($userId === false) {
            return false;
        }

		$userRoleModel = new UserRoleModel();
		$userRoleModel->UserId = $userId;
		$userRoleModel->RoleId = $roleId;
		UserRolesRepository::Insert($userRoleModel);

		$user->UserId = $userId;

        return $user;
    }

}