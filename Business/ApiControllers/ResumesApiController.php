<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumesDataManager;

class ResumesApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeById($resumeId) {

		return ResumesDataManager::GetResumeById($resumeId);
	}

	public static function GetResumeByIdAndLangCode($userId, $langCode) {

		return ResumesDataManager::GetResumeByIdAndLangCode($userId, $langCode);
	}

	public static function GetResumeDefault($userId) {
		return ResumesDataManager::GetResumeDefault($userId, 1);
	}

	public static function GetUserResumes($userId) {
		return ResumesDataManager::GetUserResumes($userId);
	}

	public static function DeleteResume($resumeId) {
		return ResumesDataManager::DeleteResume($resumeId);
	}

	public static function UpdateResume($model) {
		return ResumesDataManager::UpdateResume($model);
	}

	public static function InsertResume($model) {
		return ResumesDataManager::InsertResume($model);
	}

	public static function GetAboutByResumeId($resumeId) {
		return ResumesDataManager::GetAboutByResumeId($resumeId);
	}

	public static function GetResumes() {
		return ResumesDataManager::GetResumes();
	}

	public static function CountResumes() {
		return ResumesDataManager::CountResumes();
	}
}