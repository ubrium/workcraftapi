<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeBasicsDataManager;
use Data\DataManagers\ResumesDataManager;
use Data\Repositories\ResumeBasicsRepository;

class ResumeBasicsApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeBasicsDefault($resumeId) {

		return ResumeBasicsDataManager::GetResumeBasicsDefault($resumeId);
	}

	public static function UpdateResumeBasics($model) {

		return ResumeBasicsDataManager::UpdateResumeBasics($model);
	}

	public static function InsertResumeBasics($model) {

		return ResumeBasicsDataManager::InsertResumeBasics($model);
	}

}