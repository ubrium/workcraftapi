<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 1.4.2018.
 * Time: 14.45
 */

namespace Business\ApiControllers;

use Business\Models\JobModel;
use Data\DataManagers\JobsDataManager;

class JobsApiController {
	/**
	 * @param $jobId
	 * @return JobModel
	 */

	public static function GetJob($jobId) {
		return JobsDataManager::GetJob($jobId);
	}

	/**
	 * @return JobModel[]
	 */

	public static function GetJobs() {
		return JobsDataManager::GetJobs();
	}

	public static function InsertJob($model) {
		return JobsDataManager::InsertJob($model);
	}

	public static function UpdateJob($model) {
		return JobsDataManager::UpdateJob($model);
	}

	public static function DeleteJob($jobId) {
		return JobsDataManager::DeleteJob($jobId);
	}

	public static function CountJobs() {
		return JobsDataManager::CountJobs();
	}
}