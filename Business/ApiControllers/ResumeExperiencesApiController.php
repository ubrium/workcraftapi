<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeExperiencesDataManager;

class ResumeExperiencesApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeExperiencesByResumeId($resumeId) {

		return ResumeExperiencesDataManager::GetResumeExperiencesByResumeId($resumeId);
	}

	/**
	 * @param $model
	 * @return mixed
     */
	public static function InsertResumeExperience($model)
	{
		return ResumeExperiencesDataManager::InsertResumeExperience($model);
	}

	public static function UpdateResumeExperience($model)
	{
		return ResumeExperiencesDataManager::UpdateResumeExperience($model);
	}

	public static function DeleteResumeExperience($resumeExperienceId)
	{
		return ResumeExperiencesDataManager::DeleteResumeExperience($resumeExperienceId);
	}

}