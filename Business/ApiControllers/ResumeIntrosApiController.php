<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeIntrosDataManager;
use Data\DataManagers\ResumeSkillsDataManager;

class ResumeIntrosApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeIntroByResumeId($resumeId) {

		return ResumeIntrosDataManager::GetResumeIntroByResumeId($resumeId);
	}

	public static function InsertResumeIntro($model)
	{
		return ResumeIntrosDataManager::InsertResumeIntro($model);
	}

	public static function UpdateResumeIntro($model)
	{
		return ResumeIntrosDataManager::UpdateResumeIntro($model);
	}

	public static function DeleteResumeIntro($resumeIntroId)
	{
		return ResumeIntrosDataManager::DeleteResumeIntro($resumeIntroId);
	}

}