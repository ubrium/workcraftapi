<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeLanguagesDataManager;

class ResumeLanguagesApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeLanguagesByResumeId($resumeId) {

		return ResumeLanguagesDataManager::GetResumeLanguagesByResumeId($resumeId);
	}

	public static function InsertResumeLanguage($model)
	{
		return ResumeLanguagesDataManager::InsertResumeLanguage($model);
	}

	public static function UpdateResumeLanguage($model)
	{
		return ResumeLanguagesDataManager::UpdateResumeLanguage($model);
	}

	public static function DeleteResumeLanguage($resumeLanguageId)
	{
		return ResumeLanguagesDataManager::DeleteResumeLanguage($resumeLanguageId);
	}

}