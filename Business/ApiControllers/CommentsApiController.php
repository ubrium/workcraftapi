<?php

namespace Business\ApiControllers;

use Business\Models\CommentModel;
use Data\DataManagers\CommentsDataManager;

class CommentsApiController {
	/**
	 * @param $articleId
	 * @return CommentModel
	 */

	public static function GetArticleComment($articleId) {
		return CommentsDataManager::GetArticleComment($articleId);
	}

	/**
	 * @return CommentModel[]
	 */
	public static function GetArticleComments($articleId) {
		return CommentsDataManager::GetArticleComments($articleId);
	}

	public static function InsertArticleComment($model) {
		return CommentsDataManager::InsertArticleComment($model);
	}

	public static function UpdateArticleComment($model) {
		return CommentsDataManager::UpdateArticleComment($model);
	}

	public static function DeleteArticleComment($commentId) {
		return CommentsDataManager::DeleteArticleComment($commentId);
	}

}