<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeSkillsDataManager;

class ResumeSkillsApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeSkillsByResumeId($resumeId) {

		return ResumeSkillsDataManager::GetResumeSkillsByResumeId($resumeId);
	}

	public static function InsertResumeSkill($model)
	{
		return ResumeSkillsDataManager::InsertResumeSkill($model);
	}

	public static function UpdateResumeSkill($model)
	{
		return ResumeSkillsDataManager::UpdateResumeSkill($model);
	}

	public static function DeleteResumeSkill($resumeSkillId)
	{
		return ResumeSkillsDataManager::DeleteResumeSkill($resumeSkillId);
	}

}