<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeVolunteerWorksDataManager;

class ResumeVolunteerWorksApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeVolunteerWorksByResumeId($resumeId) {

		return ResumeVolunteerWorksDataManager::GetResumeVolunteerWorksByResumeId($resumeId);
	}

	/**
	 * @param $model
	 * @return mixed
     */
	public static function InsertResumeVolunteerWork($model)
	{
		return ResumeVolunteerWorksDataManager::InsertResumeVolunteerWork($model);
	}

	public static function UpdateResumeVolunteerWork($model)
	{
		return ResumeVolunteerWorksDataManager::UpdateResumeVolunteerWork($model);
	}

	public static function DeleteResumeVolunteerWork($resumeVolunteerWorkId)
	{
		return ResumeVolunteerWorksDataManager::DeleteResumeVolunteerWork($resumeVolunteerWorkId);
	}

}