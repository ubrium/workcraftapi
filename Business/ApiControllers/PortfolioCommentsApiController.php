<?php

namespace Business\ApiControllers;

use Business\Models\PortfolioCommentModel;
use Data\DataManagers\PortfolioCommentsDataManager;

class PortfolioCommentsApiController {
	/**
	 * @param $portfolioId
	 * @return PortfolioCommentModel
	 */

	public static function GetPortfolioComment($portfolioId) {
		return PortfolioCommentsDataManager::GetPortfolioComment($portfolioId);
	}

	/**
	 * @return PortfolioCommentModel[]
	 */
	public static function GetPortfolioComments($portfolioId) {
		return PortfolioCommentsDataManager::GetPortfolioComments($portfolioId);
	}

	public static function InsertPortfolioComment($model) {
		return PortfolioCommentsDataManager::InsertPortfolioComment($model);
	}

	public static function UpdatePortfolioComment($model) {
		return PortfolioCommentsDataManager::UpdatePortfolioComment($model);
	}

	public static function DeletePortfolioComment($commentId) {
		return PortfolioCommentsDataManager::DeletePortfolioComment($commentId);
	}




}