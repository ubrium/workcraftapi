<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeBasicsDataManager;
use Data\DataManagers\ResumeSectionsDataManager;

class ResumeSectionsApiController
{

    /**
     * @param $resumeId
     * @return ResumeModel[]
     */

    public static function GetResumeSectionsByResumeId($resumeId)
    {
        return ResumeSectionsDataManager::GetResumeSectionsByResumeId($resumeId);
    }

    public static function InsertResumeSection($model)
    {
        return ResumeSectionsDataManager::InsertResumeSection($model);
    }

    public static function DeleteResumeSection($resumeSectionId)
    {
        return ResumeSectionsDataManager::DeleteResumeSection($resumeSectionId);
    }


}