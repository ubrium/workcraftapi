<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeEducationsDataManager;
use Data\DataManagers\ResumeSectionsDataManager;

class ResumeEducationsApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeEducationByResumeId($resumeId) {

		return ResumeEducationsDataManager::GetResumeEducationByResumeId($resumeId);
	}

	/**
	 * @param $model
	 * @return mixed
     */
	public static function InsertResumeEducation($model)
	{
		return ResumeEducationsDataManager::InsertResumeEducation($model);
	}

	public static function UpdateResumeEducation($model)
	{
		return ResumeEducationsDataManager::UpdateResumeEducation($model);
	}

	public static function DeleteResumeEducation($resumeEducationId)
	{
		return ResumeEducationsDataManager::DeleteResumeEducation($resumeEducationId);
	}

}