<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\PortfolioImagesDataManager;

class PortfolioImagesApiController {


	public static function GetPortfolioItemImages($resumePortfolioId) {

		return PortfolioImagesDataManager::GetPortfolioItemImages($resumePortfolioId);
	}

	public static function InsertImage($model)
	{
		return PortfolioImagesDataManager::InsertImage($model);
	}

	public static function DeleteImage($portfolioImageId)
	{
		return PortfolioImagesDataManager::DeleteImage($portfolioImageId);
	}


}