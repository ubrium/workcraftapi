<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeCertificatesDataManager;

class ResumeCertificatesApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeCertificatesByResumeId($resumeId) {

		return ResumeCertificatesDataManager::GetResumeCertificatesByResumeId($resumeId);
	}

	/**
	 * @param $model
	 * @return mixed
     */
	public static function InsertResumeCertificate($model)
	{
		return ResumeCertificatesDataManager::InsertResumeCertificate($model);
	}

	public static function UpdateResumeCertificate($model)
	{
		return ResumeCertificatesDataManager::UpdateResumeCertificate($model);
	}

	public static function DeleteResumeCertificate($resumeCertificateId)
	{
		return ResumeCertificatesDataManager::DeleteResumeCertificate($resumeCertificateId);
	}


}