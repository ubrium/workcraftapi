<?php

namespace Business\ApiControllers;

use Business\Models\ArticleModel;
use Business\Models\CommentModel;
use Data\DataManagers\ArticlesDataManager;

class ArticlesApiController {

	/**
	 * @param $articleId
	 * @return ArticleModel
	 */

	public static function GetArticle($articleId) {
		return ArticlesDataManager::GetArticle($articleId);
	}

	public static function InsertArticle($model) {
		return ArticlesDataManager::InsertArticle($model);
	}

	public static function UpdateArticle($model) {
		return ArticlesDataManager::UpdateArticle($model);
	}

	public static function DeleteArticle($articleId) {
		return ArticlesDataManager::DeleteArticle($articleId);
	}

	/**
	 * @return ArticleModel[]
	 */
	public static function GetArticles() {
		return ArticlesDataManager::GetArticles();
	}

	/**
	 * @param $commentId
	 * @return CommentModel
	 */

	public static function GetArticleComment($articleId) {
		return ArticlesDataManager::GetArticleComment($articleId);
	}

	/**
	 * @return CommentModel[]
	 */
	public static function GetArticleComments() {
		return ArticlesDataManager::GetArticleComments();
	}

	public static function InsertArticleComment($model) {
		return ArticlesDataManager::InsertArticleComment($model);
	}

	public static function UpdateArticleComment($model) {
		return ArticlesDataManager::UpdateArticleComment($model);
	}

	public static function DeleteArticleComment($commentId) {
		return ArticlesDataManager::DeleteArticleComment($commentId);
	}

	public static function CountArticles() {
	return ArticlesDataManager::CountArticles();
	}


}