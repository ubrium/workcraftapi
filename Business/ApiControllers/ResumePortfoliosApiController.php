<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumePortfoliosDataManager;

class ResumePortfoliosApiController
{

    /**
     * @param $resumeId
     * @param null $status
     * @return \Business\Models\ResumeModel[]
     */

    public static function GetResumePortfoliosByResumeId($resumeId, $status = null)
    {

        return ResumePortfoliosDataManager::GetResumePortfoliosByResumeId($resumeId, $status);
    }

    public static function GetPortfolioItem($resumePortfolioId)
    {

        return ResumePortfoliosDataManager::GetPortfolioItem($resumePortfolioId);
    }

    public static function GetPortfolioNew($resumeId)
    {

        return ResumePortfoliosDataManager::GetPortfolioNew($resumeId);
    }

    public static function InsertPortfolioNew($model)
    {

        return ResumePortfoliosDataManager::InsertPortfolioNew($model);
    }

    public static function InsertResumePortfolio($model)
    {
        return ResumePortfoliosDataManager::InsertResumePortfolio($model);
    }

    public static function PublishPortfolioItem($model)
    {
        return ResumePortfoliosDataManager::PublishPortfolioItem($model);
    }

    public static function UpdateResumePortfolio($model)
    {
        return ResumePortfoliosDataManager::UpdateResumePortfolio($model);
    }

    public static function DeleteResumePortfolio($resumePortfolioId)
    {
        return ResumePortfoliosDataManager::DeleteResumePortfolio($resumePortfolioId);
    }


}