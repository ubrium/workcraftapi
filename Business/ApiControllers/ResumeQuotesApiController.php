<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeQuotesDataManager;

class ResumeQuotesApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeQuotesByResumeId($resumeId) {

		return ResumeQuotesDataManager::GetResumeQuotesByResumeId($resumeId);
	}

	public static function InsertResumeQuote($model)
	{
		return ResumeQuotesDataManager::InsertResumeQuote($model);
	}

	public static function UpdateResumeQuote($model)
	{
		return ResumeQuotesDataManager::UpdateResumeQuote($model);
	}

	public static function DeleteResumeQuote($resumeQuoteId)
	{
		return ResumeQuotesDataManager::DeleteResumeQuote($resumeQuoteId);
	}


}