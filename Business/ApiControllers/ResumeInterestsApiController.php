<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 19.4.2017.
 * Time: 18.05
 */

namespace Business\ApiControllers;


use Business\Models\ResumeModel;
use Data\DataManagers\ResumeInterestsDataManager;
use Data\DataManagers\ResumeSkillsDataManager;

class ResumeInterestsApiController {

	/**
	 * @param $resumeId
	 * @return ResumeModel[]
	 */

	public static function GetResumeInterestsByResumeId($resumeId) {

		return ResumeInterestsDataManager::GetResumeInterestsByResumeId($resumeId);
	}

	public static function InsertResumeInterest($model)
	{
		return ResumeInterestsDataManager::InsertResumeInterest($model);
	}

	public static function UpdateResumeInterest($model)
	{
		return ResumeInterestsDataManager::UpdateResumeInterest($model);
	}

	public static function DeleteResumeInterest($resumeInterestId)
	{
		return ResumeInterestsDataManager::DeleteResumeInterest($resumeInterestId);
	}


}